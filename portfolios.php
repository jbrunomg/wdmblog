<?php
require_once './loader.php'; ?>
<?php 
$titulo_pagina = $modulo7->modulo7_nome." - ".$site->site_meta_titulo;

$categoria_portfolio = new Area1();
$categoria_portfolio->db = new DB;
$categoria_portfolio->getAreas1();

require_once './header.php';
?>

<body class="js">
	
<?php require_once './menu.php'; ?>

<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($modulo7->modulo7_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($modulo7->modulo7_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($modulo7->modulo7_nome) ?></h2>
					<ul>
						<li><a href="home/">Home</a></li>
                        <li class="active"><a href="projetos/"><?= stripslashes($modulo2->modulo2_nome2) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->

	
	<!--  Project Archive-->
    <section id="project projetos" class="project archive section">
        <div class="container">
            <div class="row">
            <!-- In�cio Publicidade Top -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_top)) : ?>
   <section  class="ads_top_sub">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_top) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Top -->
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
					<!-- Project Nav -->
                    <div class="project-nav">
                        <ul>
                            <li class="active" data-filter="*">Todos</li>
                          <?php if (isset($categoria_portfolio->db->data[0])): ?>
                             <?php foreach ($categoria_portfolio->db->data as $categoria): ?>
                             <?php if ($categoria->area1_nome == "Publicidade") {
                                $categoria_area1_nome = "Public";
                                 } else {
                                 $categoria_area1_nome = $categoria->area1_nome;     
                                 }?>
                               <li data-filter=".<?= Filter::slug2(trim($categoria_area1_nome)) ?>"><?= stripslashes($categoria->area1_nome) ?></li>
                            <?php endforeach; ?>
                           <?php endif; ?> 
                        </ul>
                    </div>
					<!--/ End Project Nav -->
                </div>
            </div>
            <div class="row wow fadeInUp" data-wow-delay="0.4s">
				<div class="isotop-active">
                <?php $projeto = new Portfolio();
                      $projeto->getPortfolios();
                      
                      if (isset($projeto->db->data[0])): ?>
                     <?php foreach ($projeto->db->data as $trabalhos): ?>
                     <?php if ($trabalhos->area1_nome == "Publicidade") {
                                $trabalhos->area1_nome = "Public";
                            }?>
					<div class="col-md-3 col-sm-4 col-xs-12 <?= Filter::slug2(trim($trabalhos->area1_nome)) ?>">
						<!-- Single Project -->
						<div class="single-project">
							<div class="project-head" onclick="window.location.href='projeto/<?= Filter::slug2($trabalhos->portfolio_nome) ?>/<?= $trabalhos->portfolio_id ?>/';return false;" >
								<img src="thumb.php?w=262&h=262&zc=0&src=images/portfolio/<?= $trabalhos->portfolio_imagem ?>" alt="<?= stripslashes($trabalhos->portfolio_nome) ?>"/>
								<div class="project-hover">
									<h4><a href="projeto/<?= Filter::slug2($trabalhos->portfolio_nome) ?>/<?= $trabalhos->portfolio_id ?>/"><?= stripslashes($trabalhos->portfolio_nome) ?></a></h4>
									<p><a href="projeto/<?= Filter::slug2($trabalhos->portfolio_nome) ?>/<?= $trabalhos->portfolio_id ?>/"><?= stripslashes(Validacao::cut($trabalhos->portfolio_descricao, 150, '...')) ?></a></p>
								</div>
							</div>
						</div>
						<!--/ End Single Project -->
					</div>
		
			  <?php endforeach; ?>
          <?php endif; ?>
				
                    
				</div>
            </div>
         <!-- In�cio Publicidade Bottom -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_bottom)) : ?>
   <section  class="ads_bottom">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_bottom) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Bottom -->
        </div>
    </section>
    <!--/ End Project Archive -->
    


<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
      <!-- Custom  -->
    
  <script>
   $('li#projeto').addClass('current');
  </script> 
</body>
</html>          