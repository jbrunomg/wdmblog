<?php
require_once './loader.php'; ?>
<?php 
$titulo_pagina = $blog->modulo10_nome." - ".$site->site_meta_titulo;

require_once './header.php';
?>
<body class="js">
	
<?php require_once './menu.php'; ?>

 <!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($blog->modulo10_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($blog->modulo10_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($blog->modulo10_nome) ?></h2>
					<ul>
						<li><a href="home/">Home</a></li>
						<li class="active"><a href="blog/"><?= stripslashes($modulo2->modulo2_nome3) ?></a></li>
					</ul>
                    
           		</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->
 
	
    <!-- Start News -->
    <section class="newsblog section single">
        <div class="container">
            <div class="row">
             <!-- Início Publicidade Top -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_top)) : ?>
   <section  class="ads_top">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_top) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Top -->
                <div class="col-md-8 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-delay="0.4s">
					<div class="row">
                    <?php if (isset($_POST['busca'])) {
                   
                      $pagina = new Pagina();
                      $pagina->db->url = "blog/";
                      $pagina->db->paginate($blog->modulo10_paginacao); ?>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                      <?php
                      $pagina->getSearch();
                      ?>
                      </div>
                      <?php
                       } else {      
                    
                        $pagina = new Pagina();
                        $pagina->db->url = "blog/";
                        $pagina->db->paginate($blog->modulo10_paginacao);
                        $pagina->getPaginas();

                     } 

                     if (isset($pagina->db->data[0])): ?>
                             <?php foreach ($pagina->db->data as $pos): ?>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<!-- Single News -->
							<div class="single-news slide">
								<div class="news-head">
                                <?php if(!empty($pos->pagina_imagem)): ?> 
									<div class="news-slide">
                                    
										<div class="single-slide">
											<a title="<?= stripslashes($pos->pagina_nome) ?>" href="post/<?= Filter::slug2($pos->pagina_nome) ?>/<?= $pos->pagina_id ?>/">
                                            <img src="thumb.php?w=718&h=300&zc=0&src=images/blog/<?= $pos->pagina_imagem?>" alt="<?= stripslashes($pos->pagina_nome) ?>"/></a>
										</div>                                       
									</div>
                                    <?php endif; ?>
									<a href="post/<?= Filter::slug2($pos->pagina_nome) ?>/<?= $pos->pagina_id ?>/" class="link"><i class="fa fa-link"></i></a>
								</div>
								<div class="news-content">
									<h4><a href="post/<?= Filter::slug2($pos->pagina_nome) ?>/<?= $pos->pagina_id ?>/"><?= stripslashes($pos->pagina_nome) ?></a></h4>
									<div class="date">
                                    <a href="categoria/blog/<?= Filter::slug2($pos->area_nome) ?>/<?= $pos->area_id ?>/">
                                    <i class="fa fa-list"></i> <?= stripslashes($pos->area_nome)?></a> 
                                    <a href="javascript:void(0);">
                                    <i class="fa fa-calendar ipadding-left"></i> <?= stripslashes($pos->pagina_data)?></a>
                                    <?php if ($modulo10->modulo10_comment == 1) : ?>
                                    <a href="<?=Validacao::getBaseUrl()?>/post/<?= Filter::slug2($pos->pagina_nome) ?>/<?= $pos->pagina_id ?>/#comentario">
                                    <i class="fa fa-comment  ipadding-left"></i> <span class="fb-comments-count" data-href="<?=Validacao::getBaseUrl()?>/post/<?= Filter::slug2($pos->pagina_nome) ?>/<?= $pos->pagina_id ?>/"></span>  Comentários</a>
                                    <?php endif; ?>
                                    </div>
									<p>
                                     <?=Validacao::cut(stripslashes($pos->pagina_descricao), 320, '...');  ?>
                                    </p>
									<a href="post/<?= Filter::slug2($pos->pagina_nome) ?>/<?= $pos->pagina_id ?>/" class="btn"><?= stripslashes($blog->modulo10_button1) ?> <?= stripslashes($_GET['busca']) ?><span class="fa fa-angle-double-right"></span></a>
								</div>
							</div>
							<!--/ End Single News -->
						</div>
                        <?php endforeach; ?>
                      <?php endif; ?>
     
					
					</div>	
					<div class="row">
						<div class="col-md-12">
							<!-- Start Pagination -->
							<ul class="pagination">
								<?= $pagina->db->paginacao ?>
							</ul>
							<!--/ End Pagination -->
						</div>
					</div>
                </div>	
				
				<div class="col-md-4 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0.4s">
					<!-- News Sidebar -->
					<div class="blog-sidebar">
                    <!-- Start Search Form -->
					   <?php require_once './form_busca.php'; ?>
						<!--/ End Search Form -->
                        
						<!-- Latest News -->
						<?php require_once './ultimos_posts.php'; ?>
						<!--/ End Latest News -->
						<!-- News Category -->
						 <?php require_once './menu_categoria.php'; ?>
						<!--/ End News Category -->
                        
                        <!-- Início Instagram -->
                      <?php if ($modulo16->modulo16_status == 1) : ?>
                       <?php if (!empty($modulo16->modulo16_userid)) : ?>  
                       <div class="single-sidebar">
                       <h2><i class="fa fa-instagram"></i> <?= stripslashes($modulo16->modulo16_nome) ?></h2>
                       <div id="instagram"></div>
                       </div>
                       <?php endif; ?>
                     <?php endif; ?>
                     <!-- Final Instagram -->  
                        
						
                        <!-- Início Publicidade Side -->
                        <?php if ($modulo4->modulo4_status == 1) : ?>
                         <?php if (!empty($modulo4->modulo4_side)) : ?>
                           <div class="single-sidebar call-to-action">
                           	<?php if (!empty($modulo4->modulo4_side1)) : ?> <h2><?= stripslashes($modulo4->modulo4_side1) ?></h2><?php endif; ?>
                              <?= stripslashes($modulo4->modulo4_side) ?>
                            </div>
                          <?php endif; ?>
                        <?php endif; ?>	 
                       <!-- / Final Publicidade Side -->
 
					</div>
					<!--/ End News Sidebar -->
				</div>
            </div>	
             <!-- Início Publicidade Bottom -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_bottom)) : ?>
   <section  class="ads_bottom_sub">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_bottom) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Bottom -->
        </div>
    </section>
    <!--/ End News -->
    


<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
      <!-- Custom  -->
    
  <script>
   $('li#posts').addClass('current');
  </script> 

</body>
</html>          