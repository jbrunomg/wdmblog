<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class Modulo16 extends Controle {

    public $db;
    public $modulo16_id;
    public $modulo16_nome;
    public $modulo16_userid;
    public $modulo16_status;

    public function __construct() {
        parent::__construct();
        require_once 'Registry.php';
        $registry = Registry::getInstance();
        if( $registry->get('db') == false ) {
            $registry->set('db', new DB);
        }
        $this->db = $registry->get('db');           
    }
    
    

    public function atualizar() {
        $query = "modulo16_nome = '$this->modulo16_nome', modulo16_userid  = '$this->modulo16_userid', modulo16_status = '$this->modulo16_status'";
             
        $this->update("modulo16", "$query", "modulo16_id = '$this->modulo16_id'");
    }

    public function getModulo16() {
        $this->select("modulo16", "", "*", "", "WHERE modulo16_id = 1", "");
    }

    
}