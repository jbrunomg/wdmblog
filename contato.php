<?php
require_once './loader.php'; ?>
<?php 
$titulo_pagina = stripslashes($modulo9->modulo9_nome);
$imagem_pagina =  Validacao::getBaseUrl()."/thumb.php?w=200&src=images/".$contato->modulo9_imagem;

$descricao_pagina = Validacao::cut(stripslashes($contato->modulo9_descricao), 190, ' ...');

require_once './header.php';
?>
<body class="js">

	<!-- Preloader -->
	 <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
	<!-- End Preloader -->
	
<?php require_once './menu.php'; ?>
      
<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($modulo9->modulo9_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($modulo9->modulo9_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($modulo9->modulo9_nome) ?></h2>
					<ul>
						<li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
						<li class="active"><a href="contato/"><?= stripslashes($modulo9->modulo9_nome) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->   
    
  
 <section class="section single" style="padding-top: 70px;"> 




  <!-- Contact Us -->
    <?php if ($modulo9->modulo9_status == 1): ?>
  <section id="contact" class="section">
    <div class="container">
       <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                <?php if(!empty($modulo9->modulo9_nome) || !empty($modulo9->modulo9_subtitulo)) {?>
          <div class="section-title">
             <h2><?= stripslashes($modulo9->modulo9_nome) ?></h2>
             <p><?= stripslashes($modulo9->modulo9_subtitulo) ?></p>
          </div>
                <?php } ?>
                     <?php
                            if (isset($_POST['email']) && !empty($_POST['email'])) {
                                if ($mail->Send()) {
                                    echo "<p class='alert alert-success' id='msg_alert'> <strong>Obrigado !</strong> Sua Mensagem foi entregue.</p>";
                                } else {
                                    echo "<p class='alert alert-danger' id='msg_alert'> Erro ao enviar  Mensagem: $mail->ErrorInfo</p>";
                                }
                            }
                            ?> 
        </div>
       </div>
    
      <div class="row">
        <!-- Contact Form -->
        <div class="col-md-5 col-sm-6 col-xs-12 wow fadeInUp">
          <form method="post" id="contactfrm" action="#contact" role="form">
            <div class="form-group">
              <input type="text" name="nome" placeholder="Seu nome" required="required">
                        </div>
            <div class="form-group">
              <input type="email" name="email" placeholder="Seu email" required="required">
                        </div>
            <div class="form-group">
              <input type="text" name="assunto" placeholder="Assunto" required="required">
                        </div>
            <div class="form-group">
              <textarea name="mensagem" rows="6" placeholder="Escreva sua mensagem aqui" required="required"></textarea>
                        </div>
            <div class="form-group">  
              <button type="submit" class="button primary"><i class="fa fa-send"></i><?= $modulo9->modulo9_button ?></button>
                        </div>
          </form>
                    
        </div>
        <!--/ Contact Form -->
        <!-- Google Map -->
        <div class="col-md-7 col-sm-6 col-xs-12 wow fadeInUp">
          <div class="contact-map">
            <div id="mapa" style="width: 100%; height: 385px;"><?=$contato->contato_maps?></div>
          </div>
        </div>
        <!--/ End Google Map -->
      </div>
    </div>
  </section>
   <?php endif; ?>
  <!--/ End Clients Us -->



   	</section>    
         



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>	
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
      <!-- Custom  -->
    
  <script>
   $('li#contato').addClass('current');
  </script> 
</body>
</html>          