<?php
require_once './loader.php';

$pagina_id = intval($_GET['id']);
$pagina = new Pagina();
$pagina->pagina_id = "$pagina_id";
$pagina->getPagina();

$titulo_pagina = stripslashes($pagina->pagina_nome);
if(!empty($pagina->pagina_description)){
$descricao_pagina = stripslashes($pagina->pagina_description);
}
if(!empty($pagina->pagina_keywords)){
$meta_palavra = stripslashes($pagina->pagina_keywords);
}
$imagem_pagina = Validacao::getBaseUrl()."/thumb.php?h=250&src=images/blog/".$pagina->pagina_imagem;

require_once './header.php';
?>
<body class="js">

	<!-- Preloader -->
	 <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
	<!-- End Preloader -->
	
<?php require_once './menu.php'; ?>



<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($blog->modulo10_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($blog->modulo10_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($pagina->pagina_nome) ?></h2>
					<ul>
						<li><a href="home/">Home</a></li>
                           <li><a href="categoria/blog/<?= Filter::slug2($pagina->area_nome) ?>/<?= $pagina->area_id ?>/"><?= stripslashes($pagina->area_nome) ?></a></li>
                        <li class="active"><a href="post/<?= Filter::slug2($pagina->pagina_nome) ?>/<?= $pagina->pagina_id ?>/"><?= stripslashes($pagina->pagina_nome) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->
    
	
    <?php if (isset($pagina->db->data[0])) { ?>
    <!-- Start News -->
    <section class="newsblog section single">
        <div class="container">
            <div class="row">
            <!-- Início Publicidade Top -->
                           <?php if ($modulo4->modulo4_status == 1) : ?>
                            <?php if (!empty($modulo4->modulo4_top)) : ?>
                               <section  class="ads_top">
                                      <div class="container">
                                          <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                                               <?= stripslashes($modulo4->modulo4_top) ?>
                                            </div>
                                          </div>
                                      </div>
                               </section>
                            <?php endif; ?>
                          <?php endif; ?>
                        <!-- / Final Publicidade Top -->
                <div class="col-md-8 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-delay="0.4s">
					<div class="row">
						 <div class="col-md-12 col-sm-12 col-xs-12">
                          
                         
                         
							<!-- Single News -->
							<div class="single-news">
								<div class="news-head">
									<img src="thumb.php?h=400&src=images/blog/<?= $pagina->pagina_imagem ?>" alt="<?= stripslashes($pagina->pagina_nome) ?>">
									<a href="#" class="link"><i class="fa fa-link"></i></a>
								</div>
								<!-- News Content --->
								<div class="news-content">
									<h4><a><?= stripslashes($pagina->pagina_nome) ?></a></h4>
									<div class="date">
                                    <a href="categoria/blog/<?= Filter::slug2($pagina->area_nome) ?>/<?= $pagina->area_id ?>/">
                                    <i class="fa fa-list"></i> <?= stripslashes($pagina->area_nome) ?></a> 
                                    <a href="javascript:void(0);">
                                    <i class="fa fa-calendar ipadding-left"></i> <?= stripslashes($pagina->pagina_data) ?></a>
                                    <?php if ($modulo10->modulo10_comment == 1) : ?>
                                    <a href="<?=Validacao::getBaseUrl()?>/post/<?= Filter::slug2($pagina->pagina_nome) ?>/<?= $pagina->pagina_id ?>/#comentario">
                                    <i class="fa fa-comment  ipadding-left"></i> <span class="fb-comments-count" data-href="<?=Validacao::getBaseUrl()?>/post/<?= Filter::slug2($pagina->pagina_nome) ?>/<?= $pagina->pagina_id ?>/"></span>  Comentários</a>
                                    <?php endif; ?>
                                    </div>
                             <?= $pagina->pagina_descricao ?>
								</div>
								<!--/ End News Content -->
								<!-- News Share -->
								<div class="news-share">
								             
                            <?php require_once "shared_buttons.php"; ?>
								</div>
								<!--/ End News Share -->
								<!-- Next Prev -->
								<div class="prev-next" style="display: none;">
									<ul>
										<li class="prev"><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
										<li class="next"><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
									</ul>
								</div>
								<!--/ End Next Prev -->
								<!-- Comments -->
                                <a name="comentario">&nbsp;</a>
                                <?php if (!isset($_POST['busca'])) : ?>
                                
                                 <?php if ($modulo10->modulo10_comment == 1) : ?>
								<div class="blog-comments">
									<h2>Comentários</h2>
									<div class="comments-body">

										<div class="fb-comments" data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" data-order-by="reverse_time" data-numposts="5" include-parent="false"></div>
										
									</div>
								
								</div>
                                 <?php endif; ?>
                               <?php endif; ?>
								<!--/ End Comments -->
							</div>
							<!--/ End Single News -->
						</div>
					</div>	
                </div>	
				
				<div class="col-md-4 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0.4s">
					<!-- News Sidebar -->
					<div class="blog-sidebar">
						<!-- Start Search Form -->
					 <?php require_once './form_busca.php'; ?>
						<!--/ End Search Form -->
						<!-- Latest News -->
						<?php require_once './ultimos_posts.php'; ?>
						<!--/ End Latest News -->
						<!-- News Category -->
					 <?php require_once './menu_categoria.php'; ?>
						<!--/ End News Category -->
                        
                        <!--/ Tags Cloud -->
                        <?php 
                        if($modulo10->modulo10_tags == 1):
                        $pagina_id = intval($_GET['id']);
                        $pagina = new Pagina();
                        $pagina->pagina_id = "$pagina_id";
                        $pagina->getPagina();
                        if(!empty($pagina->pagina_keywords)):
                        $tags = explode(",",$pagina->pagina_keywords);?>
                      <div class="single-sidebar form">  
                        <div class="widget">
                            <h2>Tags</h2>
                            <div class="w-tags">
                   
                            <?php foreach($tags as $tag) {?> 
                            <form id="tagform-<?=stripslashes(trim($tag))?>" action="<?=Validacao::getBaseUrl()?>/blog/" method="post">
                             <input type="hidden" name="busca" value="<?=stripslashes(trim($tag))?>"/>
                             <a href="javascript:;" onclick="parentNode.submit();" class="text-edit"><?=stripslashes(trim($tag))?></a>
                             </form>
                            <?php } ?>
                         
                         </div>
                        </div>
                     </div>
                    <?php endif; ?> 
                   <?php endif; ?>
                        <!--/ Tags Cloud --> 
                      
                       <!-- Início Instagram -->
                      <?php if ($modulo16->modulo16_status == 1) : ?>
                       <?php if (!empty($modulo16->modulo16_userid)) : ?>  
                       <div class="single-sidebar">
                       <h2><i class="fa fa-instagram"></i> <?= stripslashes($modulo16->modulo16_nome) ?></h2>
                       <div id="instagram"></div>
                       </div>
                       <?php endif; ?>
                     <?php endif; ?>
                     <!-- Final Instagram -->  
                     
						<!-- Início Publicidade Side -->
                        <?php if ($modulo4->modulo4_status == 1) : ?>
                         <?php if (!empty($modulo4->modulo4_side)) : ?>
                           <div class="single-sidebar call-to-action">
                           	<?php if (!empty($modulo4->modulo4_side1)) : ?> <h2><?= stripslashes($modulo4->modulo4_side1) ?></h2><?php endif; ?>
                              <?= stripslashes($modulo4->modulo4_side) ?>
                            </div>
                          <?php endif; ?>
                        <?php endif; ?>	  
                       <!-- / Final Publicidade Side -->
                       
                       
                       
                       
                       
					</div>
					<!--/ End News Sidebar -->
				</div>
            </div>	
 <!-- Início Publicidade Bottom -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_bottom)) : ?>
   <section  class="ads_bottom">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_bottom) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
        </div>
<!-- / Final Publicidade Bottom -->
    </section>
    <!--/ End News -->
    



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>	
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
      <!-- Custom  -->
    
  <script>
   $('li#posts<?= stripslashes($post->area_id) ?>').addClass('current');
   $('li#posts<?= stripslashes($post->area_id) ?>').click(function(){
   window.location.href='blog/';
})

</script>
  	
</body>
</html>   
<?php } else {
 header("Location: ".Validacao::getBaseUrl()."/404") ;
 } 
    
?>       