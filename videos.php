<?php
require_once './loader.php'; ?>
<?php 
$titulo_pagina = stripslashes($menu->modulo2_nome4)." - ".$site->site_meta_titulo;
$descricao_pagina = stripslashes($video_->modulo12_descricao); 

require_once './header.php';
?>
<body class="js">
	
<?php require_once './menu.php'; ?>

<?php
$categoria_video = new Area2();
$categoria_video->db = new DB;
$categoria_video->getAreas2();

$videos = new Video();
$videos->getVideos();
?>

	<!-- Start Breadcrumbs -->
	<section class="breadcrumbs"  <?php if(!empty($video->modulo12_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($video->modulo12_imagem).");'"; } ?> >
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($video->modulo12_nome) ?></h2>
					<ul>
						<li><a href="home/">Home</a></li>
						<li class="active"><a href="videos/"><?= stripslashes($video->modulo12_nome) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
<!--/ End Breadcrumbs --> 
    
    <!--  Videos Archive-->
    <section id="videos" class="project archive section">
                <!-- Início Publicidade Top -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_top)) : ?>
   <section  class="ads_top_sub">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_top) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Top -->  
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
					<!-- Videos Nav -->
                    <div class="project-nav">
                        <ul>                           
                            <li class="active" data-filter="*">Todos</li>
                            <?php if (isset($categoria_video->db->data[0])): ?>
                              <?php foreach ($categoria_video->db->data as $cat_video): ?>
                                 <li data-filter=".<?= Filter::slug2(trim($cat_video->area2_nome)) ?>"><?= stripslashes($cat_video->area2_nome) ?></li>
                                 <?php endforeach; ?>
                            <?php endif; ?> 
        
                        </ul>
                    </div>
					<!--/ End Videos Nav -->
                </div>
            </div>

           <div class="row wow fadeInUp" data-wow-delay="0.4s">
				<div class="isotop-active newsblog">
                <?php if (isset($videos->db->data[0])): ?>
                   <?php foreach ($videos->db->data as $vid): ?>
            <?php 
            if ($vid->video_views < 2) {
                $views = stripslashes($vid->video_views)." visualização";
            } else {
                $views = stripslashes($vid->video_views)." visualizações";
            } ?>
					<div class="col-md-3 col-sm-4 col-xs-12  wow fadeInUp  <?= Filter::slug2(trim($vid->area2_nome)) ?>" >
						<!-- Single Video -->
						                       
                      <div class="single-news" style="margin-top: 0px;">
						<div class="news-head">
											
							<img src="<?= $vid->video_imagem ?>" width="262" height="198" alt="Assista: <?= stripslashes($vid->video_nome) ?>"/>
                            <a href="play_video.php?video_url=<?= $vid->video_url ?>&video_id=<?= stripslashes($vid->video_id) ?>" class="link video-play mfp-iframe" title="Assista: <?= stripslashes($vid->video_nome) ?>">
                            <i class="fa fa-play"></i></a>
                            
					</div>
						<div class="news-content">
							<h4><a href="play_video.php?video_url=<?= $vid->video_url ?>&video_id=<?= stripslashes($video->video_id) ?>" class="video-play mfp-iframe"><?= stripslashes($vid->video_nome) ?></a></h4>
							<div class="date datesmall">
                             <a href="javascript:void(0);">
                             <i class="fa fa-list"></i> <?= stripslashes($vid->area2_nome)?></a> 
                             <a href="javascript:void(0);">
                             <i class="fa fa-eye ipadding-left"></i> <?= $views?></a> 
                            </div>
						</div>
					</div>
						<!--/ End Single Video -->
				</div>
                  <?php endforeach; ?>
                 <?php endif; ?>  
			  </div>
          </div>
        </div>
         <!-- Início Publicidade Bottom -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_bottom)) : ?>
   <section  class="ads_bottom">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_bottom) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Bottom -->
    </section>
<!--/ End Videos Archive -->



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>	
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
      <!-- Custom  -->
    
  <script>
   $('li#video').addClass('current');
  </script> 
</body>
</html>          