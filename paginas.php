<?php
require_once './loader.php'; ?>
<?php 
$titulo_pagina = $modulo17->modulo17_nome." - ".$site->site_meta_titulo;

require_once './header.php';
?>
<body class="js">
	
<?php require_once './menu.php'; ?>

 <!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($modulo17->modulo17_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($modulo17->modulo17_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($modulo17->modulo17_nome) ?></h2>
					<ul>
						<li><a href="home/">Home</a></li>
						<li class="active"><a href="paginas/"><?= stripslashes($modulo17->modulo17_nome) ?></a></li>
					</ul>
                    
           		</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->
 
	
    <!-- Start News -->
    <section class="newsblog section single">
        <div class="container">
            <div class="row">
             <!-- Início Publicidade Top -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_top)) : ?>
   <section  class="ads_top">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_top) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Top -->
                <div class="col-md-8 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-delay="0.4s">
					<div class="row">
                    <?php if (isset($_POST['busca'])) {
                   
                      $pagina = new Paginas();
                      $pagina->db->url = "paginas/";
                      $pagina->db->paginate($modulo17->modulo17_paginacao); ?>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                      <?php
                      $pagina->getSearch();
                      ?>
                      </div>
                      <?php
                       } else {      
                    
                        $pagina = new Paginas();
                        $pagina->db->url = "paginas/";
                        $pagina->db->paginate($modulo17->modulo17_paginacao);
                        $pagina->getPaginasAll();

                     } 

                     if (isset($pagina->db->data[0])): ?>
                             <?php foreach ($pagina->db->data as $pos): ?>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<!-- Single News -->
							<div class="single-news slide">
								<div class="news-head">
                                <?php if(!empty($pos->paginas_imagem)): ?>   
									<div class="news-slide">
                                    
										<div class="single-slide">
											<a title="<?= stripslashes($pos->paginas_nome) ?>" href="post/<?= Filter::slug2($pos->paginas_nome) ?>/<?= $pos->paginas_id ?>/">
                                            <img src="thumb.php?w=718&h=350&zc=0&src=images/paginas/<?= $pos->paginas_imagem?>" alt="<?= stripslashes($pos->paginas_nome) ?>"/></a>
										</div>                                       
									</div>
                                 <?php endif; ?>
									<a href="pagina/<?= Filter::slug2($pos->paginas_nome) ?>/<?= $pos->paginas_id ?>/" class="link"><i class="fa fa-link"></i></a>
								</div>
								<div class="news-content">
									<h4><a href="pagina/<?= Filter::slug2($pos->paginas_nome) ?>/<?= $pos->paginas_id ?>/"><?= stripslashes($pos->paginas_nome) ?></a></h4>
									<div class="date">

                                    
                                     <?php 
                                       if($pos->area3_parent != '0') {
                                       $p_area3 = new Area3();
                                       $p_area3->db = new DB;
                                       $p_area3->area3_id = $pos->area3_parent;
                                       $p_area3->getArea3_();
                                       if (isset($p_area3->db->data[0])) { ?>
                                       <a href="categoria/paginas/<?= Filter::slug2($p_area3->area3_nome) ?>/<?= $p_area3->area3_id ?>/"><i class="fa fa-list"></i> <?=stripslashes($p_area3->area3_nome) ?> <i class="fa fa-angle-right"></i> </a> 
                                       <?php }
                                       } ?>
                                    
                                       <a href="categoria/paginas/<?= Filter::slug2($pos->area3_nome) ?>/<?= $pos->area3_id ?>/"><?php if($pos->area3_parent == '0') {?> <i class="fa fa-list"></i><?php } ?> <?= stripslashes($pos->area3_nome)?></a> 
                                    
                                    
                                    <?php if ($modulo17->modulo17_comment == 1) : ?>
                                    <a href="<?=Validacao::getBaseUrl()?>/pagina/<?= Filter::slug2($pos->paginas_nome) ?>/<?= $pos->paginas_id ?>/#comentario">
                                    <i class="fa fa-comment  ipadding-left"></i> <span class="fb-comments-count" data-href="<?=Validacao::getBaseUrl()?>/pagina/<?= Filter::slug2($pos->paginas_nome) ?>/<?= $pos->paginas_id ?>/"></span>  Comentários</a>
                                    <?php endif; ?>
                                    </div>
									<p>
                                     <?=Validacao::cut(stripslashes($pos->paginas_descricao), 320, '...');  ?>
                                    </p>
									<a href="pagina/<?= Filter::slug2($pos->paginas_nome) ?>/<?= $pos->paginas_id ?>/" class="btn"><?= stripslashes($modulo17->modulo17_button) ?> <?= stripslashes($_GET['busca']) ?><span class="fa fa-angle-double-right"></span></a>
								</div>
							</div>
							<!--/ End Single News -->
						</div>
                        <?php endforeach; ?>
                      <?php endif; ?>
     
					
					</div>	
					<div class="row">
						<div class="col-md-12">
							<!-- Start Pagination -->
							<ul class="pagination">
								<?= $pagina->db->paginacao ?>
							</ul>
							<!--/ End Pagination -->
						</div>
					</div>
                </div>	
				
				<div class="col-md-4 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0.4s">
					<!-- News Sidebar -->
					<div class="blog-sidebar">
                    <!-- Start Search Form -->
					   <!-- Start Search Form -->
					   <?php require_once './form_busca_paginas.php'; ?>
						<!--/ End Search Form -->
                        
						<!-- Latest News -->
						<?php require_once './ultimas_paginas.php'; ?>
						<!--/ End Latest News -->
						<!-- News Category -->
						 <?php require_once './menu_categorias.php'; ?>
						<!--/ End News Category -->
                        
                        <!-- Início Instagram -->
                      <?php if ($modulo16->modulo16_status == 1) : ?>
                       <?php if (!empty($modulo16->modulo16_userid)) : ?>  
                       <div class="single-sidebar">
                       <h2><i class="fa fa-instagram"></i> <?= stripslashes($modulo16->modulo16_nome) ?></h2>
                       <div id="instagram"></div>
                       </div>
                       <?php endif; ?>
                     <?php endif; ?>
                     <!-- Final Instagram -->  
                        
						
                        <!-- Início Publicidade Side -->
                        <?php if ($modulo4->modulo4_status == 1) : ?>
                         <?php if (!empty($modulo4->modulo4_side)) : ?>
                           <div class="single-sidebar call-to-action">
                           	<?php if (!empty($modulo4->modulo4_side1)) : ?> <h2><?= stripslashes($modulo4->modulo4_side1) ?></h2><?php endif; ?>
                              <?= stripslashes($modulo4->modulo4_side) ?>
                            </div>
                          <?php endif; ?>
                        <?php endif; ?>	 
                       <!-- / Final Publicidade Side -->
 
					</div>
					<!--/ End News Sidebar -->
				</div>
            </div>	
             <!-- Início Publicidade Bottom -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_bottom)) : ?>
   <section  class="ads_bottom_sub">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_bottom) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Bottom -->
        </div>
    </section>
    <!--/ End News -->
    


<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
      <!-- Custom  -->
    

</body>
</html>          