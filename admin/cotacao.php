<?php
require_once '../loader.php';
@session_start();
if (!isset($_SESSION['LOGADO']) || $_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- START @HEAD -->
    <head>
        <!-- START @META SECTION -->
        <?php require_once './base.php'; ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title><?= $site->site_meta_titulo ?></title>
        <!--/ END META SECTION -->

        <!-- START @FAVICONS -->
        <link href="./assets/img/ico/<?= $modulo_aparencia->modulo_aparencia_favicon ?>" rel="shortcut icon" sizes="144x144">
        <!--/ END FAVICONS -->

        <!-- START @FONT STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'>
        <!--/ END FONT STYLES -->

        <!-- START @GLOBAL MANDATORY STYLES -->
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- START @PAGE LEVEL STYLES -->
        <link href="./assets/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="./assets/css/animate.min.css" rel="stylesheet">
        <link href="./assets/css/bootstrap-tagsinput.css" rel="stylesheet">
        <link href="./assets/css/jasny-bootstrap-fileinput.min.css" rel="stylesheet">
        <link href="./assets/css/chosen.min.css" rel="stylesheet">
        <link href="./assets/css/bootstrap-switch.min.css" rel="stylesheet">
        <!--/ END PAGE LEVEL STYLES -->

        <!-- START @THEME STYLES -->
        <link href="./assets/css/reset.css" rel="stylesheet">
        <link href="./assets/css/layout.css" rel="stylesheet">
        <link href="./assets/css/components.css" rel="stylesheet">
        <link href="./assets/css/plugins.css" rel="stylesheet">
        <link href="./assets/css/themes/default.theme.css" rel="stylesheet" id="theme">
        <link href="./assets/css/custom.css" rel="stylesheet">
        <link href="./assets/css/datepicker.css" rel="stylesheet">
        <link href="./assets/css/social.css" rel="stylesheet">
        <link href="./assets/css/jquery.rtnotify.css" rel="stylesheet">
        <link href="./assets/css/noty_theme_default.css" rel="stylesheet">
        <!--/ END THEME STYLES -->

        <!-- START @IE SUPPORT -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="./assets/js/html5shiv.min.js"></script>
        <script src="./assets/js/respond.min.js"></script>
        <![endif]-->
        <!--/ END IE SUPPORT -->
    </head>
    <!--/ END HEAD -->

    <body>

        <!--[if lt IE 9]>
        <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <section id="wrapper" class="page-sound">
            <?php require_once './navegacao.php'; ?>
            <?php require_once './menu.php'; ?>
            <section id="page-content">
                <div class="header-content">
                    <h2><i class="fa fa-comments"></i>  <span>Gerenciar Cotacao  </span></h2>
                    <div class="breadcrumb-wrapper hidden-xs">
                        <span class="label">Você está em :</span>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="home/">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Cotacao</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="body-content animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="pull-left">
                            <h3 class="panel-title"><button class="btn btn-primary" data-toggle="modal" data-target="#incluir"><i class="fa fa-plus-circle"></i> Adicionar Cotação</button></h3>
                        </div>
                        <div class="clearfix"></div>
                        <br />
                           <?php $cotacao = new Cotacao();
                                          $cotacao->getCotacao();
                                    ?>
                                        <?php if (isset($cotacao->db->data [0])): ?>
                            <div class="table-responsive mb-20">
                                <table class="table table-striped table-primary">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Celular</th>
                                            <th>Status</th>
                                            <th>Data Atuallização</th>
                                            <th>Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                 
                                            <?php foreach ($cotacao->db->data as $cad): ?>
                                                <tr>
                                                    <td>
                                                        <?= stripslashes($cad->cotacao_nome) ?>    
                                                    </td>

                                                    <td>
                                                        <?= stripslashes($cad->cotacao_celular) ?>    
                                                    </td>

                                                    <td>
                                                        <?= stripslashes($cad->cotacao_status_atendimento) ?>    
                                                    </td>

                                                     <td>
                                                        <?= date('d/m/Y h:i:s', strtotime($cad->cotacao_atualizacao)); ?>    
                                                    </td>

                                                    <td style="width: 20%;">
                                                        <div class="form-group">
                                                            <a class="btn btn-circle btn-info atualizar" id="cotacao" data-update="<?= $cad->cotacao_id ?>" data-date="<?= date('d/m/Y', strtotime($cad->cotacao_data)); ?>">
                                                                <i class="fa fa-edit icon-white"></i>
                                                            </a>
                                                            <a  data-url="cotacao_fn.php?acao=remover&id=<?= $cad->cotacao_id ?>" class="btn  btn-danger btn-circle delete">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            <?= $cotacao->db->paginacao ?>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        
           <!--***************MODAL INCLUIR*****************-->
        <div class="modal fade" id="incluir" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog panel panel-primary">
                <div class="modal-content ">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h3 class="panel-title text-center">Nova Cotação</h3>
                    </div>
                    <div class="modal-header ">
                        <form role="form" method="post" enctype="multipart/form-data" action="cotacao_fn.php?acao=incluir">

                            <div class="form-group">
                                <label class="control-label"><i class="fa fa-"></i>Status do atendimento</label>
                                <select class="form-control input-sm mb-15 rounded"  name="cotacao_status_atendimento" style="text-transform: uppercase;" >
                                    <option value="">Selecione um tipo</option>                                    
                                    <option value="Em aberto">Em aberto</option>
                                    <option value="Em andamento">Em andamento</option>
                                    <option value="Finalizado">Finalizado</option>
                                </select>
                            </div>  
                            
                            <div class="form-group">
                                <label class="control-label">Nome</label>
                                <input class="form-control" type="text"  name="cotacao_nome" style="max-height:50px;" placeholder="Informe o nome " required/>
                            </div>

                             <div class="form-group">
                                <label class="control-label">Email</label>
                                <input class="form-control" type="email"  name="cotacao_email" style="max-height:50px;" placeholder="Informe o email "/>
                            </div>

                             <div class="form-group">
                                <label class="control-label">Celular</label>
                                <input class="form-control" type="fone"  name="cotacao_celular" style="max-height:50px;" placeholder="Informe o celular " required/>
                            </div>

                             <div class="form-group">
                                <label class="control-label">Placa</label>
                                <input class="form-control" type="text"  name="cotacao_placa_veiculo" style="max-height:50px;" placeholder="Informe a placa do veiculo " required />
                            </div>

                             <div class="form-group">
                                <label class="control-label">Endereço</label>
                                <input class="form-control" type="text"  name="cotacao_logradouro" style="max-height:50px;" placeholder="Informe o endereço "/>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Observação</label>
                                <textarea class="form-control rounded" type="text"  name="cotacao_observacao" style="max-height:50px;"></textarea>
                            </div>

                            <div class="form-group">
                            <label for="area_data">Data cadastro:</label>
                                <div class="input-group date"  style="max-width: 150px;">
                                    <input type="text" class="form-control"  name="cotacao_cadastro" value="<?php echo date('d-m-Y') ?>" readonly />
                                    <div class="input-group-addon">
                                       <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>

                                                   
                            
                            
                            <!-- /.form-group -->

                            <div class="form-group">
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        
        

           <?php //foreach ($cotacao->db->data as $cad): ?>
        <!--***************MODAL ATUALIZAR*****************-->
        <div class="modal fade" id="modalid" tabindex="-1" role="dialog"  aria-hidden="true">

            <div class="modal-dialog panel panel-primary">
                <div class="modal-content ">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h3 class="panel-title text-center">Editar Cotação</h3>
                    </div>
                    <div class="modal-header ">
                        <form role="form" method="post" enctype="multipart/form-data" action="cotacao_fn.php?acao=atualizar">

                            <div class="form-group">
                                <label class="control-label"><i class="fa fa-"></i>Status do atendimento</label>
                                <select class="form-control input-sm mb-15 rounded" id="cotacao_status_atendimento" name="cotacao_status_atendimento" style="text-transform: uppercase;" >
                                    <option value="">Selecione um tipo</option>                                    
                                    <option <?php if($cotacao->cotacao_status_atendimento == 'Em aberto'){echo 'selected';} ?> value="Em aberto">Em aberto</option>
                                    <option <?php if($cotacao->cotacao_status_atendimento == 'Em andamento'){echo 'selected';} ?> value="Em andamento">Em andamento</option>
                                    <option <?php if($cotacao->cotacao_status_atendimento == 'Finalizado'){echo 'selected';} ?> value="Finalizado">Finalizado</option>                  

                                </select>
                            </div>  
                            
                            <div class="form-group">
                                <label class="control-label">Nome</label>
                                <input class="form-control" type="text"  name="cotacao_nome" id="cotacao_nome" style="max-height:50px;" placeholder="Informe o nome " value="<?= stripslashes($cotacao->cotacao_nome) ?>" required/>
                            </div>

                             <div class="form-group">
                                <label class="control-label">Email</label>
                                <input class="form-control" type="email"  name="cotacao_email" id="cotacao_email" style="max-height:50px;" placeholder="Informe o email " value="<?= stripslashes($cotacao->cotacao_email) ?>"/>
                            </div>                        

                             <div class="form-group">
                                <label class="control-label">Celular</label>
                                <input class="form-control" type="fone"  name="cotacao_celular" id="cotacao_celular" style="max-height:50px;" placeholder="Informe o celular " value="<?= stripslashes($cotacao->cotacao_celular) ?>" required/>
                            </div>

                             <div class="form-group">
                                <label class="control-label">Placa</label>
                                <input class="form-control" type="text"  name="cotacao_placa_veiculo" id="cotacao_placa_veiculo" style="max-height:50px;" placeholder="Informe a placa do veiculo " value="<?= stripslashes($cotacao->cotacao_placa_veiculo) ?>" required />
                            </div>

                             <div class="form-group">
                                <label class="control-label">Endereço</label>
                                <input class="form-control" type="text"  name="cotacao_logradouro"  id="cotacao_logradouro" style="max-height:50px;" placeholder="Informe o endereço " value="<?= stripslashes($cotacao->cotacao_logradouro) ?>"/>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Observação</label>
                                <textarea class="form-control rounded" type="text"   name="cotacao_observacao" id="cotacao_observacao" style="max-height:50px;"><?= stripslashes($cotacao->cotacao_observacao) ?></textarea>
                            </div>

                            <div class="form-group">
                            <label for="area_data">Data cadastro:</label>
                                <div class="input-group date"  style="max-width: 150px;">
                                    <input type="text" class="form-control"  name="cotacao_cadastro" id="cotacao_cadastro"  value="<?=  date('d/m/Y', strtotime($cotacao->cotacao_cadastro))  ?>" readonly />
                                    <div class="input-group-addon">
                                       <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                            


                            <input type="hidden" id="cotacao_id"  name="cotacao_id" value="<?= $cotacao->cotacao_id ?>">

                            <div class="form-group">
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Atualizar</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
  <?php // endforeach; ?>

        <!--***************MODAL ATUALIZAR*****************-->


        <!--***************MODAL REMOVER*****************-->
        <div class="modal fade" id="MODALREMOVE" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="text-center text-danger">Atenção!</h4>
                        <p class="text-center text-danger">
                            Você está prestes à excluir um registro de forma permanente.<br />
                            Deseja realmente executar este procedimento?
                        </p>
                        <p class="text-center">
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-remove"></i> Cancelar</button>
                            <button type="button" class="btn btn-danger" id="btn-confirm-remove"><i class="fa fa-trash"></i> Remover</button>

                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!--***************MODAL REMOVER*****************-->
        <div id="back-top" class="animated pulse circle">
            <i class="fa fa-angle-up"></i>
        </div>

        <!-- START @CORE PLUGINS -->
        <script src="./assets/js/jquery.min.js"></script>
        <script src="./assets/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="./assets/js/handlebars.js"></script>
        <script src="./assets/js/typeahead.bundle.min.js"></script>
        <script src="./assets/js/jquery.nicescroll.min.js"></script>
        <script src="./assets/js/index.js"></script>
        <script src="./assets/js/jquery.easing.1.3.min.js"></script>
        <script src="./assets/ionsound/ion.sound.min.js"></script>
        <script src="./assets/js/bootbox.js"></script>
        <!--/ END CORE PLUGINS -->

        <!-- START @PAGE LEVEL PLUGINS -->
        <script src="./assets/js/bootstrap-tagsinput.min.js"></script>
        <script src="./assets/js/jasny-bootstrap.fileinput.min.js"></script>
        <script src="./assets/js/holder.js"></script>
        <script src="./assets/js/bootstrap-maxlength.min.js"></script>
        <script src="./assets/js/jquery.autosize.min.js"></script>
        <script src="./assets/js/chosen.jquery.min.js"></script>
        <script src="./assets/js/jquery.rtnotify.js"></script>
        <!--/ END PAGE LEVEL PLUGINS -->

        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="./assets/js/apps.js"></script>
        <script src="./assets/js/bootstrap-switch.min.js"></script>
        <script src="./assets/js/bootstrap-datepicker.js"></script>
        <script src="./assets/js/bootstrap-datepicker.pt-BR.js"></script>
        <script>
            $('.cotacao').addClass('active');
<?php if (isset($_GET['success'])): ?>
                $(document).ready(function () {
                    $.rtnotify({title: "Procedimento Realizado",
                        type: "default"});
                });
<?php endif; ?>
            $("[name='switch']").bootstrapSwitch();

            $('.delete').on('click', function () {
                var url = $(this).attr('data-url');
                $('#MODALREMOVE').modal('show');
                $('#btn-confirm-remove').on('click', function () {
                    window.location = url;
                });
            });

            $(".sound").on("click", function () {
                ion.sound.play("button_push.mp3");
            });
            
            

            $('.atualizar').on('click', function () {
        
                var id = $(this).attr('data-update');
                var data_cad = $(this).attr('data-date');
                
                
                $('#cadastro').val(id);
                $('#modalid').addClass('modalid-'+id);
      
                $('.modalid-'+id+'  #cadastro_data').addClass('cadastro_data_id'+id);
                $('.cadastro_data_id'+id).val(data_cad);
              
                $('.cadastro_data_id'+id).datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true,
                        language: "pt-BR"
                        
                });
                
              
                
                var url = "cotacao_fn.php?acao=Json";
                $.getJSON(url, {cotacao_id: id}, function (data) {
                   // console.log(data);   
                    $('.modalid-'+id+'  #cotacao_id').val(data.cotacao_id);
                    $('.modalid-'+id+'  #cotacao_nome').val(data.cotacao_nome);
                    $('.modalid-'+id+'  #cotacao_email').val(data.cotacao_email);
                    $('.modalid-'+id+'  #cotacao_celular').val(data.cotacao_celular);
                    $('.modalid-'+id+'  #cotacao_placa_veiculo').val(data.cotacao_placa_veiculo);
                    $('.modalid-'+id+'  #cotacao_logradouro').val(data.cotacao_logradouro);
                    $('.modalid-'+id+'  #cotacao_observacao').val(data.cotacao_observacao);
                    $('.modalid-'+id+'  #cotacao_placa_veiculo').val(data.cotacao_placa_veiculo);
                    $('.modalid-'+id+'  #cotacao_status_atendimento').val(data.cotacao_status_atendimento);
                    $('.modalid-'+id+'  #cotacao_cadastro').val(data.cotacao_cadastro);
                   
                });
                $('.modalid-'+id).modal('show');
            });



            
         
        </script>
    </body>
</html>