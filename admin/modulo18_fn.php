<?php
/*
 * @author phpstaff.com.br
 */
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}

function atualizar() {
    $a = new Modulo18();
    $a->modulo18_nome = addslashes($_POST['modulo18_nome']);
    $a->modulo18_subtitulo = addslashes($_POST['modulo18_subtitulo']);
    $a->modulo18_button = addslashes($_POST['modulo18_button']);
    $a->modulo18_status = intval($_POST['modulo18_status']);
    $a->modulo18_envios = intval($_POST['modulo18_envios']);
    $a->modulo18_id = intval($_POST['modulo18_id']);
    if (isset($_FILES['modulo18_imagem']['name']) && !empty($_FILES['modulo18_imagem']['name'])) {
        $a->removerArquivo();
        $a->enviar();
    }
    $a->atualizar();
    Filter :: redirect("frontend/?success");
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}



