<?php
require_once './loader.php'; ?>
<?php 
$titulo_pagina = stripslashes($sobre->modulo3_nome);
$imagem_pagina =  Validacao::getBaseUrl()."/thumb.php?w=200&src=images/".$sobre->modulo3_imagem;

$descricao_pagina = Validacao::cut(stripslashes($sobre->modulo3_descricao), 190, ' ...');

require_once './header.php';
?>
<body class="js">

	<!-- Preloader -->
	 <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
	<!-- End Preloader -->
	
<?php require_once './menu.php'; ?>
      
<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($sobre->modulo3_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($sobre->modulo3_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($menu->modulo2_nome1) ?></h2>
					<ul>
						<li><a href="home/"><?= stripslashes($menu->modulo2_nome) ?></a></li>
						<li class="active"><a href="sobre/"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->   
    
  
 <section class="section single" style="padding-top: 70px;"> 

  <!-- In�cio Publicidade Top -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_top)) : ?>
   <section  class="ads_top_sub">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_top) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Top -->      
 <section id="about-me" class="about-me">
		<div class="container">
            <div class="row">
				<div class="col-md-4 col-sm-12 col-xs-12 wow fadeInUp">
					<!-- My Image -->
					<div class="my-image">
						<img src="thumb.php?w=360&zc=0&src=images/<?= $sobre->modulo3_foto ?>" alt="<?= stripslashes($sobre->modulo3_nome) ?>">
						<a href="images/<?= $sobre->modulo3_foto ?>"><i class="fa fa-search"></i></a>
					</div>		
					<!--/ End My Image -->
					<!-- Social -->
					<ul class="social">
						<?php if (isset($social->db->data[0])): ?>
                        <?php foreach ($social->db->data as $redes): 
								if($redes->social_nome == "fa fa-youtube") {
									$redes->social_nome = "fa fa-youtube-play";
								}
						?>
                            <li><a href="<?= Filter::UrlExternal($redes->social_url) ?>"  target="_blank" class="tips"><i class="<?= stripslashes($redes->social_nome) ?>"></i></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
					</ul>
					<!--/ End Social -->
				</div>
				<div class="col-md-8 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInRight;">
					<div class="about-text">
						<h3><?= stripslashes($sobre->modulo3_nome) ?></h3>
						<?= stripslashes($sobre->modulo3_descricao)?>
					</div>	
			  <hr>
              
               <?php require_once "shared_buttons.php"; ?>
              
           
            
				</div>
			</div>
        </div>
       	</section>    
      <!-- In�cio Publicidade Bottom -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_bottom)) : ?>
   <section  class="ads_bottom">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_bottom) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Bottom -->


    <!-- Start Team -->
    <?php if ($modulo13->modulo13_status == 1): ?>

        <?php 
            $equipe = new Equipe();
            $equipe->getEquipes();
        ?>
    <?php if (isset($equipe->db->data[0])): ?>
    <section id="equipe" class="team section">
        <div class="container">
        <?php if(!empty($modulo13->modulo13_nome) || !empty($modulo13->modulo13_descricao)) {?>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                    <div class="section-title">
                        <h2><?= stripslashes($modulo13->modulo13_nome) ?></h2>
                        <p><?= stripslashes($modulo13->modulo13_descricao) ?></p>
                    </div>
                </div>
            </div>
		<?php } ?>	
            <div class="row">
               <?php foreach ($equipe->db->data as $membros): ?>
                <div class="col-md-3 col-sm-3 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
					<!-- Single Team -->
                    <div class="single-team">
						<div class="team-head">
							<img src="thumb.php?w=270&h=300&zc=0&src=images/team/<?= $membros->equipe_imagem ?>" alt="<?= stripslashes($membros->equipe_nome) ?>">
					        <div class="hover-team">
								<p class="team-text"><?= stripslashes($membros->equipe_descricao) ?></p>
								<ul class="team-social">
                                <?php if ($membros->equipe_link1): ?>
                                    <li><a href="mailto:<?= $membros->equipe_link1 ?>" target="_blank"><i class="fa fa-envelope"></i></a></li>
                                     <?php endif; ?>
                                  <?php if ($membros->equipe_link2): ?>
                                    <li><a href="<?= Filter::UrlExternal($membros->equipe_link2) ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                  <?php endif; ?>
                                  <?php if ($membros->equipe_link3): ?>
                                   <li><a href="<?= Filter::UrlExternal($membros->equipe_link3) ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                  <?php endif; ?>
                                  <?php if ($membros->equipe_link4): ?>
                                   <li><a href="<?= Filter::UrlExternal($membros->equipe_link4) ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                  <?php endif; ?>                                    
								</ul>
							</div>
						</div>
						<div class="member-name">
							<h4><?= stripslashes($membros->equipe_nome) ?></h4>
							<p><?= stripslashes($membros->equipe_subtitulo) ?></p>
						</div>
                    </div>                   
					<!--/ End Team -->
               </div>
          <?php endforeach; ?> 
            </div>
        </div>
    </section>
    <?php endif; ?>
  <?php endif; ?>    
    <!--/ End Team -->

   	</section>    
         



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>	
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
      <!-- Custom  -->
    
  <script>
   $('li#sobre').addClass('current');
  </script> 
</body>
</html>          