<!-- facebook funs box tpl -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	<!-- Start Header -->
   <?php     $base_url = getUrl(); ?>
<header class="header clearfix" id="header" >     
  
    
    <!-- Header Inner -->
      
  <?php  if ($base_url == 'index.php') { ?>
    <div class="header-inner"  style="width: 100%; position: fixed;">  
  <?php } else{ ?>
    <div class="header-inner" style="width: 100%;">
  <?php  } ?>

		<!-- Start Topbar -->
        <div class="topbar">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
						<!-- Address -->
                        <ul class="address">
                            <?php if (!empty($contato->contato_telefone1)):?><li><i class="fa fa-phone"></i><?= stripslashes($contato->contato_telefone1) ?></li><?php endif; ?> 
                            <?php if (!empty($contato->contato_horario)):?><li><i class="fa fa-clock-o"></i><?= stripslashes($contato->contato_horario) ?></li><?php endif; ?> 
                        </ul>
						<!--/ End Address -->
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
						<!-- Social -->
						<ul class="social">
						<?php if (!empty($contato->contato_email)):?>
                        <li><a href="mailto:<?=stripslashes($contato->contato_email)?>"><i class="fa fa-send"></i><?=stripslashes($contato->contato_email)?></a></li>
                        <?php endif; ?> 
					
                    <?php 
                   $social = new Social();
                   $social->getSociaistatus();
                    if (isset($social->db->data[0])): ?>
                        <?php foreach ($social->db->data as $redes): 
							if($redes->social_nome == "fa fa-youtube") {
								$redes->social_nome = "fa fa-youtube-play";
							}
						?>
                            <li><a href="<?= Filter::UrlExternal($redes->social_url) ?>"  target="_blank" class="tips"><i class="<?= stripslashes($redes->social_nome) ?>"></i></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                         </ul>
						<!-- End Social -->
                    </div>
                </div>
            </div>
        </div>
		<!--/ End Topbar -->
			
        
			<div class="container">
				<div class="row">
					<div class="col-md-2 col-sm-2 col-xs-12">
						<!-- Logo -->
						<div class="logo">
	                     <a title="<?= $site->site_meta_titulo ?>" href="<?= Validacao::getBaseUrl() ?>/">
                            <img src="images/<?= $modulo_aparencia->modulo_aparencia_logo ?>" alt="<?= $site->site_meta_titulo ?>"/>
						</a>
						</div>
						<!--/End Logo -->
						
						<!-- Mobile Menu -->
						<div class="mobile-menu"></div>	
						<!--/ End Mobile Menu -->
					</div>
					<div class="col-md-10 col-sm-10 col-xs-12">
						<!-- Main Menu -->
						<div class="main-menu">
							<!-- Cart Bar -->
							<ul class="cart-search-bar"  style="display: none;">
								<li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
							</ul>
							<!--/ End Cart Bar -->
							
							<!-- Search Form -->
							<form class="search-form">
								<input type="text" placeholder="escreva aqui..." name="search" style="text-transform: none;">
								<input type="submit" value=" Ok ">
							</form>
							<!--/ End Search Form -->
                            
							
							
							<!-- Navigation -->
							<nav class="navbar">
								<div class="collapse navbar-collapse">
									<ul class="nav menu navbar-nav">
										<li id="index">
                                        <?php if(isset($home) && $home == 1) {?>
                                        <a href="<?= Validacao::getBaseUrl() ?>/#home" class="firstLevel last" id="index"><?= stripslashes($menu->modulo2_nome) ?></a>
                                        <?php } else {?>
                                         <a href="<?= Validacao::getBaseUrl() ?>" class="firstLevel last" id="index"><?= stripslashes($menu->modulo2_nome) ?></a>
                                         <?php } ?>
										</li>
                                        <?php if ($modulo3->modulo3_status == 1) : ?>
										<li id="sobre"><a href="sobre/" class="firstLevel last" id="sobre"><?= stripslashes($menu->modulo2_nome1) ?></a></li>
                                        <?php endif; ?>
                                        
                                        <?php if ($modulo17->modulo17_status == 1): ?>
                                        <?php
                                        $categories = new Area3();
                                        $categories->db = new DB;
                                        $categories->getAreasPai();
                                        ?>
                                        <?php if (isset($categories->db->data[0])): ?>
                                         <?php foreach ($categories->db->data as $catpai): ?>     
                                             <li id="paginas_<?= $catpai->area3_id ?>"><a href="categoria/paginas/<?= Filter::slug2($catpai->area3_nome) ?>/<?= $catpai->area3_id ?>/"><?=stripslashes($catpai->area3_nome)?></a>
                                             <?php 
                                                $area3 = new Area3();
                                                $area3->db = new DB;
                                                $area3->area3_id = $catpai->area3_id;
                                                $area3->getAreasSub();
                                            ?> 
                                            <?php if (isset($area3->db->data[0])): ?>
                                               <ul class="dropdown">
                                                 <?php foreach ($area3->db->data as $catsub): ?>
                                                   <li class="firstLevel last"><a href="categoria/paginas/<?= Filter::slug2($catsub->area3_nome) ?>/<?= $catsub->area3_id ?>/"><?=stripslashes($catsub->area3_nome)?></a></li>
                                                     <?php endforeach; ?>
                                               </ul>
                                            <?php endif; ?>
                                                   </li>
										   <?php endforeach; ?>
                                         <?php endif; ?>                   
                                        <?php endif; ?>
                                        
                                        <?php if ($modulo6->modulo6_status == 1) : ?>
										<li><a href="#services"><?= stripslashes($menu->modulo2_nome6) ?></a></li>
                                        <?php endif; ?>
                                       
                                        <?php if ($modulo7->modulo7_status == 1) : ?>
                                        <?php if(isset($home) && $home == 1) {?>
										<li><a href="#projetos"><?= stripslashes($menu->modulo2_nome2) ?></a></li>
                                        <?php } else {?>
                                        <li id="projeto"><a href="projetos/"><?= stripslashes($menu->modulo2_nome2) ?></a></li>
                                        <?php } ?>
                                        
                                        <?php endif; ?> 
                                        
                                        <?php if ($modulo13->modulo13_status == 1) : ?>
										<li><a href="#equipe"><?= stripslashes($menu->modulo2_nome7) ?></a></li>
                                        <?php endif; ?> 
                                        
                                        <?php if ($modulo10->modulo10_status == 1) : ?>
                                        <li id="posts"><a href="<?php if(isset($home) && $home == 1) { echo "#blog"; } else { echo "blog/"; } ?>"><?= stripslashes($menu->modulo2_nome3) ?></a>
                                        <?php
                                        $categoria_blog = new Area();
                                        $categoria_blog->db = new DB;
                                        $categoria_blog->getAreas();
                                        ?>
									    <?php if (isset($categoria_blog->db->data[0])): ?>
											<ul class="dropdown">
                                             <?php foreach ($categoria_blog->db->data as $cat_blog): ?>   
                                             <li id="posts<?= $cat_blog->area_id ?>"><a href="categoria/blog/<?= Filter::slug2(trim($cat_blog->area_nome)) ?>/<?= $cat_blog->area_id ?>/" class="firstLevel last" id="posts<?= $cat_blog->area_id ?>"><?= stripslashes($cat_blog->area_nome) ?></a></li>
												<?php endforeach; ?>
											</ul>
                                        <?php endif; ?>      
										</li>
                                        <?php endif; ?>
                                        
                                        
                                        <?php if ($modulo12->modulo12_status == 1) : ?>
										<li id="video"><a href="<?php if(isset($home) && $home == 1) { echo "#videos"; } else { echo "videos/"; } ?>"><?= stripslashes($menu->modulo2_nome4) ?></a></li>
                                        <?php endif; ?> 
                                        
                                        									
									   <!-- <li><a href="<?= Validacao::getBaseUrl() ?>/#contact"><?= stripslashes($menu->modulo2_nome5) ?></a></li> -->

                                         <?php if ($modulo9->modulo9_status == 1) : ?>
                    <li id="contato"><a href="contato/" class="firstLevel last" id="contato"><?= stripslashes($menu->modulo2_nome5) ?></a></li>
                                        <?php endif; ?>

									</ul>
								</div>
							</nav>
							<!--/ End Navigation -->
						</div>
						<!--/ End Main Menu -->
					</div>
				</div>
			</div>
        </div>
		<!--/ End Header Inner -->
    </header>
    <!--/ End Header