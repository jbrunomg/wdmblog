<?php

require_once 'Controle.php';

class Cotacao extends Controle {

    public $db;

   public $cotacao_id;
   public $cotacao_nome;
   public $cotacao_email;
   public $cotacao_celular;
   public $cotacao_placa_veiculo;
   public $cotacao_logradouro;
   public $cotacao_status_atendimento;
   public $cotacao_observacao;
   public $cotacao_cadastro;
   public $cotacao_atualizacao;

    public function __construct() {
        parent::__construct();
        $this->data = date('Y-m-d');
    }
    

    public function incluir() {
        $this->insert("cotacao", "cotacao_nome,  cotacao_email, cotacao_celular,  cotacao_placa_veiculo, cotacao_logradouro,  cotacao_status_atendimento, cotacao_observacao, cotacao_cadastro ", "'$this->cotacao_nome','$this->cotacao_email','$this->cotacao_celular','$this->cotacao_placa_veiculo','$this->cotacao_logradouro','$this->cotacao_status_atendimento','$this->cotacao_observacao','$this->cotacao_cadastro'");
    }
    
    public function atualizar() {
        $query = "cotacao_nome = '$this->cotacao_nome', cotacao_email = '$this->cotacao_email', cotacao_celular = '$this->cotacao_celular', cotacao_placa_veiculo = '$this->cotacao_placa_veiculo', cotacao_logradouro = '$this->cotacao_logradouro', cotacao_status_atendimento = '$this->cotacao_status_atendimento', cotacao_observacao = '$this->cotacao_observacao' ";
        // var_dump($query.''.$this->cotacao_id);die();
      
        $this->update("cotacao", "$query", "cotacao_id = '$this->cotacao_id'");
    }

    public function getCotacao() {
        $this->select("cotacao", "", "*", "", "order by cotacao_id DESC", "");
    }

    public function remover() {
        $this->delete("cotacao", "cotacao_id = $this->cotacao_id");
    }

    
    public function getCadastros($cotacao_id) {
        $this->select("cotacao", "", "*", "", " WHERE cotacao_id = $cotacao_id", "");
    }
    
    public function getCotacaoPlaca() {
        $this->select("cotacao", "", "*", "", " WHERE cotacao_placa_veiculo = '".$this->cotacao_placa_veiculo."'", "");
        
    }

    public function mudarStatus($cotacao_id, $cotacao_status_atendimento) {
        $this->Moderar("cotacao", "cotacao_status_atendimento", "cotacao_id", "$cotacao_id", "$cotacao_status_atendimento");
    }

    public function Contar($id) {
        $this->getCount("cotacao", "");
    }
    
    public function JSON() {
        $this->getJSON("cotacao", "cotacao_id = '$this->cotacao_id'");
    }

}