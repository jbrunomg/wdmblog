<?php
require_once './loader.php';

$portfolio_id = intval($_GET['id']);
$cat_portfolio = new Area1();
$cat_portfolio->getAreas1();

$projeto = new Portfolio();
$projeto->portfolio_id = "$portfolio_id";
$projeto->getBy();

$titulo_pagina = stripslashes($projeto->portfolio_nome);
$imagem_pagina = Validacao::getBaseUrl()."/thumb.php?h=250&src=images/portfolio/".$projeto->portfolio_imagem;

require_once './header.php';
?>
<body class="js">

	<!-- Preloader -->
	 <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
	<!-- End Preloader -->
	
<?php require_once './menu.php'; ?>



<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($modulo7->modulo7_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($modulo7->modulo7_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($projeto->portfolio_nome) ?></h2>
					<ul>
						<li><a href="index.html">Home</a></li>
                         <li><a href="projetos/"><?= stripslashes($modulo7->modulo7_nome) ?></a></li>
						 <li class="active"><a href="projeto/<?= Filter::slug2($projeto->portfolio_nome) ?>/<?= $projeto->portfolio_id ?>/"><?= stripslashes($projeto->portfolio_nome) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->

<!-- In�cio Publicidade Top -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_top)) : ?>
   <section  class="ads_top" style="padding-top: 30px;">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_top) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Top -->
    

   <?php if (isset($projeto->db->data[0])) { ?>	
<?php
$projeto = new Portfolio();
$projeto->portfolio_id = "$portfolio_id";
$projeto->getBy();
?>
	<!-- Project Project -->
    <section class="project single section">
    
		<div class="container">
			<div class="row">

				<div class="col-md-6 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-delay="0.4s">
					<!-- Project Slide -->
					<div class="project-slide">
                                                      
                        <?php if(isset($projeto->portfolio_imagem)): ?>              
						<div class="single-project">
							<div class="project-head">
								<img src="thumb.php?w=555&zc=0&src=images/portfolio/<?= $projeto->portfolio_imagem ?>" alt="<?= stripslashes($projeto->portfolio_nome) ?>">			
								<div class="project-hover">
									<h4><a href="images/portfolio/<?= $projeto->portfolio_imagem ?>"><?= stripslashes($projeto->portfolio_nome) ?></a></h4>
									<p><?= stripslashes(Validacao::cut($projeto->portfolio_descricao, 150, '...')) ?></p>
								</div>
							</div>
						</div>
                        <?php endif;?>      
              <?php foreach ($projeto->db->data as $slide): ?> 
              
                <?php if ($slide->foto_url != ""): ?>
						<div class="single-project">
							<div class="project-head">
								<img src="thumb.php?w=555&zc=0&src=images/portfolio/<?= stripslashes($slide->foto_url) ?>" alt="<?= stripslashes($projeto->portfolio_nome) ?>">			
								<div class="project-hover">
									<h4><a href="images/portfolio/<?= $slide->foto_url ?>"><?= stripslashes($projeto->portfolio_nome) ?></a></h4>
									<p><?= stripslashes(Validacao::cut($projeto->portfolio_descricao, 150, '...')) ?></p>
								</div>
							</div>
					</div>
                    <?php endif;?> 
              <?php endforeach; ?>
                    </div>
					<!--/ End Porject Slide -->
                </div>	
				<div class="col-md-6 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0.4s">
					<!-- Project Description -->
					<div class="project-descreption">
						<h2><?= stripslashes($projeto->portfolio_nome) ?></h2>
						<p><?= stripslashes($projeto->portfolio_descricao) ?></p>
						<ul class="project-info">
                        <?php if(!empty($projeto->portfolio_cliente) || !empty($projeto->portfolio_url)) : ?>
							<li><i class="fa fa-angle-double-right"></i><span>Cliente:</span><a href="<?php if(!empty($projeto->portfolio_url)) { echo stripslashes($projeto->portfolio_url); } ?>">
                            <?php if(empty($projeto->portfolio_cliente)) { echo stripslashes($projeto->portfolio_url); } else { echo stripslashes($projeto->portfolio_cliente); }  ?></a></li>
					         <?php endif; ?> 
                             
							<li><i class="fa fa-angle-double-right"></i><span>Data:</span><?=stripslashes($projeto->portfolio_data)?></li>
						</ul>
					</div>
					<!--/ End Project Description -->
				</div>
			</div>
			
            <?php if(!empty($projeto->portfolio_url)) : ?>
			<div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
					<!-- Button -->
                    <div class="button">
                        <a href="<?=stripslashes($projeto->portfolio_url)?>" target="_blank" class="btn"><i class="fa fa-send"></i>Visite o Projeto</a>
                    </div>
					<!--/ End Button -->
                </div>
            </div>
		<?php endif; ?>
        
        	<?php    $proj = new Portfolio();
                     $proj->portfolio_area1 = $projeto->portfolio_area1;
                     $proj->portfolio_id = "$portfolio_id";
                     $proj->getPortfoliosArea1();
                  if (isset($proj->db->data[0])): ?>	
			<div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
					<!-- Related Project -->
					<div class="project-related">
						<h4>Projetos Relacionados</h4>
						<div class="related-slide">
				
                     <?php foreach ($proj->db->data as $trabalhos): ?>
  
                            <!-- Single Project -->
							<div class="single-project">
								<div class="project-head">
									<img src="thumb.php?w=285&h=285&zc=0&src=images/portfolio/<?= $trabalhos->portfolio_imagem ?>" alt="<?= stripslashes($trabalhos->portfolio_nome) ?>">			
									<div class="project-hover">
										<h4><a href="projeto/<?= Filter::slug2($trabalhos->portfolio_nome) ?>/<?= $trabalhos->portfolio_id ?>/"><?= stripslashes($trabalhos->portfolio_nome) ?></a></h4>
										<p><a href="projeto/<?= Filter::slug2($trabalhos->portfolio_nome) ?>/<?= $trabalhos->portfolio_id ?>/"><?= stripslashes(Validacao::cut($trabalhos->portfolio_descricao, 150, '...')) ?></a></p>
									</div>
								</div>
							</div>
							<!--/ End Single Project -->
                         <?php endforeach; ?>   
                            
						</div>
                    </div>
					<!-- End Related Project -->
                </div>	
            </div>
        <?php endif; ?>    
		</div>
            
 <!-- In�cio Publicidade Bottom -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_bottom)) : ?>
   <section  class="ads_bottom">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_bottom) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Bottom -->
	</section>
	<!--End Project Project -->



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
      <!-- Custom  -->
    
  <script>
  $('li#projeto').addClass('current');
  $('li#projeto').click(function(){
   window.location.href='projetos/';
})
  </script> 
</body>
</html>   
<?php } else {
  header("Location: ".Validacao::getBaseUrl()."/404") ;
    } 
    
?> 