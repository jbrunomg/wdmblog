<?php 
$pagina->getPaginasRight(); 
                
?>  
  <?php if (isset($pagina->db->data[0])) : ?>

<div class="single-sidebar latest">
 <h2> <?= stripslashes($modulo17->modulo17_nome) ?> Recentes</h2>
      <?php foreach ($pagina->db->data as $p): ?>
		<!-- Single Post -->
	<div class="single-post">
    <?php if(!empty($p->paginas_imagem)): ?>
		<div class="post-img">
			<img src="thumb.php?w=60&amp;h=60&amp;zc=1&amp;src=images/paginas/<?=$p->paginas_imagem?>" alt="<?=stripslashes($p->paginas_nome)?>">
		</div>
     <?php endif; ?>
	<div class="post-info">
		<h4><a href="pagina/<?= Filter::slug2($p->paginas_nome) ?>/<?= $p->paginas_id ?>/"><?= Validacao::cut(stripslashes($p->paginas_nome), 61, '...') ?> </h4>
		<p><?= Validacao::cut(stripslashes($p->paginas_descricao), 64, '...') ?></p></a>
	</div>
  </div>
<!--/ End Single Post -->
  <?php endforeach; ?>
</div> 
  <?php endif; ?>  