<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content='codeglim'>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php require_once './base.php';?>
	<!-- Title Tag-->
    <title><?php if(isset($titulo_pagina)) { echo $titulo_pagina; } else { echo $site->site_meta_titulo; } ?></title>
    <meta name="description" content="<?php if(isset($descricao_pagina)) { echo $descricao_pagina; } else { echo $site->site_meta_desc; } ?>">
    <meta name="keywords" content="<?php if(isset($meta_palavra)) { echo $meta_palavra; } else { echo $site->site_meta_palavra; } ?>">
    <meta name="author" content="<?= $site->site_meta_autor ?>">
	
	<!-- FavIcon -->
	<link rel="icon" type="image/png" href="admin/assets/img/ico/<?= $modulo_aparencia->modulo_aparencia_favicon ?>">
    
    <!-- Facebook Comment Plugin -->
     
      <!-- START - Facebook Open Graph, Google+ and Twitter Card Tags 2.0.4 -->
    <!-- Facebook Open Graph -->
    <meta property="og:locale" content="pt-BR" />
    <meta property="og:site_name" content="<?php if(isset($titulo_pagina)) { echo $titulo_pagina; } else { echo $site->site_meta_titulo; } ?>" />
    <meta property="og:title" content="<?php if(isset($titulo_pagina)) { echo $titulo_pagina; } else { echo $site->site_meta_titulo; } ?>" />
    <meta property="og:url" content="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" />
    <meta property="og:image" content="<?php if(isset($imagem_pagina)) { echo $imagem_pagina; } else { echo Validacao::getBaseUrl()."/thumb.php?h=65&src=images/".$modulo_aparencia->modulo_aparencia_logo; } ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="<?php if(isset($descricao_pagina)) { echo $descricao_pagina; } else { echo $site->site_meta_desc; } ?>" />
    <meta property="og:image:type" content="image/jpeg"/>
    <!-- Google+ / Schema.org -->
    <meta itemprop="name" content="<?php if(isset($titulo_pagina)) { echo $titulo_pagina; } else { echo $site->site_meta_titulo; } ?>" />
    <meta itemprop="description" content="<?php if(isset($titulo_pagina)) { echo $titulo_pagina; } else { echo $site->site_meta_titulo; } ?>" />
    <!-- Twitter Cards -->
    <meta name="twitter:title" content="<?php if(isset($titulo_pagina)) { echo $titulo_pagina; } else { echo $site->site_meta_titulo; } ?>" />
    <meta name="twitter:url" content="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" />
    <meta name="twitter:description" content="<?php if(isset($descricao_pagina)) { echo $descricao_pagina; } else { echo $site->site_meta_desc; } ?>" />
    <meta name="twitter:card" content="summary_large_image" />
    <!-- SEO -->
    <!-- Misc. tags -->
    <!-- END - Facebook Open Graph, Google+ and Twitter Card Tags 2.0.4 -->
	
	<!-- Google Fonts & Google Maps API -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600,700" rel="stylesheet"> 
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnhgNBg6jrSuqhTeKKEFDWI0_5fZLx0vM" type="text/javascript"></script>	
	
	<!-- Hover  Css -->
    <link rel="stylesheet" href="css\hover.min.css"/>
	
	<!-- Mobile Nav -->
    <link rel="stylesheet" href="css\slicknav.min.css"/>	
	
	<!-- Font Awesome -->
    <link rel="stylesheet" href="css\font-awesome.min.css"/>
	
	<!-- Animate Min -->
    <link rel="stylesheet" href="css\animate.min.css"/>
    <link rel="stylesheet" href="css/owl.carousel.css" />
    <link rel="stylesheet" href="css/owl.transitions.css" />
	
	<!-- Magnific Popup -->
    <link rel="stylesheet" href="css\magnific-popup.css"/>
	
	<!-- Slick Slider -->
    <link rel="stylesheet" href="css\slick.css"/>
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="css\bootstrap.min.css"/>
	
	<!-- Stylesheet -->
	<link rel="stylesheet" href="css\reset.css"/>
    <link rel="stylesheet" href="css\loaders.css"/>
    <link rel="stylesheet" href="style.css"/>
    <link rel="stylesheet" href="css\responsive.css"/>
    <link rel="stylesheet" href="css\shared_buttons.css"/>
	
	<!-- Color -->
    <!--<link rel="stylesheet" href="css/skin/orange.css">-->
    <!--<link rel="stylesheet" href="css/skin/red.css">-->
    <!--<link rel="stylesheet" href="css/skin/green.css">-->	
    <!--<link rel="stylesheet" href="css/skin/purple.css">-->	
    <!--<link rel="stylesheet" href="css/skin/pink.css">-->	
    <!--<link rel="stylesheet" href="css/skin/blaze.css">-->
    <!--<link rel="stylesheet" href="css/skin/blue.css">-->	
    <!--<link rel="stylesheet" href="css/skin/blue2.css">-->	
    <!--<link rel="stylesheet" href="css/skin/cherry.css">-->
    <!--<link rel="stylesheet" href="css/skin/yellow.css">-->
    <!--<link rel="stylesheet" href="css/skin/gold.css">-->
    <!--<link rel="stylesheet" href="css/skin/coffee.css">-->
    <!--<link rel="stylesheet" href="css/skin/bordo.css">-->
    <!--<link rel="stylesheet" href="css/skin/dark.css">-->
    <!--<link rel="stylesheet" href="css/skin/royal.css">-->
    <!--<link rel="stylesheet" href="css/skin/red_blue.css">-->
    
    <link rel="stylesheet" href="css\skin\<?= $modulo_aparencia->modulo_aparencia_cor ?>.css">	
	
    <!--[if lt IE 9]>
           <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
     <![endif]-->
  
     
	<link rel="stylesheet" href="#" id="colors">
    <style>
	
.header .header-inner .logo {
    padding: 4px 0;
}
.header .header-inner.sticky .logo {
    padding: 4px 0;
}
.header .mobile-menu {
    float: right;
    position: absolute;
    width: 95%;
    top: 30px;
    right: 10px;
    z-index: 99999999;
}
.newsblog .news-content .date {
    color: #888;
  }
  .ipadding-left {
    padding-left: 8px;
  }
  .newsblog .news-content .datesmall {
    font-size: 12px
  }
  .newsblog .news-content h4 a {
    text-transform: none;
  }
  .project .project-hover p a {
    color: #fff;
}
.ads_top_home { margin: 70px 0 0 0;}
.ads_top { margin: 40px 0 0 0;}
.ads_top_sub { margin: 0 0 30px 0;}
.ads_bottom { margin: 70px 0 0 0;}
.ads_bottom_sub { margin: 0 0 70px 0;}



#instagram a {
  display: inline-block;
  width: 12%;
  height: 0;
  padding: 12%;
  background: #C0C0C0 50% 50% no-repeat;
  background-size: cover;
  margin: -3px 1px;
  -webkit-transition: all 0.4s ease;
  -moz-transition: all 0.4s ease;
  transition: all 0.4s ease;
}
#instagram a:hover{
	-webkit-transform:scale(1.2);
	-moz-transform:scale(1.2);
	transform:scale(1.2);
} 

.sufia-slider .single-slider:before{
	background:rgba(0, 0, 0, .<?= $modulo_aparencia->modulo_aparencia_pelicula ?>);
}  

.widget {
    
    display: inline-block;
    width: 100%;
}
.widget .w-tags a {
    float: left;
    margin-right: 10px;
    margin-bottom: 10px;
    padding: 5px 10px;
    color: #69696E;
    border: 1px solid #d3d3d3;
    border-radius: 4px;
}
.goog-te-banner-frame.skiptranslate {display: none !important;}
body { top: 0px !important; }

.header .nav li {
    margin-right: 4px;
    position: relative;
}

._4t2a {
    display:none;
}
    </style>
  
  <?php 
   if(!empty($site->site_analytics)) {
   require_once './analytics.php'; 
  }?>

  <?php 
   if(!empty($site->site_pixel)) {
   require_once './pixel.php'; 
  }?>
  
</head>