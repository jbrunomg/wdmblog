<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class Modulo18 extends Controle {

    public $db;
    public $modulo18_id;
    public $modulo18_nome;
    public $modulo18_subtitulo;
    public $modulo18_button;
    public $modulo18_status;
    public $modulo18_paginacao;
    public $modulo18_imagem;
    public $result;

    public function __construct() {
        parent::__construct();
        require_once 'Registry.php';
        $registry = Registry::getInstance();
        if( $registry->get('db') == false ) {
            $registry->set('db', new DB);
        }
        $this->db = $registry->get('db');           
    }

    public function atualizar() {
        if ($this->modulo18_imagem != "") {
            $this->update("modulo18", "modulo18_nome  = '$this->modulo18_nome', modulo18_subtitulo = '$this->modulo18_subtitulo',  modulo18_button = '$this->modulo18_button',"
                    . " modulo18_status = '$this->modulo18_status', modulo18_imagem = '$this->modulo18_imagem',"
                    . " modulo18_envios = '$this->modulo18_envios'", "modulo18_id = '$this->modulo18_id'");
        } else {
            $this->update("modulo18", "modulo18_nome  = '$this->modulo18_nome', modulo18_subtitulo = '$this->modulo18_subtitulo',  modulo18_button = '$this->modulo18_button',"
                    . " modulo18_status = '$this->modulo18_status', "
                    . " modulo18_envios = '$this->modulo18_envios'", "modulo18_id = '$this->modulo18_id'");
        }
    }

    public function removerArquivo() {
        $this->deleteArquivo("modulo18", "modulo18_id = '$this->modulo18_id'", "modulo18_imagem", "../images/");
    }

    public function enviar() {
        $this->upload("../images/", "modulo18_imagem", "");
    }

    public function getModulo18() {
        $this->select("modulo18", "", "*", "", "WHERE modulo18_id = 1", "");
    }

}