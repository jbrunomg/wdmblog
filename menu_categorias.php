<style>
.categorias ul li {
    font-size: 16px;
    margin-bottom: 12px;
    padding-left: 14px;
}
.categorias ul li.active {
    background:#F4F4F4;
}

.categorias li ul li {
    padding-left: 18px;
}



</style>
<div class="single-sidebar categorias">
	<h2><?= stripslashes($modulo17->modulo17_nome) ?> <span>Categorias</span></h2>
	 <ul>
		<?php 
        $cat_page = new Area3();
        $cat_page->db = new DB; 
        $cat_page->getAreasPai(); 
        
        ?>
        <?php if (isset($cat_page->db->data[0])){ ?>
            <?php foreach ($cat_page->db->data as $cp) { ?>
              <li <?php if(intval($_GET['id']) ==  $cp->area3_id) { echo 'class="active"'; }?>><a href="categoria/paginas/<?= Filter::slug2($cp->area3_nome) ?>/<?= $cp->area3_id ?>/"><i class="fa fa-angle-right"></i> <?= $cp->area3_nome ?></a> 
              <?php 
              $areas3 = new Area3();
              $areas3->db = new DB;
              $areas3->area3_id = $cp->area3_id;
              $areas3->getAreasSub();
              if (isset($areas3->db->data[0])) { ?>
              <!-- <a href="#area3_<?= $cp->area3_id ?>" data-toggle="collapse"><i class="fa fa-angle-down"></i></a> -->
              <li id="area3_<?= $cp->area3_id ?>" class="collapse in">
              <ul>
                 <?php foreach ($areas3->db->data as $cpsub){ ?>
                  <li <?php if(intval($_GET['id']) ==  $cpsub->area3_id) { echo 'class="active"'; }?>><a href="categoria/paginas/<?= Filter::slug2($cpsub->area3_nome) ?>/<?= $cpsub->area3_id ?>/"><i class="fa fa-angle-double-right"></i> <?= $cpsub->area3_nome ?></a></li>
               <?php } ?>
              </ul>
               </li>
             <?php } ?>   
              </li>

          <?php } ?>
        <?php } ?>
            <li><a href="paginas/">Todas as Categorias</a></li>			
	 </ul>
</div>