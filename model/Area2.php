<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class Area2 extends Controle {

    public $db;
    public $area2_id;
    public $area2_nome;
    public $result;

    public function __construct() {
        parent::__construct();
    }

    public function incluir() {
        $this->insert("area2", "area2_nome", "'$this->area2_nome'");
    }

    public function atualizar() {
        $this->update("area2", "area2_nome = '$this->area2_nome'", "area2_id = $this->area2_id");
    }

    public function deletar() {
        require_once '../loader.php';
        $p = new Video();
        $p->video_area2 = $this->area2_id;
        //$p->video_id = $this->area2_id;
        //$p->getBy();
        $p->db->query("SELECT * FROM area2 
                        INNER JOIN video ON (video_area2 = area2_id) 
                        WHERE area2_id = $this->area2_id")->fetchAll();
        if(isset($p->db->data[0])){
            foreach ($p->db->data as $rem) {
                $p->video_id = $rem->video_id;
                $p->remover();
            }
        }
        $this->delete("area2", "area2_id =  $this->area2_id");
    }

    public function getAreas2() {
        $this->db->query("SELECT * FROM area2  ORDER BY area2_pos ASC")->fetchAll();
        //$this->select("area2", "", "*", "", " order by area2_pos ASC", "");
    }
   
    public function getArea2($id) {
        $this->select("area2", "", "*", "", "INNER JOIN video ON (video_area2 = area2_id) WHERE video_area2 = $this->area2_id", "");
    }
    
    public function getRelacionados($id,$n) {
        $this->select("area2", "", "*", "", "INNER JOIN video ON (video_area2 = area2_id) WHERE video_area2 = $id AND video_id <> $n", "");
    }

    public function getJason() {
        $this->getJSON("area2", "area2_id = '$this->area2_id'");
    }

    public function updatePos() {        
        $item = $_POST['item'];
        if(!empty($item )){
            $this->Posicao($item, "area2", "area2_pos", "area2_id");
        }
    }

}
