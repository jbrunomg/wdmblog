﻿<?php
/*
 * @author phpstaff.com.br
 */
@session_start();
if (isset($_GET['deslogar'])) {
    $_SESSION['LOGADO'] = FALSE;
    session_destroy();
    session_start();
    $_SESSION['USER']['foto'] = "./fotos/avatar.jpg"; //anovnuous
}
if (isset($_SESSION['LOGADO']) && $_SESSION['LOGADO'] == TRUE) {
    @header('location:home/');
    exit;
}

require_once '../loader.php';
$site = new Site();
$site->getMeta();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- START @HEAD -->
    <head>
        <?php require_once './base.php';?>
        <!-- START @META SECTION -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title><?= $site->site_meta_titulo ?></title>
        <!--/ END META SECTION -->

        <!-- START @FAVICONS -->

        <link href="./assets/img/ico/<?= $modulo_aparencia->modulo_aparencia_favicon ?>" rel="shortcut icon" sizes="144x144">
        <!--/ END FAVICONS -->

        <!-- START @FONT STYLES -->
        <!--/ END FONT STYLES -->

        <!-- START @GLOBAL MANDATORY STYLES -->
        <link href="./assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- START @PAGE LEVEL STYLES -->
        <link href="./assets/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="./assets/css/animate.min.css" rel="stylesheet">
        <!--/ END PAGE LEVEL STYLES -->

        <!-- START @THEME STYLES -->
        <link href="./assets/css/reset.css" rel="stylesheet">
        <link href="./assets/css/layout.css" rel="stylesheet">
        <link href="./assets/css/components.css" rel="stylesheet">
        <link href="./assets/css/plugins.css" rel="stylesheet">
        <link href="./assets/css/themes/default.theme.css" rel="stylesheet" id="theme">
        <link href="./assets/css/sign.css" rel="stylesheet">
        <link href="./assets/css/custom.css" rel="stylesheet">
        <link href="./assets/css/jquery.noty.css" rel="stylesheet">
        <link href="./assets/css/noty_theme_default.css" rel="stylesheet">
        <!--/ END THEME STYLES -->

        <!-- START @IE SUPPORT -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="./assets/js/html5shiv.min.js"></script>
        <script src="./assets/js/respond.min.js"></script>
        <![endif]-->
        <!--/ END IE SUPPORT -->
    </head>
    <!--/ END HEAD -->
    <body>


<!--[if lt IE 9]>
        <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @SIGN WRAPPER -->
        <div id="sign-wrapper">

            <!-- Brand -->
            <div class="brand">
                <h1><p style="font-family: 'Open Sans', sans-serif;"><?= $site->site_meta_titulo ?></p></h1>
            </div>
            <!--/ Brand -->

            <!-- Register form -->
            <form class="form-horizontal rounded shadow no-overflow"  method="post" action="enviar_senha.php">
                <div class="sign-header">
                    <div class="form-group">
                        <div class="sign-text">
                            <span style="font-family: 'Open Sans', sans-serif;">Recuperar Senha</span>
                        </div>
                    </div>
                </div>
                <div class="sign-body">
                    <div class="form-group">
                        <div class="input-group input-group-lg rounded no-overflow">
                            <input id="send-email" name="send_email" type="email" placeholder="Informe o email" class="form-control email" required="required" autocomplete="off"/>
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        </div>
                    </div>
                </div>
                <div class="sign-footer">
                    <div class="form-group">
                    <input type="hidden" name="ip" value="<?=getUserIP()?>"/>
                    <button class="btn btn-theme btn-lg btn-block no-margin rounded" style="font-family: 'Open Sans', sans-serif;" id="btn-senha" name="btn-senha" type="submit">Envie a minha senha</button>
                    </div>
                </div>
                
            </form>
            <div class="panel-footer">
            <p class="text-center"><a href="./"> <strong><i class="fa fa-back"></i> Voltar para o Login</strong> </a>
            </p>
            <div style=" clear:both"></div>
            </div>
            <!--/ Register form -->
        </div><!-- /#sign-wrapper -->
        <!--/ END SIGN WRAPPER -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- START @CORE PLUGINS -->
        <script src="./assets/js/jquery.min.js"></script>
        <script src="./assets/js/jquery.easing.1.3.min.js"></script>
        <script src="./assets/ionsound/ion.sound.min.js"></script>
        <!--/ END CORE PLUGINS -->

        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="./assets/js/pages/blankon.sign.js"></script>
        <script src="./assets/js/apps.js"></script>
        <script src="./assets/js/jquery.noty.js"></script>
        <!--/ END PAGE LEVEL SCRIPTS -->
        <!--/ END JAVASCRIPT SECTION -->

        <!-- START GOOGLE ANALYTICS -->
        <script>


<?php if (isset($_GET['op']) && $_GET['op'] == "erro"): ?>
                noty({
                    text: 'Erro! Email Inválido.',
                    layout: 'top',
                    closeWith: ['click', 'hover'],
                    type: 'error'
                    
                });
<?php endif; ?>

<?php if (isset($_GET['op']) && $_GET['op'] == "erro-processar"): ?>
                noty({
                    text: 'Erro! Ocorreu um problema no sistema.',
                    layout: 'top',
                    closeWith: ['click', 'hover'],
                    type: 'error'
                    
                });
<?php endif; ?>

        </script>
        <!--/ END GOOGLE ANALYTICS -->
    </body>
    <!-- END Body -->
</html> 