<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class Modulo15 extends Controle {

    public $db;
    public $modulo15_id;
    public $modulo15_nome;
    public $modulo15_subtitulo;
    public $modulo15_button;
    public $modulo15_status;
    public $modulo15_paginacao;
    public $modulo15_imagem;
    public $result;

    public function __construct() {
        parent::__construct();
        require_once 'Registry.php';
        $registry = Registry::getInstance();
        if( $registry->get('db') == false ) {
            $registry->set('db', new DB);
        }
        $this->db = $registry->get('db');           
    }

    public function atualizar() {
        if ($this->modulo15_imagem != "") {
            $this->update("modulo15", "modulo15_nome  = '$this->modulo15_nome', modulo15_subtitulo = '$this->modulo15_subtitulo',  modulo15_button = '$this->modulo15_button',"
                    . " modulo15_status = '$this->modulo15_status', modulo15_imagem = '$this->modulo15_imagem',"
                    . " modulo15_envios = '$this->modulo15_envios'", "modulo15_id = '$this->modulo15_id'");
        } else {
            $this->update("modulo15", "modulo15_nome  = '$this->modulo15_nome', modulo15_subtitulo = '$this->modulo15_subtitulo',  modulo15_button = '$this->modulo15_button',"
                    . " modulo15_status = '$this->modulo15_status', "
                    . " modulo15_envios = '$this->modulo15_envios'", "modulo15_id = '$this->modulo15_id'");
        }
    }

    public function removerArquivo() {
        $this->deleteArquivo("modulo15", "modulo15_id = '$this->modulo15_id'", "modulo15_imagem", "../images/");
    }

    public function enviar() {
        $this->upload("../images/", "modulo15_imagem", "");
    }

    public function getModulo15() {
        $this->select("modulo15", "", "*", "", "WHERE modulo15_id = 1", "");
    }

}