<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class Video extends Controle {

    public $db;
    public $video_id;
    public $video_nome;
    public $video_imagem;
    public $video_area2;
    public $video_url;
    public $result;

    public function __construct() {
        parent::__construct();         
    }

    public function incluir() {
        $video_imagem = "https://i.ytimg.com/vi/". $this->video_url."/hqdefault.jpg?custom=true&w=400&h=250&jpg";
        $this->insert("video", "video_nome, video_area2, video_url, video_imagem", "'$this->video_nome', '$this->video_area2', '$this->video_url', '$video_imagem'");
    }

          
        
        public function atualizar() {
        $video_imagem_atualizar = "https://i.ytimg.com/vi/".$this->video_url."/hqdefault.jpg?custom=true&w=400&h=250&jpg";    
        $query = "video_nome ='$this->video_nome', video_area2 = '$this->video_area2',"
                . " video_url = '$this->video_url', video_imagem = '$video_imagem_atualizar'";
        
        $this->update("video", "$query", "video_id = '$this->video_id'");
    }

    public function remover() {
        //echo "delete from video where video_id = $this->video_id";exit;
        $sql = "delete from video where video_id = $this->video_id";
        $this->db->query("$sql");        
        //$this->delete("video", "video_id = '$this->video_id'");
    }

    
    public function getVideo() {
        $this->select("video", "", "*", "", "INNER JOIN area2 ON(video_area2 = area2_id) WHERE video_id = $this->video_id", "");
    }

    /* QUERY USADA NO PORTFOLIO FRONT */

    public function getBy() {
        $this->select("area2", "", "*", "", "INNER JOIN video ON (video_area2 = area2_id) WHERE video_id = $this->video_id", "");
        //$this->select("area2", "", "*", "video_nome DESC", "INNER JOIN video ON (video_area2 = area2_id) WHERE area2_id = $this->video_area2", "");
    }

    /* QUERY USADA NO PORTFOLIO */

    public function getVideos() {
        $this->select("video", "", "*", "video_id DESC", "INNER JOIN area2 ON (video_area2 = area2_id)", "");
        //$this->select("video", "", "*", "", "INNER JOIN area2 ON (video_area2 = area2_id) ORDER BY video_id DESC", "");
    }

    /* PORTFOLIO ULTIMOS 6 RODAPE*/
    public function getUltimos() {
        $this->select("video", "", "*", "", "ORDER BY video_id DESC LIMIT 0,6", "");
    }
    /* PORTFOLIO ULTIMOS 6 RODAPE*/
    
     public function atualizaVideo() {
        $query_views = "video_views = video_views+1";
        $this->update("video", "$query_views", "video_id = '$this->video_id'");
    }

   

    public function JSON() {
        $this->getJSON("video", "video_id = '$this->video_id'");
    }

}
