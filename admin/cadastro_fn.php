<?php

require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}
function incluir() {
    $cadastros = new Cadastros();
    $cadastros->cadastro_email = addslashes($_POST['cadastro_email']);
    $cadastros->incluir();
    Filter::redirect("cadastro/?success");
}



function remover() {
    if (isset($_REQUEST['id'])) {
        $id = intval($_REQUEST['id']);
        $r = new Cadastros();
        $r->cadastro_id = $id;
        $r->remover();
        Filter :: redirect("cadastro/?success");
    }
}

function Json() {
        $j = new Cadastros();
        $j->cadastro_id =  intval($_REQUEST['cadastro_id']);
        echo $j->JSON();
}

function atualizar() {
    $cadastro_id = intval($_POST['cadastro_id']);
    $cadastro_email = addslashes($_POST['cadastro_email']);
    $cad_data = explode("/",$_POST['cadastro_data']);
    $cadastro_data = $cad_data[2]."-".$cad_data[1]."-".$cad_data[0];
    
    $a = new Cadastros();
    $a->cadastro_id = $cadastro_id;
    $a->cadastro_email = $cadastro_email;
    $a->cadastro_data = $cadastro_data;

    $a->atualizar();
    Filter :: redirect("cadastro/?success");
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}