<?php
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}

function atualizar() {
    $a = new ModuloAparencia();
    $a->modulo_aparencia_cor  = $_POST['modulo_aparencia_cor'];
    $a->modulo_aparencia_idioma  = $_POST['modulo_aparencia_idioma'];
    $a->modulo_aparencia_id  = intval($_POST['modulo_aparencia_id']);
    $a->modulo_aparencia_slide  = intval($_POST['modulo_aparencia_slide']);
    $a->modulo_aparencia_pelicula = $_POST['modulo_aparencia_pelicula'];
    if (isset($_FILES['modulo_aparencia_favicon']['name']) && !empty($_FILES['modulo_aparencia_favicon']['name'])) {
        $a->modulo_aparencia_favicon = $_POST['modulo_aparencia_favicon'];
        $a->removerFavicon();
        $a->enviarFavicon();
    }
    if (isset($_FILES['modulo_aparencia_logo']['name']) && !empty($_FILES['modulo_aparencia_logo']['name'])) {
        $a->modulo_aparencia_logo = $_POST['modulo_aparencia_logo'];
        $a->removerLogo();
        $a->enviarLogo();
    }
    if (isset($_FILES['modulo_aparencia_rodape']['name']) && !empty($_FILES['modulo_aparencia_rodape']['name'])) {
        $a->modulo_aparencia_rodape = $_POST['modulo_aparencia_rodape'];
        $a->removerRodape();
        $a->enviarRodape();
    }
    $a->atualizar();
    Filter :: redirect("frontend/?success");
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}

