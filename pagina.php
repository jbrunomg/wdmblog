<?php
require_once './loader.php';

$paginas_id = intval($_GET['id']);
$pagina = new Paginas();
$pagina->paginas_id = "$paginas_id";
$pagina->getByPaginas();
$area_menu = $pagina->area3_id;


$titulo_pagina = stripslashes($pagina->paginas_nome);
if(!empty($pagina->paginas_descricao)){
$descricao_pagina = Validacao::cut(stripslashes($p->paginas_descricao), 160, '...');
}
if(!empty($pagina->paginas_imagem)){
$imagem_pagina = Validacao::getBaseUrl()."/thumb.php?h=250&src=images/paginas/".$pagina->paginas_imagem;
}
require_once './header.php';
?>
<body class="js">


	<!-- Preloader -->
	 <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
	<!-- End Preloader -->
	
<?php require_once './menu.php'; ?>

<?php
$pagina = new Paginas();
$pagina->paginas_id = "$paginas_id";
$pagina->getByPaginas();
?>

<!-- Start Breadcrumbs -->
	<section class="breadcrumbs" <?php if(!empty($moduloo17->modulo17_imagem)) { echo "style='background: url(thumb.php?w=1280&zc=0&src=images/".stripslashes($moduloo17->modulo17_imagem).");'"; } ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><?= stripslashes($pagina->paginas_nome) ?></h2>
					<ul>
						<li><a href="home/">Home</a></li>
                           <li><a href="categoria/paginas/<?= Filter::slug2($pagina->area3_nome) ?>/<?= $pagina->area3_id ?>/"><?= stripslashes($pagina->area3_nome) ?></a></li>
                        <li class="active"><a href="pagina/<?= Filter::slug2($pagina->paginas_nome) ?>/<?= $pagina->paginas_id ?>/"><?= stripslashes($pagina->paginas_nome) ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Breadcrumbs -->
    
	
    <?php if (isset($pagina->db->data[0])) { ?>
    <!-- Start News -->
    <section class="newsblog project section single">
        <div class="container">
            <div class="row">
            <!-- Início Publicidade Top -->
                           <?php if ($modulo4->modulo4_status == 1) : ?>
                            <?php if (!empty($modulo4->modulo4_top)) : ?>
                               <section  class="ads_top">
                                      <div class="container">
                                          <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                                               <?= stripslashes($modulo4->modulo4_top) ?>
                                            </div>
                                          </div>
                                      </div>
                               </section>
                            <?php endif; ?>
                          <?php endif; ?>
                        <!-- / Final Publicidade Top -->
                <div class="col-md-8 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-delay="0.4s">
					<div class="row">
						 <div class="col-md-12 col-sm-12 col-xs-12">
                          
                         
                         
							<!-- Single News -->
							<div class="single-news">
                            
                                <!-- Project Slide -->
                                <div class="project-slide">
                                                      
                        <?php if(isset($pagina->paginas_imagem)): ?>              
						<div class="single-project">
							<div class="project-head">
								<img src="thumb.php?w=718&zc=0&src=images/paginas/<?= $pagina->paginas_imagem ?>" alt="<?= stripslashes($pagina->paginas_nome) ?>">	
                                <div class="project-hover">
									<h4><a href="images/paginas/<?= $pagina->paginas_imagem ?>"><?= stripslashes($pagina->paginas_nome) ?></a></h4>
									<p><?= stripslashes(Validacao::cut($pagina->paginas_descricao, 150, '...')) ?></p>
								</div>		
							</div>
						</div>
                        <?php endif;?>      
              <?php foreach ($pagina->db->data as $slide): ?> 
              
                <?php if ($slide->fotos_url != ""): ?>
						<div class="single-project">
							<div class="project-head">
								<img src="thumb.php?w=718&zc=0&src=images/paginas/<?= stripslashes($slide->fotos_url) ?>" alt="<?= stripslashes($pagina->paginas_nome) ?>">
                                <div class="project-hover">
									<h4><a href="images/paginas/<?= $pagina->paginas_imagem ?>"><?= stripslashes($pagina->paginas_nome) ?></a></h4>
									<p><?= stripslashes(Validacao::cut($pagina->paginas_descricao, 150, '...')) ?></p>
								</div>				
							</div>
					</div>
                    <?php endif;?> 
              <?php endforeach; ?>
                    </div>
					<!--/ End Porject Slide -->
                            
                            
                            
                            
								<div class="news-head" style="display: none;">
                                   <?php if(!empty($pagina->paginas_imagem)){?>
									<img src="thumb.php?w=718&h=300&src=images/paginas/<?= $pagina->paginas_imagem ?>" alt="<?= stripslashes($pagina->paginas_nome) ?>">
									<a href="#" class="link"><i class="fa fa-link"></i></a>
                                   <?php } ?> 
								</div>
								<!-- News Content --->
								<div class="news-content">
									<h4><a><?= stripslashes($pagina->paginas_nome) ?></a></h4>
									<div class="date">
                                    <?php 
                                       if($pagina->area3_parent != '0') {
                                       $p_area3 = new Area3();
                                       $p_area3->db = new DB;
                                       $p_area3->area3_id = $pagina->area3_parent;
                                       $p_area3->getArea3_();
                                       $area_menu = $pagina->area3_parent;
                                       if (isset($p_area3->db->data[0])) { ?>
                                       <a href="categoria/paginas/<?= Filter::slug2($p_area3->area3_nome) ?>/<?= $p_area3->area3_id ?>/"><i class="fa fa-list"></i> <?=stripslashes($p_area3->area3_nome) ?> <i class="fa fa-angle-right"></i> </a> 
                                       <?php }
                                       } ?>
                                    
                                       <a href="categoria/paginas/<?= Filter::slug2($pagina->area3_nome) ?>/<?= $pagina->area3_id ?>/"><?php if($pagina->area3_parent == '0') {?> <i class="fa fa-list"></i><?php } ?> <?= stripslashes($pagina->area3_nome)?></a> 
                                    
                                    
                                    
                                    <?php if ($modulo17->modulo17_comment == 1) : ?>
                                    <a href="<?=Validacao::getBaseUrl()?>/pagina/<?= Filter::slug2($pagina->paginas_nome) ?>/<?= $pagina->paginas_id ?>/#comentario">
                                    <i class="fa fa-comment  ipadding-left"></i> <span class="fb-comments-count" data-href="<?=Validacao::getBaseUrl()?>/pagina/<?= Filter::slug2($pagina->paginas_nome) ?>/<?= $pagina->paginas_id ?>/"></span>  Comentários</a>
                                    <?php endif; ?>
                                    </div>
                                   <?= $pagina->paginas_descricao ?>
								</div>
								<!--/ End News Content -->
								<!-- News Share -->
								<div class="news-share">
								             
                            <?php require_once "shared_buttons.php"; ?>
								</div>
								<!--/ End News Share -->
								<!-- Next Prev -->
								<div class="prev-next" style="display: none;">
									<ul>
										<li class="prev"><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
										<li class="next"><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
									</ul>
								</div>
								<!--/ End Next Prev -->
								<!-- Comments -->
                                <a name="comentario">&nbsp;</a>
                                <?php if (!isset($_POST['busca'])) : ?>
                                
                                 <?php if ($modulo17->modulo17_comment == 1) : ?>
								<div class="blog-comments">
									<h2>Comentários</h2>
									<div class="comments-body">

										<div class="fb-comments" data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" data-order-by="reverse_time" data-numposts="5" include-parent="false"></div>
										
									</div>
								
								</div>
                                 <?php endif; ?>
                               <?php endif; ?>
								<!--/ End Comments -->
							</div>
							<!--/ End Single News -->
						</div>
					</div>	
                </div>	
				
				<div class="col-md-4 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0.4s">
				    	<!-- News Sidebar -->
					<div class="blog-sidebar">
                    <!-- Start Search Form -->
					   <!-- Start Search Form -->
					   <?php require_once './form_busca_paginas.php'; ?>
						<!--/ End Search Form -->
                        
						<!-- Latest News -->
						<?php require_once './ultimas_paginas.php'; ?>
						<!--/ End Latest News -->
						<!-- News Category -->
						 <?php require_once './menu_categorias.php'; ?>
						<!--/ End News Category -->
                        
                        <!-- Início Instagram -->
                      <?php if ($modulo16->modulo16_status == 1) : ?>
                       <?php if (!empty($modulo16->modulo16_userid)) : ?>  
                       <div class="single-sidebar">
                       <h2><i class="fa fa-instagram"></i> <?= stripslashes($modulo16->modulo16_nome) ?></h2>
                       <div id="instagram"></div>
                       </div>
                       <?php endif; ?>
                     <?php endif; ?>
                     <!-- Final Instagram -->  
                        
						
                        <!-- Início Publicidade Side -->
                        <?php if ($modulo4->modulo4_status == 1) : ?>
                         <?php if (!empty($modulo4->modulo4_side)) : ?>
                           <div class="single-sidebar call-to-action">
                           	<?php if (!empty($modulo4->modulo4_side1)) : ?> <h2><?= stripslashes($modulo4->modulo4_side1) ?></h2><?php endif; ?>
                              <?= stripslashes($modulo4->modulo4_side) ?>
                            </div>
                          <?php endif; ?>
                        <?php endif; ?>	 
                       <!-- / Final Publicidade Side -->
 
					</div>
					<!--/ End News Sidebar -->
				
				</div>
            </div>	
 <!-- Início Publicidade Bottom -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_bottom)) : ?>
   <section  class="ads_bottom">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_bottom) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
        </div>
<!-- / Final Publicidade Bottom -->
    </section>
    <!--/ End News -->
    



<?php require_once './footer.php'; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>	
    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
      <!-- Custom  -->
    

	

  <script>
   $('li#paginas_<?= $area_menu ?>').addClass('current');
  </script> 
</body>
</html>   
<?php } else {
 header("Location: ".Validacao::getBaseUrl()."/404") ;
 } 
    
?>       