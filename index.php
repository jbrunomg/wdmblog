<?php
require_once './loader.php';
require_once './header.php';
$home = "1";

if (isset($_POST['email']) && !empty($_POST['email'])) {
    require_once './plugin/email/email.php';
    global $mail;
    $smtp = new Smtpr();
    $smtp->getSmtp();
    $mail->Port = $smtp->smtp_port;
    $mail->Host = $smtp->smtp_host;
    $mail->Username = $smtp->smtp_username;
    $mail->From = $smtp->smtp_username;
    $mail->Password = $smtp->smtp_password;
    $mail->FromName = $smtp->smtp_fromname;
    $mail->Subject = utf8_decode("Contato Via Site " . stripslashes($site->site_meta_titulo));
    $mail->AddBCC($smtp->smtp_bcc);
    $mail->AddAddress($smtp->smtp_username);

    $hour=date('H');
    $data = date('d/m/Y H:i',mktime($hour+1));
    $nome = stripslashes($_POST['nome']);
    $email = stripslashes($_POST['email']);
    $assunto = utf8_decode(stripslashes($_POST['assunto']));
    $mensagem = utf8_decode(stripslashes($_POST['mensagem']));
    $userip = getUserIP();

    $mail->AddReplyTo($email);
    $body = "<b>Data da Mensagem: </b> $data <br />";
    $body .= "<b>Nome:</b> $nome <br />";
    $body .= "<b>E-mail:</b> $email <br />";
    $body .= "<b>Assunto:</b> $assunto <br />";
    $body .= "<b>Mensagem: </b>$mensagem <br /><br />";
    $body .= "<b>IP: </b>$userip<br />";
    $mail->Body = nl2br($body);
}
?>

<body class="js">

	<!-- Preloader -->
	 <div class="loader" style="background: rgba(0, 0, 0, 0.80);">
        <div class="loader-inner ball-scale-ripple-multiple vh-center">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
	<!-- End Preloader -->
	

<?php require_once './menu.php'; ?>

    <?php if ($modulo_aparencia->modulo_aparencia_slide == 1): ?>
             <?php
                $slide = new Slide();
                $slide->getImagens();                   
             ?>        
      <?php if ($slide->db->data[0]): ?>
    <!-- Start Slider -->
    <section class="sufia-slider clearfix" id="home">
        <div class="slider-main">
   
    <?php foreach ($slide->db->data as $imagens): ?>
            <!-- Slider Item -->
			<div class="single-slider" style="background-image:url('images/<?= $imagens->slide_imagem ?>')">

                
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="welcome-text <?=stripslashes($imagens->slide_alinha)?>">

								 <?php if( stripslashes($imagens->slide_nome) != "" ): ?><h1><?= stripslashes($imagens->slide_nome) ?></h1><?php endif; ?> 
                                 <?php if( stripslashes($imagens->slide_subtitulo) != "" ): ?><h2><?= stripslashes($imagens->slide_subtitulo) ?></h2><?php endif; ?>
                            
                            
								<div class="button">
                                <?php if( stripslashes($imagens->slide_button_text1) != "" ): ?>
									<a <?php if( stripslashes($imagens->slide_button_link1) != "" ): ?>href="<?= Filter::UrlExternal($imagens->slide_button_link1)?>"<?php endif; ?> class="btn primary"><?=stripslashes($imagens->slide_button_text1)?></a>
                                    <?php endif; ?>
								</div>
                                
						<a class="b-arrow" href="#services"></a>
							</div>
						</div>
					</div>
				</div>
            </div>
            <!--/ End Slider Item -->
            <?php endforeach; ?>
	  </div>
    </section>
    <!--/ End Slider -->	
      <?php endif; ?>
<?php endif; ?>	

<!-- Início Publicidade Top -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_top)) : ?>
   <section  class="ads_top_home">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_top) ?>
                   </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Top -->

<?php if ($modulo1->modulo1_status == 1): ?>
<section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                    <div class="section-title">
                        <h2><?= stripslashes($modulo1->modulo1_nome) ?></h2>
                        <p><?= stripslashes($modulo1->modulo1_subtitulo1) ?></p>
                    </div>
                </div>
            </div>
   <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
<?= stripslashes($modulo1->modulo1_conteudo1) ?>
                </div>
              </div>
            </div>
        </div>
    </section>
<?php endif; ?>


	
    <!-- Início Serviços -->
    
    <?php
        $servico = new Servico();
        $servico->getServicos();                    
     ?>
    <?php if ($modulo6->modulo6_status == 1) : ?>
        <?php if (isset($servico->db->data[0])): ?>
    <section id="services" class="features section" style="padding-top: 70px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                    <div class="section-title">
                        <h2><?= stripslashes($modulo6->modulo6_nome) ?></h2>
                        <p><?= stripslashes($modulo6->modulo6_descricao) ?></p>
                    </div>
                </div>
            </div>
			
            <div class="row">
            <?php foreach ($servico->db->data as $s): ?>
                <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
					<!-- Início Serviço -->
                    <div class="single-features">
                        <i class="<?= stripslashes($s->servico_icon) ?>"></i>
						<h4><?= stripslashes($s->servico_nome) ?></h4>
						<p><?= stripslashes($s->servico_descricao) ?></p>
                    </div>
					<!--/ Final Serviço -->
                </div>
            <?php endforeach; ?>    
		 
                </div>
            </div>
        </div>
    </section>
  	<?php endif; ?>
 <?php endif; ?>	  
<!--/ Final Serviços -->



    <!-- Start Cotacao -->
      <?php if ($modulo18->modulo18_status == 1) : ?>
        <section id="newslatter" class="newslatter section" <?php if(!empty($modulo18->modulo18_imagem)) { ?>style="background-image:url(thumb.php?w=1280&src=images/<?= $modulo18->modulo18_imagem?>);"<?php } ?> >
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xs-12 wow fadeInLeft" data-wow-delay="0.4s">
                    <!-- Cotacao Title -->
                    <div class="news-title title-cotacao">
                        <h2>PROTEÇÃO  <span> VEICULAR</span></h2>
                        <!-- <h2><?= stripslashes($modulo18->modulo18_nome) ?></h2> -->
                        <!-- <p><?= stripslashes($modulo18->modulo18_subtitulo) ?></p> -->
                    </div>
                    <div class="box-actions-buttons clearfix">
                        <div class="col-md-4 col-sm-4 col-xs-12 actions-buttons">
						<a href="https://painel.rastrek.com.br/login" target="_blank" > 
                            <button class="newsActionButton">
                                <i class="fa fa-edit action-icon-button"></i> 
                                <span class="action-button">Rastrek</span>
                            </button>
						</a>	
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 actions-buttons">
						<a href="http://ap3.stc.srv.br/webcliente/mrsilva" target="_blank" >
                            <button class="newsActionButton">
                                <i class="fa fa-edit action-icon-button"></i> 
                                <span class="action-button">Movit</span>
                            </button>
						</a>	
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 actions-buttons">
                            <button class="newsActionButton">
                                <i class="fa fa-edit action-icon-button"></i> 
                                <span class="action-button">2ª Via Boleto</span>
                            </button>
                        </div>
                    </div>

                    <div class="news-images">
                        <div class="vehicle-image vehicle-image-50 vehicle-align-left">
                            <div>
                            <a href="#" >
                                <img src="images/veiculos/veiculo-site-08.png" alt="" title=""/>
                            </a>
                            </div>
                        </div>
                        <div class="vehicle-image vehicle-image-50 vehicle-align-right">
                            <div>
                            <a href="#" >
                                <img src="images/veiculos/veiculo-site-09.png" alt="" title=""/>
                            </a>
                            </div>
                        </div>
                        <div class="vehicle-image vehicle-image-50">
                        <div>
                            <a href="#" >
                                <img src="images/veiculos/veiculo-site-10.png" alt="" title=""/>
                            </a>
                        </div>
                        </div>
                    </div>
                    <!--/ End Cotacao Title -->
                </div>

                 <div class="col-md-4 col-xs-12 wow fadeInRight" data-wow-delay="0.4s">
                    <!-- Cotacao Form -->
                    <div id="orcamento" class="single-sidebar title-cotacao">
                     <!-- <h2>Solicitar <span> Orçamento</span></h2> -->
                     <h2><?= stripslashes($modulo18->modulo18_nome) ?></h2>
                        <p><?= stripslashes($modulo18->modulo18_subtitulo) ?></p>                 
                    
                    <form id="formCotacao" class="box-form-cotacao">    
                        <div class="form-group">
                            <input type="text" id="nome_cotacao" class="form-control rounded inputCotacao" placeholder="Seu nome" required="required">
                        </div>
                        <div class="form-group">
                            <input type="email" id="email_cotacao" class="form-control rounded inputCotacao" placeholder="Seu email" required="required">
                        </div>
                        <div class="form-group">
                            <input type="text" id="tel_cotacao" class="form-control rounded inputCotacao" placeholder="Seu Celular" required="required">
                        </div>
                        <div class="form-group">
                            <input type="text" id="placa_cotacao" class="form-control rounded inputCotacao" placeholder="Placa Veiculo" required="required">
                        </div>

                        <div class="form-group">    
                            <!-- <button type="submit" class="button primary"><i class="fa fa-shopping-cart"></i><?= stripslashes($modulo18->modulo18_button) ?></button> -->                           
                            <input type="submit" class="signup sendCotacao" value="<?= stripslashes($modulo18->modulo18_button) ?>">                          
                        </div>
                    </form>                  
                  </div>
             
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!--/ End Cotacao -->


    <!-- Start Brand (Clube Beneficio)-->
     <?php if ($modulo8->modulo8_status == 1) : ?>
     <?php 
            $clientes = new Cliente();
            $clientes->getClientes();
            $i = 0;
        ?>
            <?php if (isset($clientes->db->data[0])): ?>
    <div id="brand" class="brand section">
        <div class="container">
        <?php if(!empty($modulo8->modulo8_nome) || !empty($modulo8->modulo8_descricao)) {?>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                    <div class="section-title">
                        <h2><?= $modulo8->modulo8_nome ?></h2>
                        <p><?= stripslashes($modulo8->modulo8_descricao) ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>  
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                    <div class="brand-slider">
                    <?php foreach ($clientes->db->data as $cliente) : $i = $i+1; ?>
                        <!-- Single Brand -->
                        <div class="single-brand <?php if($i  == 2){ echo "active"; } ?>">
                            <a href="<?= stripslashes($cliente->cliente_subtitulo) ?>" target="_blank">
                            <img src="thumb.php?w=170&zc=0&src=images/partner/<?= stripslashes($cliente->cliente_imagem) ?>" alt="<?= stripslashes($cliente->cliente_nome) ?>"/></a>
                        </div>
                        <!--/ End Single Brand -->
                    <?php endforeach; ?>    
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ End Brand (Clube Beneficio) -->
 <?php endif; ?>
   <?php endif; ?>  


       <!-- Start Videos -->
    <?php if ($modulo12->modulo12_status == 1) : ?>
        <?php 
            $videos = new Video();
            $videos->getVideos();
        ?>
            <?php if (isset($videos->db->data[0])): ?>
    <section id="videos" class="newsblog section">
        <div class="container">
        <?php if(!empty($modulo12->modulo12_nome) || !empty($modulo12->modulo12_descricao)) {?>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                    <div class="section-title">
                        <h2><?= $modulo12->modulo12_nome ?></h2>
                        <p><?= stripslashes($modulo12->modulo12_descricao) ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>  
            <div class="row">
            <?php $v = 0; 
            foreach ($videos->db->data as $video) : ?>
            <?php 
            $v++;

            // var_dump($v);die();
            if ($video->video_views < 2) {
                $views = stripslashes($video->video_views)." visualização";
            } else {
                $views = stripslashes($video->video_views)." visualizações";
            } ?>
                <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
                    <!-- Single Video -->
                   <div class="single-news">
                        <div class="news-head">
                                            
                            <img src="<?= $video->video_imagem ?>" width="358" height="245" alt="Assista: <?= stripslashes($video->video_nome) ?>"/>
                            <a href="play_video.php?video_url=<?= $video->video_url ?>&video_id=<?= stripslashes($video->video_id) ?>" class="link video-play mfp-iframe" title="Assista: <?= stripslashes($video->video_nome) ?>">
                            <i class="fa fa-play"></i></a>
                            
                    </div>
             <!--            <div class="news-content">
                            <h4><a href="play_video.php?video_url=<?= $video->video_url ?>&video_id=<?= stripslashes($video->video_id) ?>" class="video-play mfp-iframe"><?= stripslashes($video->video_nome) ?></a></h4>
                            <div class="date datesmall">
                             <a href="javascript:void(0);">
                             <i class="fa fa-list"></i> <?= stripslashes($video->area2_nome)?></a> 
                             <a href="javascript:void(0);">
                             <i class="fa fa-eye ipadding-left"></i> <?= $views?></a> 
                            </div>
                        </div> -->
                    </div>
                    <!--/ End Single Video -->
                </div>
                <?php if($v == 3) {
                    break; }                   
                endforeach; ?>
                
            </div>
            
            <div class="row">
                <div class="col-md-12 col-sm-12  col-xs-12 wow fadeInUp">
                    <!-- Load More Button -->
                    <div class="button">
                        <a href="videos/" class="btn">Todos Videos</a>
                    </div>
                    <!--/ End Load More Button -->
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
   <?php endif; ?>  
    <!--/ End Videos -->


    <!-- Start Newslatter -->
      <?php if ($modulo15->modulo15_status == 1) : ?>
    <section id="newslatter" class="newslatter section" <?php if(!empty($modulo15->modulo15_imagem)) { ?>style="background-image:url(thumb.php?w=1280&src=images/<?= $modulo15->modulo15_imagem?>);"<?php } ?> >
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInLeft" data-wow-delay="0.4s">
                    <!-- Newslatter Title -->
                    <div class="news-title">
                        <h2><?= stripslashes($modulo15->modulo15_nome) ?></h2>
                        <p><?= stripslashes($modulo15->modulo15_subtitulo) ?></p>
                    </div>
                    <!--/ End Newslatter Title -->
                </div>
                 <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInRight" data-wow-delay="0.4s">
                    <!-- Newslatter Form -->
                    <div class="news-form">
                    <form id="formSend">
                        <input type="email" class="newso-input" id="email-send" required placeholder="Informe seu email...">
                        <input type="submit" class="signup" value="<?= stripslashes($modulo15->modulo15_button) ?>">
                     </form>   
                    </div>
                    <!--End Newslatter Form -->
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
    <!--/ End Newslatter -->
    
    
    
    
	<!-- Start Count Down -->
    <?php if ($modulo14->modulo14_status == 1) : ?>
    <section id="countdown" class="count-down section" <?php if (!empty($modulo14->modulo14_imagem)){ echo 'style="background-image: url(./images/'.$modulo14->modulo14_imagem.');"'; } ?>>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-6 wow fadeInUp" data-wow-delay="0.3s">
					<!-- Single Count -->
                    <div class="single-count">
                        <i class="<?=$modulo14->modulo14_icon1?>"></i>
                        <h2 class="count"><?=$modulo14->modulo14_text1?></h2>
                        <p><?=$modulo14->modulo14_descricao1?></p>
                    </div>
					<!--/ End Single Count -->
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 wow fadeInUp" data-wow-delay="0.5s">
					<!-- Single Count -->
                    <div class="single-count">
                        <i class="<?=$modulo14->modulo14_icon2?>"></i>
                        <h2 class="count"><?=$modulo14->modulo14_text2?></h2>
                        <p><?=$modulo14->modulo14_descricao2?></p>
                    </div>
					<!--/ End Single Count -->
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 wow fadeInUp" data-wow-delay="0.7s">
					<!-- Single Count -->
                    <div class="single-count">
						<i class="<?=$modulo14->modulo14_icon3?>"></i>
                        <h2 class="count"><?=$modulo14->modulo14_text3?></h2>
                        <p><?=$modulo14->modulo14_descricao3?></p>
                    </div>
					<!--/ End Single Count -->
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 wow fadeInUp" data-wow-delay="0.9s">
					<!-- Single Count -->
                    <div class="single-count">
                        <i class="<?=$modulo14->modulo14_icon4?>"></i>
                        <h2 class="count"><?=$modulo14->modulo14_text4?></h2>
                        <p><?=$modulo14->modulo14_descricao4?></p>
                    </div>
					<!--/ End Single Count -->
                </div>
            </div>
        </div>
    </section>
   <?php endif; ?> 
    <!--/ End Count Down -->
	
    
    
        <?php if ($modulo7->modulo7_status == 1) : ?>
        <?php 
            $projeto = new Portfolio();
            $projeto->getPortfolios();
            
            $categoria = new Area1();
            $categoria->db = new DB;
            $categoria->getAreas1();  
            
                   
        ?>
        
   <section id="projetos" class="project archive section">
        <div class="container">
        <?php if(!empty($modulo7->modulo7_nome) || !empty($modulo7->modulo7_descricao)) {?>
        <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                    <div class="section-title">
                        <h2><?= stripslashes($modulo7->modulo7_nome) ?></h2>
                        <p><?= stripslashes($modulo7->modulo7_descricao) ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
					<!-- Project Nav -->
                    <div class="project-nav">
                        <ul>
                            <li class="active" data-filter="*">Todos</li>
                            <?php if (isset($categoria->db->data[0])): ?>
                                    <?php foreach ($categoria->db->data as $categoria): ?>
                                    <?php if ($categoria->area1_nome == "Publicidade") {
                                $categoria_area1_nome = "Public";
                                 } else {
                                 $categoria_area1_nome = $categoria->area1_nome;     
                                 }?>
                            <li data-filter=".<?= Filter::slug2(trim($categoria_area1_nome)) ?>"><?= stripslashes($categoria->area1_nome) ?></li>
                              <?php endforeach; ?>
                            <?php endif; ?> 
        
                        </ul>
                    </div>
					<!--/ End Project Nav -->
                </div>
            </div>
            <div class="row wow fadeInUp" data-wow-delay="0.4s">
				<div class="isotop-active">
                <?php if (isset($projeto->db->data[0])): ?>
                         <?php foreach ($projeto->db->data as $trabalhos): ?>
                         <?php 
                          $a++;
                          if ($trabalhos->area1_nome == "Publicidade") {
                                $trabalhos->area1_nome = "Public";
                            }?>
					<div class="col-md-3 col-sm-4 col-xs-12 <?= Filter::slug2($trabalhos->area1_nome) ?>">
						<!-- Single Project -->
						<div class="single-project">
							<div class="project-head" onclick="window.location.href='projeto/<?= Filter::slug2($trabalhos->portfolio_nome) ?>/<?= $trabalhos->portfolio_id ?>/';return false;" >
								<img src="thumb.php?w=262&h=262&zc=1&src=images/portfolio/<?= $trabalhos->portfolio_imagem ?>" alt="<?= stripslashes($trabalhos->portfolio_nome) ?>"/>
								<div class="project-hover">
									<h4><a href="projeto/<?= Filter::slug2($trabalhos->portfolio_nome) ?>/<?= $trabalhos->portfolio_id ?>/"><?= stripslashes($trabalhos->portfolio_nome) ?></a></h4>
									<p><a href="projeto/<?= Filter::slug2($trabalhos->portfolio_nome) ?>/<?= $trabalhos->portfolio_id ?>/"><?= stripslashes(Validacao::cut(stripslashes($trabalhos->portfolio_descricao), 100, '...')) ?></a></p>
								</div>
							</div>
						</div>
						<!--/ End Single Project -->
					</div>
				   <?php if($a == 4) {
                    break; }
                endforeach; ?>
              <?php endif; ?>	
				</div>
            </div>
            <div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
					<!-- Load More -->
                    <div class="button">
                        <a href="projetos/" class="btn">Ver Mais</a>
                    </div>
					<!--/ End Load More -->
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?> 
    <!--/ End Project Archive -->
   
   
    <!-- Start Testimonial -->
    <?php if ($modulo5->modulo5_status == 1): ?>
          

            <?php
                $depoimento = new Depoimento();
                $depoimento->getHome();
            ?>
     <?php if (isset($depoimento->db->data[0])): ?>
    <section id="testimonials" class="testimonial section" <?php if(!empty($modulo5->modulo5_imagem)) {?> style="background-image:url(thumb.php?w=1280&src=images/<?= $modulo5->modulo5_imagem?>);"<?php } ?>>
        <div class="container">
            <div class="row">
				<div class="testimonial-active wow fadeInUp" data-wow-delay="0.4s">
					<!-- Single Testimonial -->
                    <?php foreach ($depoimento->db->data as $dp): ?>
					<div class="testimonial-content">  
                                     
						<h2><?= stripslashes($modulo5->modulo5_nome) ?></h2>
                        <div class="pull-left wow fadeInUp" data-wow-delay="2.4s">
                        <img class="client-img img-circle" src="thumb.php?w=150&h=150&zc=0&src=images/team/<?= $dp->depoimento_imagem ?>"  style="width:150px;height:150px;margin-right:18px;" alt=""/>
                         </div>
                          <div class="media-body">
						<p><?= stripslashes($dp->depoimento_sobre) ?></p>
						<h4 class="t-name"><?= stripslashes($dp->depoimento_nome) ?> — <?= stripslashes($dp->depoimento_cargo) ?></h4>
                        </div>
					</div>
                    <?php endforeach; ?>   
					<!--/ End Single Testimonial -->
					
				</div>
            </div>
        </div>
    </section>
      <?php endif; ?>
   <?php endif; ?>
    <!--/ End Testimonial -->
	

	

    <!-- Start News -->
    <?php if ($modulo10->modulo10_status == 1) : ?>
        <?php 
            $pagina = new Pagina();
            $pagina->getBlog();
        ?>
            <?php if (isset($pagina->db->data[0])): ?>
    <section id="blog" class="newsblog section project archive">
        <div class="container">
        <?php if(!empty($modulo10->modulo10_button) || !empty($modulo10->modulo10_subtitulo)) {?>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                    <div class="section-title">
                        <h2><?= $modulo10->modulo10_button ?></h2>
                        <p><?= stripslashes($modulo10->modulo10_subtitulo) ?></p>
                    </div>
                </div>
            </div>
		<?php } ?>	
            <div class="row">
             <div id="latest_news">
            <?php foreach ($pagina->db->data as $post) : ?>
                <div class="item col-md-12 col-sm-12" style="width:99%">
					<!-- Single News -->
					<div class="single-news">
						<div class="news-head">
							<img src="thumb.php?w=358&h=245&zc=1&src=images/blog/<?= $post->pagina_imagem ?>" alt="Mais sobre post: <?= stripslashes($post->pagina_nome) ?>">
							<a href="post/<?= Filter::slug2($post->pagina_nome) ?>/<?= $post->pagina_id ?>/" class="link" title="Mais sobre post: <?= stripslashes($post->pagina_nome) ?>"><i class="fa fa-link"></i></a>
						</div>
	<!-- 					<div class="news-content">
							<h4><a href="post/<?= Filter::slug2($post->pagina_nome) ?>/<?= $post->pagina_id ?>/"><?= stripslashes($post->pagina_nome) ?></a></h4>
							<div class="date datesmall">
                             <a href="categoria/blog/<?= Filter::slug2($post->area_nome) ?>/<?= $post->area_id ?>/">
                             <i class="fa fa-list"></i> <?= stripslashes($post->area_nome)?></a> 
                             <a href="javascript:void(0);">
                             <i class="fa fa-calendar ipadding-left"></i> <?= stripslashes($post->pagina_data)?></a>
                             <?php if ($modulo10->modulo10_comment == 1) : ?>
                             <a href="<?=Validacao::getBaseUrl()?>/post/<?= Filter::slug2($post->pagina_nome) ?>/<?= $post->pagina_id ?>/#comentario">
                             <i class="fa fa-comment  ipadding-left"></i> <span class="fb-comments-count" data-href="<?=Validacao::getBaseUrl()?>/post/<?= Filter::slug2($post->pagina_nome) ?>/<?= $post->pagina_id ?>/"></span></a>
                            <?php endif; ?>
                            </div>
							<p><?= Validacao::cut(stripslashes($post->pagina_descricao), 110, '...') ?></p>
							<a href="post/<?= Filter::slug2($post->pagina_nome) ?>/<?= $post->pagina_id ?>/" class="btn"><?= stripslashes($modulo10->modulo10_button1) ?> <span class="fa fa-angle-double-right"></span></a>
						</div> -->
					</div>
					<!--/ End Single News -->
                </div>
                <?php endforeach; ?>
				</div>
            </div>
			
            <div class="row">
                <div class="col-md-12 col-sm-12  col-xs-12 wow fadeInUp">
					<!-- Load More Button -->
					<div class="button">
                        <a href="blog/" class="btn">Ver mais</a>
                    </div>
					<!--/ End Load More Button -->
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>
   <?php endif; ?>  
    <!--/ End News -->
    
    

    
    

    
    
 <!-- Início Publicidade Bottom -->
<?php if ($modulo4->modulo4_status == 1) : ?>
 <?php if (!empty($modulo4->modulo4_bottom)) : ?>
   <section  class="ads_bottom_sub">
       <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                 <?= stripslashes($modulo4->modulo4_bottom) ?>
                  </div>
                </div>
            </div>
   </section>
     <?php endif; ?>
<?php endif; ?>	  
<!-- / Final Publicidade Bottom -->

<div class="modal fade" id="cotacaoSend" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title"><i class=" icon-mail-2"></i> Confirme seus dados </h4>
            </div>
        <div class="modal-body">

            <div id="sucess-cotacao" style="display: none;">
                <div class="alert alert-success pgray  alert-lg" role="alert">
                <h2 class="no-margin no-padding">&#10004; Sucesso!</h2>
                <p>Seus dados foi cadastrado com sucesso!</p>
                </div>
                <div align="center">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="fechar-cotacao"><i class="fa fa-times-circle" aria-hidden="true"></i> Fechar</button>
                </div>
            </div>

            <div id="erro-cotacao" style="display: none;">
                <div class="alert alert-danger pgray  alert-lg" role="alert">
                <h2 class="no-margin no-padding">&#10004; ERRO!</h2>
                <p>A placa<span id="erro-placa" style="font-weight: 700;"></span> já está cadastrado em nosso sistema.</p>
                </div>
                <div align="center">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="fechar-cotacao"><i class="fa fa-times-circle" aria-hidden="true"></i> Fechar</button>
                </div>
            </div>

            <form role="form" name="form-cotacao" id="form-cotacao">
            <div class="form-group">
                <label for="cotacao_nome" class="control-label">Nome:</label>
                <input id="cotacao_nome" name="cotacao_nome" type="text" data-content="nome" data-trigger="manual" data-placement="top" required="" placeholder="Seu nome" class="form-control" autocomplete="off"/>
            </div>    
            <div class="form-group">
                <label for="cotacao_email" class="control-label">E-mail:</label>
                <input id="cotacao_email" name="cotacao_email" type="email" data-content="Deve ser um email válido (por ex. usuario@gmail.com)" data-trigger="manual" data-placement="top" required="" placeholder="Seu E-mail" class="form-control email" autocomplete="off"/>
            </div>
            <div class="form-group">
                <label for="cotacao_tel" class="control-label">Telefone:</label>
                <input id="cotacao_tel" name="cotacao_tel" type="text" data-content="Deve ser um nº telefone válido" data-trigger="manual" data-placement="top" required="" placeholder="Seu Telefone" class="form-control" autocomplete="off"/>
            </div>
            <div class="form-group">
                <label for="cotacao_placa" class="control-label">Placa:</label>
                <input id="cotacao_placa" name="cotacao_placa" type="text" data-content="Deve ser uma placa válida" data-trigger="manual" data-placement="top"  placeholder="Sua placa" class="form-control" autocomplete="off"/>
            </div>



                <div class="form-group">
                <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; Erro ao enviar o formulário . </p>
                </div>

        </div>
                <div class="modal-footer">
            <input type="hidden" name="ip" id="ip" value="<?php echo getUserIP(); ?>" />
            <button type="button" class="btn btn-danger" id="cancel" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-success" id="submit_form" name="submit_form" class="btn btn-default pull-right">Confirmar!</button>
            </form>
</div>
</div>
</div>
</div>
		



<div class="modal fade" id="emailSend" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title"><i class=" icon-mail-2"></i> Confirme seu email </h4>
            </div>
        <div class="modal-body">

            <div id="sucess-message" style="display: none;">
                <div class="alert alert-success pgray  alert-lg" role="alert">
                <h2 class="no-margin no-padding">&#10004; Sucesso!</h2>
                <p>Seu email foi cadastrado com sucesso!</p>
                </div>
                <div align="center">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="fechar-mensagem"><i class="fa fa-times-circle" aria-hidden="true"></i> Fechar</button>
                </div>
            </div>

            <div id="erro-message" style="display: none;">
                <div class="alert alert-danger pgray  alert-lg" role="alert">
                <h2 class="no-margin no-padding">&#10004; ERRO!</h2>
                <p>O email <span id="erro-email" style="font-weight: 700;"></span> já está cadastrado em nossa Newsletter</p>
                </div>
                <div align="center">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="fechar-mensagem1"><i class="fa fa-times-circle" aria-hidden="true"></i> Fechar</button>
                </div>
            </div>

            <form role="form" name="form-message" id="form-message">
            <div class="form-group">
                <label for="sender_email" class="control-label">E-mail:</label>
                <input id="sender_email" name="sender_email" type="email" data-content="Deve ser um email válido (por ex. usuario@gmail.com)" data-trigger="manual" data-placement="top" required="" placeholder="Seu E-mail" class="form-control email" autocomplete="off"/>
            </div>


                <div class="form-group">
                <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; Erro ao enviar o formulário . </p>
                </div>

        </div>
                <div class="modal-footer">
            <input type="hidden" name="ip" id="ip" value="<?php echo getUserIP(); ?>" />
            <button type="button" class="btn btn-danger" id="cancel" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-success" id="submit_form" name="submit_form" class="btn btn-default pull-right">Confirmar!</button>
            </form>
</div>
</div>
</div>
</div>
	
<?php require_once "footer.php"; ?>
<!-- Jquery -->
    <script type="text/javascript" src="js\jquery.min.js"></script>
	<!-- Colors -->
    <script type="text/javascript" src="js\colors.js"></script>
	<!-- Modernizr JS -->
	<script type="text/javascript" src="js\modernizr.min.js"></script>
	<!-- Appear Js -->
	<script type="text/javascript" src="js\jquery.appear.js"></script>	
	<!-- Scrool Up -->
    <script type="text/javascript" src="js\jquery.scrollUp.min.js"></script>
	<!-- Typed Js -->
	<script type="text/javascript" src="js\typed.min.js"></script>
	<!-- Slick Nav -->
	<script type="text/javascript" src="js\jquery.slicknav.min.js"></script>
	<!-- Onepage Nav -->
	<script type="text/javascript" src="js\jquery.nav.js"></script>
    <!-- Yt Player -->
	<script type="text/javascript" src="js\ytplayer.min.js"></script>
	<!-- Magnific Popup -->
	<script type="text/javascript" src="js\magnific-popup.min.js"></script>
	<!-- Wow JS -->
	<script type="text/javascript" src="js\wow.min.js"></script>
	<!-- Counter JS -->
	<script type="text/javascript" src="js\waypoints.min.js"></script>
    <script type="text/javascript" src="js\jquery.counterup.min.js"></script>
	<!-- Isotop JS -->
	<script type="text/javascript" src="js\isotope.pkgd.min.js"></script>
    <!-- Masonry JS -->
	<script type="text/javascript" src="js\masonry.pkgd.min.js"></script>
	<!-- Slick Slider -->
	<script type="text/javascript" src="js\slick.min.js"></script>
	<!-- Bootstrap JS -->
	<script type="text/javascript" src="js\bootstrap.min.js"></script>
    
    <script type="text/javascript" src="js\owl.carousel.min.js"></script>

    <!-- Activate JS -->
	<script type="text/javascript" src="js\active.js"></script>
      <!-- Custom  -->
      <script>
   $('li#index').addClass('current');

   $(document).scroll(function() {
        if ($(window).scrollTop() !== 0) {
            $(".container").css("background-color", "white");
        } else {
            $(".container").css("background-color", "transparent");
        }
    });

$('form#formCotacao').on('submit', function(ev) {

  ev.preventDefault();
    var nome  = $("#nome_cotacao").val();
    var email = $("#email_cotacao").val();
    var tel   = $("#tel_cotacao").val();
    var placa = $("#placa_cotacao").val();
    $('#cotacaoSend').modal('show'); 
    $("#cotacao_nome").val(nome); 
    $("#cotacao_email").val(email); 
    $("#cotacao_tel").val(tel); 
    $("#cotacao_placa").val(placa); 
});


$("form#form-cotacao").submit(function(event){
  var cotacao_nome  = $("#cotacao_nome").val(); 
  var cotacao_email = $("#cotacao_email").val();  
  var cotacao_tel   = $("#cotacao_tel").val();  
  var cotacao_placa = $("#cotacao_placa").val();   
 
  //disable the default form submission
  event.preventDefault();
 
  //grab all form data  
  var formData = new FormData($(this)[0]);
  
  $.ajax({
    url: 'send_cotacao.php',
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (result) {
       if(result==1){ 
        $('#sucess-cotacao').fadeIn('slow');
        $('#form-cotacao').fadeOut(100).hidden;
        $('#cancel').fadeOut(0).hidden;
        $('#submit_form').fadeOut(0).hidden;
    }  else if (result==0) {
        $("#erro-placa").html(cotacao_placa);
        $('#erro-cotacao').fadeIn(500);     //Informa o erro
        $('#form-cotacao').fadeOut(100).hidden;
        $('#cancel').fadeOut(0).hidden;
        $('#submit_form').fadeOut(0).hidden;
    }
  }  
});

  return false;
 });

$('#fechar-cotacao, #fechar-cotacao, #cancel').on('click', function(){ 
    $('#sucess-cotacao,#erro-cotacao').hide();
    $('#form-cotacao, #cancel, #submit_form').show();

});



  //Program a custom submit function for the form
  
$('form#formSend').on('submit', function(ev) {

  ev.preventDefault();
    var send = $("#email-send").val();
    $('#emailSend').modal('show'); 
    $("#sender_email").val(send); 
});
  
$("form#form-message").submit(function(event){
  var sender = $("#sender_email").val();  
 
  //disable the default form submission
  event.preventDefault();
 
  //grab all form data  
  var formData = new FormData($(this)[0]);
  
  $.ajax({
    url: 'send_email.php',
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (result) {
       if(result==1){ 
        $('#sucess-message').fadeIn('slow');
  		$('#form-message').fadeOut(100).hidden;
        $('#cancel').fadeOut(0).hidden;
        $('#submit_form').fadeOut(0).hidden;
    }  else if (result==0) {
        $("#erro-email").html(sender);
        $('#erro-message').fadeIn(500);		//Informa o erro
        $('#form-message').fadeOut(100).hidden;
        $('#cancel').fadeOut(0).hidden;
        $('#submit_form').fadeOut(0).hidden;
    }
  }  
});

  return false;
 });

$('#fechar-mensagem1, #fechar-mensagem1, #cancel').on('click', function(){ 
    $('#sucess-message,#erro-message').hide();
    $('#form-message, #cancel, #submit_form').show();

});

$("#latest_pages").owlCarousel({
		autoPlay: 3500, 
		items : 3,
		slideSpeed : 400,
		paginationSpeed : 500,
		paginationNumbers:false,
		dots:false,
 
 });
 
 $("#latest_news").owlCarousel({
		autoPlay: 3500, 
		items : 3,
		slideSpeed : 400,
		paginationSpeed : 500,
		paginationNumbers:false,
		dots:false,
 
 });
jQuery(document).ready( function() {
jQuery("iframe").width("100%");
jQuery("iframe").height(385);

w_yt = jQuery("iframe#yt").attr("width");
h_yt = jQuery("iframe#yt").attr("height");
jQuery("iframe#yt").width(w_yt );
jQuery("iframe#yt").height(h_yt );
});
</script>
</body>
</html>