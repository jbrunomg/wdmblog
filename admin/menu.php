<?php
$status0 = new Comment();
?>
<aside id="sidebar-left" class="sidebar-circle">
    <div class="sidebar-content">
        <div class="media">
            <a class="pull-left has-notif avatar" href="javascript:void(0);">
                <?php if ($_SESSION['USER']['IMAGEM']): ?>
                    <img src="../images/usuario/<?= $_SESSION['USER']['IMAGEM'] ?>" alt="admin">
                <?php else : ?>
                    <img src="../images/usuario/avatar.png" alt="admin">
                <?php endif; ?>
                <i class="online"></i>
            </a>
            <div class="media-body">
                <?php $user_nome = explode(" ", $_SESSION['USER']['NOME']); ?>
                <h4 class="media-heading">Olá, <span><?= $user_nome[0]; ?></span></h4>
            </div>
        </div>
    </div>
    <ul class="sidebar-menu">
        <li id="home">
            <a href="<?= Validacao::getBase() ?>admin/">
                <span class="icon"><i class="fa fa-home"></i></span>
                <span class="text">Dashboard</span>
            </a>
        </li>
        <li id="frontend">
            <a href="frontend/">
                <span class="icon"><i class="fa fa-cog"></i></span>
                <span class="text">Configurações </span>
            </a>
        </li>
        <?php //if ($modulo1->modulo1_status == 1): ?>
        <li id="slide">
            <a href="slide/">
                <span class="icon"><i class="fa fa-photo"></i></span>
                <span class="text">Slide </span>
            </a>
        </li>
        <?php //endif;?>
        <li>
            <a class="logout" data-url="logar/?deslogar"  href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Logout">
                <span class="icon"><i class="fa fa-power-off"></i></span>
                <span class="text">Sair/ Logout </span>
            </a>
        </li>
        <li class="sidebar-category">
            <span>Conteúdo</span>
            <span class="pull-right"><i class="fa fa-edit"></i></span>
        </li>
        <?php if ($modulo5->modulo5_status == 1): ?>
        <li class="submenu depoimentonovo listardepoimento">
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-bullhorn"></i></span>
                <span class="text">Depoimentos</span>
                <span class="arrow"></span>
            </a>
            <ul>
                <li class="depoimentonovo"><a href="depoimento/novo/">Novo Depoimento</a></li>
                <li class="listardepoimento"><a href="depoimentos/">Listar Depoimentos</a></li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if ($modulo8->modulo8_status == 1): ?>
        <li class="submenu clientenovo listar">
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-handshake-o"></i></span>
                <span class="text">Parceiros</span>
                <span class="arrow"></span>
            </a>
            <ul>
                <li class="clientenovo"><a href="parceiros/novo/">Adicionar <?= stripslashes($cliente->modulo8_nome) ?></a></li>
                <li class="listar"><a href="parceiros/">Listar <?= stripslashes($cliente->modulo8_nome) ?></a></li>
            </ul>
        </li>
       <?php endif; ?>
       <?php if ($modulo13->modulo13_status == 1): ?>
        <li class="submenu equipenovo equipelistar">
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-users"></i></span>
                <span class="text">Equipe</span>
                <span class="arrow"></span>
            </a>
            <ul>
                <li class="equipenovo"><a href="equipe/novo/">Adicionar </a></li>
                <li class="equipelistar"><a href="equipe/">Listar </a></li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if ($modulo6->modulo6_status == 1): ?>
        <li class="submenu serviconovo listarservico servicoeditar">
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-suitcase"></i></span>
                <span class="text">Serviços</span>
                <span class="arrow"></span>
            </a>
            <ul>
                <li class="serviconovo"><a href="servico/novo/">Novo Serviço</a></li>
                <li class="listarservico"><a href="servico/">Listar Serviços</a></li>
            </ul>
        </li>
        <?php endif; ?>
        
        <?php if ($modulo17->modulo17_status == 1): ?>
          <li class="submenu paginasgnovo listarpaginas paginascategoria paginaseditar">
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-file-text-o"></i></span>
                <span class="text">Páginas</span>
                <span class="arrow"></span>
            </a>
            <ul>
                <li class="paginasgnovo"><a href="paginas/novo/">Adicionar</a></li>
                <li class="listarpaginas"><a href="paginas/">Listar</a></li>
                <li class="paginascategoria"><a href="categoria/paginas/">Gerenciar Categoria</a></li>

            </ul>
        </li>
        <?php endif; ?> 
        
        
        <?php if ($modulo10->modulo10_status == 1): ?>
        <li class="submenu blognovo listarblog categoria blogeditar comentario">
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-pencil"></i></span>
                <span class="text">Notícias</span>
                <span class="arrow"></span>
            </a>
            <ul>
                <li class="blognovo"><a href="post/novo/">Adiciconar</a></li>
                <li class="listarblog"><a href="posts/">Listar</a></li>
                <li class="categoria"><a href="categoria/post/">Gerenciar Categoria</a></li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if ($modulo7->modulo7_status == 1): ?>
        <li class="submenu portfoliognovo listarportfolio portfoliocategoria portfolioeditar">
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-wrench"></i></span>
                <span class="text">Associados</span> <!--Portfólio -->
                <span class="arrow"></span>
            </a>
            <ul>
                <li class="portfoliognovo"><a href="portfolio/novo/">Adicionar</a></li>
                <li class="listarportfolio"><a href="portfolio/">Listar</a></li>
                <li class="portfoliocategoria"><a href="categoria/portfolio/">Gerenciar Categoria</a></li>

            </ul>
        </li>
        <?php endif; ?>
        <?php if ($modulo12->modulo12_status == 1): ?>
        <li class="submenu videognovo listarvideo videocategoria videoeditar">
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-youtube"></i></span>
                <span class="text">Vídeos</span>
                <span class="arrow"></span>
            </a>
            <ul>
                <li class="videognovo"><a href="video/novo/">Adicionar</a></li>
                <li class="listarvideo"><a href="video/">Listar</a></li>
                <li class="videocategoria"><a href="categoria/video/">Gerenciar Categoria</a></li>

            </ul>
        </li>
        <?php endif; ?>

        <?php if ($modulo15->modulo15_status == 1): ?>
          <li class="submenu cadastros newsletter">
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-envelope-o"></i></span>
                <span class="text">Newsletter</span>
                <span class="arrow"></span>
            </a>
            <ul>
                <li class="cadastros"><a href="cadastro/">Cadastros</a></li>
                <li class="newsletter"><a href="newsletter/">Mensagens</a></li>

            </ul>
        </li>
        <?php endif; ?>
        
        <?php if ($modulo18->modulo18_status == 1): ?>
          <li class="submenu cotacaonovo">
            <a href="javascript:void(0);">
                <span class="icon"><i class="fa fa-money"></i></span>
                <span class="text">Cotação</span>
                <span class="arrow"></span>
            </a>
            <ul>
                <li class="cotacaonovo"><a href="cotacao/">Cadastros</a></li>
                <!-- <li class="cotacao"><a href="cotacao/">Mensagens</a></li> -->

            </ul>
        </li>
        <?php endif; ?>

        <li class="sidebar-category">
            <span>Configuração</span>
            <span class="pull-right"><i class="fa fa-cog"></i></span>
        </li>
        <li id="usuario">
            <a href="usuario/">
                <span class="icon"><i class="fa fa-user"></i></span>
                <span class="text">Usuário </span>
            </a>
        </li>
        <li id="seo">
            <a href="seo/">
                <span class="icon"><i class="fa fa-globe"></i></span>
                <span class="text">SEO </span>
            </a>
        </li>
        <li id="social">
            <a href="social/">
                <span class="icon"><i class="fa fa-facebook-f"></i></span>
                <span class="text">Redes Sociais </span>
            </a>
        </li>
        <li id="contato">
            <a href="contato/">
                <span class="icon"><i class="fa fa-phone"></i></span>
                <span class="text">Contato </span>
            </a>
        </li>
        <li id="smtp">
            <a href="config/email/">
                <span class="icon"><i class="fa fa-envelope"></i></span>
                <span class="text">SMTP </span>
            </a>
        </li>
        <li class="sidebar-category">
            <span><span class="hidden-sidebar-minimize"></span> &nbsp;</span>
        </li>
    </ul>
</aside>