<?php

require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}
function incluir() {
    $cotacao = new Cotacao();
    $cotacao->cotacao_nome     = addslashes($_POST['cotacao_nome']);
    $cotacao->cotacao_email    = addslashes($_POST['cotacao_email']);
    $cotacao->cotacao_celular  = addslashes($_POST['cotacao_celular']);
    $cotacao->cotacao_placa_veiculo = addslashes($_POST['cotacao_placa_veiculo']);
    $cotacao->cotacao_logradouro    = addslashes($_POST['cotacao_logradouro']);
    $cotacao->cotacao_status_atendimento = addslashes($_POST['cotacao_status_atendimento']);
    $cotacao->cotacao_observacao    = addslashes($_POST['cotacao_observacao']);
    $cotacao->cotacao_cadastro      = addslashes($_POST['cotacao_cadastro']);
    $cotacao->incluir();
    Filter::redirect("cotacao/?success");
}



function remover() {
    if (isset($_REQUEST['id'])) {
        $id = intval($_REQUEST['id']);
        $r = new Cotacao();
        $r->cotacao_id = $id;
        $r->remover();
        Filter :: redirect("cotacao/?success");
    }
}

function Json() {
        $j = new Cotacao();
        $j->cotacao_id =  intval($_REQUEST['cotacao_id']);
        echo $j->JSON();
}

function atualizar() {

    $cotacao = new Cotacao();

    

    $cotacao->cotacao_nome     = addslashes($_POST['cotacao_nome']);
    $cotacao->cotacao_email    = addslashes($_POST['cotacao_email']);
    $cotacao->cotacao_celular  = addslashes($_POST['cotacao_celular']);
    $cotacao->cotacao_placa_veiculo = addslashes($_POST['cotacao_placa_veiculo']);
    $cotacao->cotacao_logradouro    = addslashes($_POST['cotacao_logradouro']);
    $cotacao->cotacao_status_atendimento = addslashes($_POST['cotacao_status_atendimento']);
    $cotacao->cotacao_observacao    = addslashes($_POST['cotacao_observacao']);
    $cotacao->cotacao_cadastro      = addslashes($_POST['cotacao_cadastro']);

    $cotacao->cotacao_id     = addslashes($_POST['cotacao_id']);
    
    $cotacao->atualizar();


    // $cotacao_id    = intval($_POST['cotacao_id']);

    // $cotacao_nome     = addslashes($_POST['cotacao_nome']);
    // $cotacao_email    = addslashes($_POST['cotacao_email']);
    // $cotacao_celular  = addslashes($_POST['cotacao_celular']);
    // $cotacao_placa_veiculo = addslashes($_POST['cotacao_placa_veiculo']);
    // $cotacao_logradouro    = addslashes($_POST['cotacao_logradouro']);
    // $cotacao_status_atendimento = addslashes($_POST['cotacao_status_atendimento']);
    // $cotacao_observacao    = addslashes($_POST['cotacao_observacao']);

    
    // $cad_data = explode("/",$_POST['cotacao_data']);
    // $cotacao_data = $cad_data[2]."-".$cad_data[1]."-".$cad_data[0];
    
    // $a = new Cotacao();
    // $a->cotacao_id = $cotacao_id;

    // $a->cotacao_nome     = $cotacao_nome;
    // $a->cotacao_email    = $cotacao_email;
    // $a->cotacao_celular  = $cotacao_celular;
    // $a->cotacao_placa_veiculo = $cotacao_placa_veiculo;
    // $a->cotacao_logradouro    = $cotacao_logradouro;
    // $a->cotacao_status_atendimento = $cotacao_status_atendimento;
    // $a->cotacao_observacao    = $cotacao_observacao;


    // $a->cotacao_email = $cotacao_email;
    // $a->cotacao_data = $cotacao_data;

   // $a->atualizar();

    Filter :: redirect("cotacao/?success");
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}