<?php

require_once 'Controle.php';

class Mensagens extends Controle {

    public $db;
    public $mensagem_id;
    public $mensagem_nome;
    public $mensagem_texto;
    public $mensagem_data;

    public function __construct() {
        parent::__construct();
        $this->data = date('Y-m-d');
        $this->data_envio = date('Y-m-d H:i:s');
    }
    
    public function incluir() {
        $this->insert("mensagens", "mensagem_nome, mensagem_texto, mensagem_data", "'$this->mensagem_nome','$this->mensagem_texto','$this->data'");
    }
    
    public function atualizar() {
        $query = "mensagem_nome  = '$this->mensagem_nome', mensagem_texto = '$this->mensagem_texto', mensagem_data = '$this->mensagem_data'";
      
        $this->update("mensagens", "$query", "mensagem_id = '$this->mensagem_id'");
    }

    public function getMensagem() {
        $this->select("mensagens", "", "*", "", " WHERE mensagem_id = $this->mensagem_id", "");
    }

    public function getMensagens() {
        $this->select("mensagens", "", "*", "", "order by mensagem_data DESC", "");
    }
    
    public function Enviar() {
        $query = "mensagem_envio = '$this->data_envio'";
        $this->update("mensagens", "$query", "mensagem_id = '$this->mensagem_id'");
        
        global $mail;
        global $msg;
        global $cadastros;
        global $smtp;
        global $envios;
        global $qtd;
        
        $msg = new Mensagens();
        $msg->mensagem_id = $this->mensagem_id;
        $msg->getMensagem();
        
        $envios = new Modulo15();
        $envios->getModulo15();
        $qtd = 3600/$envios->modulo15_envios;
        
        $cadastros = new Cadastros();
        $cadastros->getCadastros();
        
        
        require_once 'phpmailer/class.phpmailer.php';
        if (isset($cadastros->db->data [0])):           
            foreach ($cadastros->db->data as $cad):
            
            
            $mail = new PHPMailer(true); //defaults to using php "mail()"; the true param means it will throw exceptions on errors, which we need to catch
            $smtp = new Smtpr();
            $smtp->getSmtp();
            $mail->IsSMTP();
            $mail->SMTPDebug = 2;	
            try {
            $html = utf8_decode($msg->mensagem_texto);
            $mail->Port = $smtp->smtp_port;
            $mail->Host = $smtp->smtp_host;
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'tls'; 
            $mail->SMTPKeepAlive = true;
            $mail->Username = $smtp->smtp_username;
            $mail->Password = $smtp->smtp_password;
            $mail->SetFrom($smtp->smtp_username, utf8_decode($smtp->smtp_fromname));
            $mail->AddReplyTo($smtp->smtp_username,utf8_decode($smtp->smtp_fromname));
            $mail->AddAddress($cad->cadastro_email, utf8_decode($cad->cadastro_nome));
            $mail->Subject = utf8_decode($msg->mensagem_nome);
            $mail->MsgHTML($html);
            $mail->Send();
            sleep($qtd); 
             } catch (phpmailerException $e) {
            //echo $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
           // echo $e->getMessage(); //Boring error messages from anything else!
            }
                        
           // Clear all addresses and attachments for next loop
           $mail->clearAddresses();
           $mail->clearAttachments();
                    
      
         endforeach;
         
        endif;  
           
        
        
    }

    public function remover() {
        $this->delete("mensagens", "mensagem_id = $this->mensagem_id");
    }


    public function Contar($id) {
        $this->getCount("mensagens", "");
    }
    
    public function JSON() {
        $this->getJSON("mensagens", "mensagem_id = '$this->mensagem_id'");
    }

}