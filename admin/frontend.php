<?php
include_once '../loader.php';
@session_start();
if (!isset($_SESSION['LOGADO']) || $_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}
$site = new Site();
$site->getMeta();

$icon = new Icon();
$icon->db = new DB;
$icon->getIcones();

$icon2 = new Icon();
$icon2->db = new DB;
$icon2->getIcones();

$icon3 = new Icon();
$icon3->db = new DB;
$icon3->getIcones();

$icon4 = new Icon();
$icon4->db = new DB;
$icon4->getIcones();


// echo "<pre>";
// var_dump($modulo18);die();

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- START @HEAD -->
    <head>
        <?php require_once './base.php'; ?>
        <!-- START @META SECTION -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title><?= $site->site_meta_titulo ?></title>
        <!--/ END META SECTION -->

        <!-- START @FAVICONS -->
        <link href="./assets/img/ico/<?= $modulo_aparencia->modulo_aparencia_favicon ?>" rel="shortcut icon">
        <!--/ END FAVICONS -->

        <!-- START @FONT STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <!--/ END FONT STYLES -->

        <!-- START @GLOBAL MANDATORY STYLES -->
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- START @PAGE LEVEL STYLES -->
        <link href="./assets/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="./assets/css/animate.min.css" rel="stylesheet">
        <link href="./assets/css/bootstrap-tagsinput.css" rel="stylesheet">
        <link href="./assets/css/jasny-bootstrap-fileinput.min.css" rel="stylesheet">
        <link href="./assets/css/chosen.min.css" rel="stylesheet">
        <link href="./assets/css/ion.rangeSlider.css" rel="stylesheet">
        <link href="./assets/css/chosen.min.css" rel="stylesheet">
        <!--/ END PAGE LEVEL STYLES -->

        <!-- START @THEME STYLES -->
        <link href="./assets/css/reset.css" rel="stylesheet">
        <link href="./assets/css/layout.css" rel="stylesheet">
        <link href="./assets/css/components.css" rel="stylesheet">
        <link href="./assets/css/plugins.css" rel="stylesheet">
        <link href="./assets/css/themes/default.theme.css" rel="stylesheet" id="theme">
        <link href="./assets/css/custom.css" rel="stylesheet">
        <link href="./assets/css/jquery.rtnotify.css" rel="stylesheet">
        <link href="./assets/css/noty_theme_default.css" rel="stylesheet">
        
        <!--/ END THEME STYLES -->

        <!-- START @IE SUPPORT -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="./assets/js/html5shiv.min.js"></script>
        <script src="./assets/js/respond.min.js"></script>
        <![endif]-->
        <!--/ END IE SUPPORT -->
    </head>
    <body>
<script type="text/javascript" src="assets/tiny_mce/tinymce.js" id="richedit"></script>
<script type="text/javascript">
 tinymce.init({
  selector: 'textarea#modulo3_descricao,textarea#modulo1_conteudo1',
  language: 'pt_BR',
  convert_urls:false,
  height: 600,
  theme: 'modern',
  
  images_upload_base_path: '<?= str_replace("/admin","",Validacao::getBaseUrl()); ?>/images/',
  plugins: [
    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak imagetools",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
  ],

  toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
  toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
  toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | visualchars visualblocks nonbreaking template pagebreak restoredraft",


  menubar: false,
  toolbar_items_size: 'small',

  style_formats: [{
    title: 'Texto negrito',
    inline: 'b'
  }, {
    title: 'Texto vermelho',
    inline: 'span',
    styles: {
      color: '#ff0000'
    }
  }, {
    title: 'Cabeçalho vernelho',
    block: 'h1',
    styles: {
      color: '#ff0000'
    }
  }, {
    title: 'Examplo 1',
    inline: 'span',
    classes: 'example1'
  }, {
    title: 'Examplo 2',
    inline: 'span',
    classes: 'example2'
  }, {
    title: 'Estilo de tabelas'
  }, {
    title: 'Tabela linha 1',
    selector: 'tr',
    classes: 'tablerow1'
  }],

  templates: [{
    title: 'Testar template 1',
    content: 'Teste 1'
  }, {
    title: 'Testar template 2',
    content: 'Teste 2'
  }],
  content_css: [
   
    '//www.tinymce.com/css/codepen.min.css'
  ]
});
</script> 
        <!--[if lt IE 9]>
        <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <section id="wrapper" class="page-sound">
            <?php require_once './navegacao.php'; ?>
            <?php require_once './menu.php'; ?>
            <section id="page-content">
                <div class="header-content">
                    <h2><i class="fa fa-leaf"></i>  <span>Gerenciar Módulos</span></h2>
                    <div class="breadcrumb-wrapper hidden-xs">
                        <span class="label">Você está em :</span>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="home/">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Frontend</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="body-content animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel rounded shadow">
                                <div class="panel-sub-heading">
                                    <div class="callout callout-info" style="padding-top: 19px;"><p><strong>Gerenciar Home (Textos, Cores, Imagens, Temas e outros...)</strong></p></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="panel-body no-padding">
                                    <div class="form-body">
                                        <div class="panel-group" id="accordion">
                                            <!-- **************************** MODULO APARÊNCIA ****************************-->
                                            <div class="panel panel-custom">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#aparencia">
                                                            <i class="fa fa-list"></i> Aparência do Site<span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="aparencia" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form enctype="multipart/form-data" method="post" action="modulo_aparencia_fn.php?acao=atualizar">
                                                        
                                                            
                                                             <div class="form-group" >
                                                             <label class="control-label"><i class="fa fa-paint-brush"></i> Cores do site</label>
                                                             <div class="mp-color">
                                                               <input type="radio" class="form-control" id="blue" name="modulo_aparencia_cor" value="blue" title="Azul"/><span class="cor blue" data-color="blue" title="Azul"></span>
                                                               <input type="radio" class="form-control" id="blaze" name="modulo_aparencia_cor" value="blaze" title="Verde Montanha"/><span class="cor blaze" data-color="blaze" title="Verde Montanha"></span>
                                                               <input type="radio" class="form-control" id="blue2" name="modulo_aparencia_cor" value="blue2" title="Azul Celeste"/><span class="cor blue2" data-color="blue2" title="Azul Celeste"></span>
                                                               <input type="radio" class="form-control" id="green" name="modulo_aparencia_cor" value="green" title="Verde-mar"/><span class="cor green" data-color="green" title="Verde-mar"></span>
                                                               <input type="radio" class="form-control" id="orange" name="modulo_aparencia_cor" value="orange" title="Coral"/><span class="cor orange" data-color="orange" title="Coral"></span>
                                                               <input type="radio" class="form-control" id="pink" name="modulo_aparencia_cor" value="pink" title="Rosa"/><span class="cor pink" data-color="pink" title="Rosa"></span>
                                                               <input type="radio" class="form-control" id="purple" name="modulo_aparencia_cor" value="purple" title="Violeta"/><span class="cor purple" data-color="purple" title="Violeta"></span>
                                                               <input type="radio" class="form-control" id="red" name="modulo_aparencia_cor" value="red" title="Vermelho"/><span class="cor red" data-color="red" title="Vermelho"></span>
                                                               <input type="radio" class="form-control" id="gold" name="modulo_aparencia_cor" value="gold" title="Amarelo Ouro"/><span class="cor gold" data-color="gold" title="Amarelo Ouro"></span>
                                                               <input type="radio" class="form-control" id="dark" name="modulo_aparencia_cor" value="dark" title="Preto/Cinza"/><span class="cor dark" data-color="dark" title="Preto/Cinza"></span>
                                                               <input type="radio" class="form-control" id="bordo" name="modulo_aparencia_cor" value="bordo" title="Bordô"/><span class="cor bordo" data-color="bordo" title="Bordô"></span>
                                                               <input type="radio" class="form-control" id="coffee" name="modulo_aparencia_cor" value="coffee" title="Café"/><span class="cor coffee" data-color="coffee" title="Café"></span>
                                                               <input type="radio" class="form-control" id="royal" name="modulo_aparencia_cor" value="royal" title="Azul Royal"/><span class="cor royal" data-color="royal" title="Azul Royal"></span>
                                                               <input type="radio" class="form-control" id="yellow" name="modulo_aparencia_cor" value="yellow" title="Amarelo Canário"/><span class="cor yellow" data-color="yellow" title="Amarelo Canário"></span>
                                                               <input type="radio" class="form-control" id="red_blue" name="modulo_aparencia_cor" value="red_blue" title="Azul/Vermelho"/><span class="cor red_blue" data-color="red_blue" title="Azul/Vermelho"></span>
                                                               <input type="radio" class="form-control" id="cherry" name="modulo_aparencia_cor" value="cherry" title="Cereja"/><span class="cor cherry" data-color="cherry" title="Cereja"></span>
                                                              </div>
                                                             <div style="clear: both;"></div>
                                                             </div>
                                                            
                                                            <div class="form-group">
                                                                <label class="control-label"><i class="fa fa-"></i>Ativar SlideShow?</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo_aparencia_slide" name="modulo_aparencia_slide" style="text-transform: uppercase;" >
                                                                    <option value="">Selecione um tipo</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>    
                                                            
                                                            
                                                            <div class="form-group">
                                                                <label class="control-label"><i class="fa fa-"></i>Grau da Película SlideShow</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo_aparencia_pelicula" name="modulo_aparencia_pelicula" style="text-transform: uppercase;" >
                                                                    <option value="">Selecione um tipo</option>
                                                                    <option value="0">Nenhuma</option>
                                                                    <option value="20">Clean 20%</option>
                                                                    <option value="40">Soft 40%</option>
                                                                    <option value="60">Padrão 60%</option>
                                                                    <option value="80">Dark 80%</option>
                                                                </select>
                                                            </div>         
                                                            
                                                            <div class="form-group">
                                                                <label class="control-label"><i class="fa fa-"></i>Idioma Principal</label><span class="small pull-right">[o sistema irá traduzir automaticamente de português para o idioma escolhido]</span>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo_aparencia_idioma" name="modulo_aparencia_idioma" style="text-transform: uppercase;" >
                                                                    <option value="">Selecione um idioma</option>
                                                                    <option value="pt">Português</option>
                                                                    <option value="de">Alemão</option>
                                                                    <option value="zh-TW">Chinês (tradicional)</option>
                                                                    <option value="fr">Francês</option>
                                                                    <option value="en">Inglês</option>
                                                                    <option value="it">Italiano</option>
                                                                    <option value="ja">Japonês</option>
                                                                    <option value="ru">Russo</option>
                                                                </select>
                                                            </div>                                                 

                                                            
                                                             <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="row-fluid">
                                                                            <div class="col-md-10 pull-right">
                                                                                <label class="control-label">Favicon</label>
                                                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                    <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo_aparencia_favicon" name="modulo_aparencia_favicon"></span>
                                                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <?php if (!empty($modulo_aparencia->modulo_aparencia_favicon)): ?>
                                                                                    <img src="../thumb.php?w=32&h=32&zc=0&src=./admin//assets/img/ico/<?= $modulo_aparencia->modulo_aparencia_favicon ?>" class="img-thumbnail" style="margin: 4px auto"/>
                                                                                <?php else: ?>
                                                                                    <img src="../thumb.php?w=32&h=32&zc=0&src=images/nopic.jpg" class="img-thumbnail" />
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                         
                                                            
                                                             <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="row-fluid">
                                                                            <div class="col-md-10 pull-right">
                                                                                <label class="control-label">Imagem da Logo</label>
                                                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                    <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo_aparencia_logo" name="modulo_aparencia_logo"></span>
                                                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <?php if (!empty($modulo_aparencia->modulo_aparencia_logo)): ?>
                                                                                    <img src="../thumb.php?w=200&zc=0&src=images/<?= $modulo_aparencia->modulo_aparencia_logo ?>" class="img-thumbnail" style="margin: 4px auto"/>
                                                                                <?php else: ?>
                                                                                    <img src="../thumb.php?w=200&zc=0&src=images/nopic.jpg" class="img-thumbnail" />
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                        
                                                            <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="row-fluid">
                                                                            <div class="col-md-10 pull-right">
                                                                                <label class="control-label">Imagem do Rodapé</label>
                                                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                    <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo_aparencia_rodape" name="modulo_aparencia_rodape"></span>
                                                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <?php if (!empty($modulo_aparencia->modulo_aparencia_rodape)): ?>
                                                                                    <img src="../thumb.php?w=200&zc=0&src=images/<?= $modulo_aparencia->modulo_aparencia_rodape ?>" class="img-thumbnail" style="margin: 4px auto; background: #232328"/>
                                                                                <?php else: ?>
                                                                                    <img src="../thumb.php?w=200&zc=0&src=images/nopic.jpg" class="img-thumbnail" />
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <input class="form-control rounded" type="hidden" id="modulo_aparencia_id" name="modulo_aparencia_id" value="<?= $modulo_aparencia->modulo_aparencia_id ?>" />
                                                            
                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO APARÊNCIA ****************************-->
                                            
                                            <!-- **************************** MODULO CONTEUDO TOPO ****************************-->
                                            <div class="panel panel-custom" <?php if ($modulo1->modulo1_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#One">
                                                            <i class="fa fa-list"></i> Conteúdo Topo <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="One" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form enctype="multipart/form-data" method="post" action="modulo1_fn.php?acao=atualizar">
                                                            <div class="form-group">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo1_status" name="modulo1_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>


                                                            <br />
                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <textarea class="form-control rounded" type="text" id="modulo1_nome"  name="modulo1_nome" style="max-height:50px;" placeholder=""><?= stripslashes($modulo1->modulo1_nome) ?></textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Subtítulo </label>
                                                                <textarea class="form-control rounded" type="text" id="modulo1_subtitulo1"  name="modulo1_subtitulo1" style="max-height:50px;" placeholder=""><?= stripslashes($modulo1->modulo1_subtitulo1) ?></textarea>
                                                                <input type="hidden" id="modulo1_id"  name="modulo1_id" value="<?= $modulo1->modulo1_id ?>" />
                                                            </div>
                                                            
                                                            <div style="margin-bottom: 20px;"></div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">Conteúdo</label>
                                                                        <textarea class="form-control rounded" type="text" id="modulo1_conteudo1"  name="modulo1_conteudo1" style="max-height:50px;"><?= stripslashes($modulo1->modulo1_conteudo1) ?></textarea>
                                                                    </div>

                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO CONTEUDO TOPO ****************************-->

                                            <!-- **************************** MODULO MENU FIXO ****************************-->
                                            <div class="panel panel-custom">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#Two">
                                                            <i class="fa fa-list"></i> Menu Fixo <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="Two" class="panel-collapse collapse">
                                                    <form method="post" action="modulo2_fn.php?acao=atualizar">
                                                        <div class="panel-body">

                                                            <div class="form-group hidden">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo2_status" name="modulo2_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">1. Menu Home</label>
                                                                <input class="form-control rounded" type="text" name="modulo2_nome" value="<?= stripslashes($modulo2->modulo2_nome) ?>" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">2. Menu Sobre</label>
                                                                <input class="form-control rounded" type="text" name="modulo2_nome1" value="<?= stripslashes($modulo2->modulo2_nome1) ?>" />
                                                            </div>
                                                            
                                                              <div class="form-group col-md-6">
                                                                <label class="control-label">3. Menu Serviços</label>
                                                                <input class="form-control rounded" type="text" name="modulo2_nome6" value="<?= stripslashes($modulo2->modulo2_nome6) ?>" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">4. Menu Portfólio</label>
                                                                <input class="form-control rounded" type="text" name="modulo2_nome2" value="<?= stripslashes($modulo2->modulo2_nome2) ?>" />
                                                            </div>
                                                            
                                                             <div class="form-group col-md-6">
                                                                <label class="control-label">5. Menu Equipe</label>
                                                                <input class="form-control rounded" type="text" name="modulo2_nome7" value="<?= stripslashes($modulo2->modulo2_nome7) ?>" />
                                                            </div>                                                                                                                        

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">6. Menu Blog</label>
                                                                <input class="form-control rounded" type="text" name="modulo2_nome3" value="<?= stripslashes($modulo2->modulo2_nome3) ?>" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">7. Menu Vídeos</label>
                                                                <input class="form-control rounded" type="text" name="modulo2_nome4" value="<?= stripslashes($modulo2->modulo2_nome4) ?>" />
                                                            </div>
                                                            
                                                            <div class="form-group col-md-6">
                                                                <label class="control-label">8. Menu Contato</label>
                                                                <input class="form-control rounded" type="text" name="modulo2_nome5" value="<?= stripslashes($modulo2->modulo2_nome5) ?>" />
                                                            </div>


                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <input type="hidden" id="modulo2_id" name="modulo2_id" value="<?= $modulo2->modulo2_id ?>" />
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO MENU FIXO ****************************-->
                                            
                                            <!-- **************************** MODULO QUEM SOMOS ****************************-->
                                            <div class="panel panel-custom" <?php if ($modulo3->modulo3_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#Three">
                                                            <i class="fa fa-list"></i> Quem Somos <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="Three" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form method="post" enctype="multipart/form-data" action="modulo3_fn.php?acao=atualizar">
                                                            <div class="form-group">
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                        <select class="form-control input-sm mb-15 rounded" id="modulo3_status" name="modulo3_status">
                                                                            <option value="">Selecione uma Opção</option>
                                                                            <option value="1">Ativado</option>
                                                                            <option value="0">Desativado</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">Título</label>
                                                                        <textarea class="form-control rounded" type="text"  name="modulo3_nome" style="max-height:50px;"><?= stripslashes($modulo3->modulo3_nome) ?></textarea>
                                                                    </div>
                                                                     <div class="row">
                                                                        <div class="form-group">
                                                                            <div class="row-fluid">
                                                                                <div class="col-md-9 pull-right">
                                                                                    <label class="control-label">Imagem Topo</label>
                                                                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                        <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                        <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo3_imagem" name="modulo3_imagem"></span>
                                                                                        <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                <?php if (!empty($modulo3->modulo3_imagem)): ?>
                                                                                  <img src="../thumb.php?w=260&zc=0&src=images/<?= $modulo3->modulo3_imagem ?>" class="img-thumbnail" />
                                                                                   <?php else: ?>
                                                                                  <img src="../thumb.php?w=260&zc=0&src=images/bread-bg.jpg" class="img-thumbnail" />
                                                                                  <?php endif; ?>
                                                                               
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div style="margin-bottom: 20px;"></div>
                                                                    <div class="row">
                                                                        <div class="form-group">
                                                                            <div class="row-fluid">
                                                                                <div class="col-md-10 pull-right">
                                                                                    <label class="control-label">Foto</label>
                                                                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                        <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                        <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo3_foto" name="modulo3_foto"></span>
                                                                                        <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <img src="../thumb.php?w=150&h=160&zc=0&src=images/<?= $modulo3->modulo3_foto ?>" class="img-thumbnail" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div style="margin-bottom: 20px;"></div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">Descrição</label>
                                                                       
                                                                        <textarea class="form-control rounded" type="text" id="modulo3_descricao"  name="modulo3_descricao" style="max-height:50px;"><?= stripslashes($modulo3->modulo3_descricao) ?></textarea>
                                                                    </div>
                                                                    <div class="form-footer">
                                                                        <div class="text-center">
                                                                            <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                            <input class="form-control rounded" type="hidden" id="modulo3_id" name="modulo3_id" value="<?= $modulo3->modulo3_id ?>" />
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO QUEM SOMOS ****************************-->
                                            
                                            
    

                                            <!-- **************************** MODULO PUBLICIDADE ****************************-->
                                            <div class="panel panel-custom"  <?php if ($modulo4->modulo4_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#four">
                                                            <i class="fa fa-list"></i> Publicidade  <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>
                                                
                                                

                                                <div id="four" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo4_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                        
                                                        
                                                  <div class="form-group form-group-divider" style="margin-bottom: 10px;">
                                                                <div class="form-inner">
                                                                    <h4 class="no-margin"><span class="label label-success label-circle"></span> Adicione seus códigos de anúncios (por ex. Google Adsense) </h4>
                                                                </div>
                                                            </div>        
                                                            <div class="form-group">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo4_status" name="modulo4_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                                
                                                           <div class="form-group form-group-divider" style="margin: 20px 0 10px 0;">
                                                                <div class="form-inner">
                                                                    <h5 class="no-margin"><span class="label label-success label-circle"></span>Anúncio Top </h5>
                                                                </div>
                                                             </div>               
                                                                
                                                                <div class="form-group">
                                                                    <label class="control-label">Código HTML (largura máx. 1140px)</label>
                                                                    <textarea class="form-control rounded" type="text"  name="modulo4_top" style="min-height:150px;" placeholder="Insira o código HTML aqui"><?= stripslashes($modulo4->modulo4_top) ?></textarea>
                                                                </div>
                                                                
                                                              <div class="form-group form-group-divider" style="margin: 20px 0 10px 0;">
                                                                <div class="form-inner">
                                                                    <h5 class="no-margin"><span class="label label-success label-circle"></span>Anúncio Lateral </h5>
                                                                </div>
                                                             </div>      
                                                             
                                                             <div class="form-group">
                                                                    <label class="control-label">Título</label>
                                                                    <input class="form-control rounded" type="text"  name="modulo4_side1" placeholder="Ex: Publicidade" value="<?= stripslashes($modulo4->modulo4_side1) ?>" />
                                                                </div>     
                                                                
                                                                <div class="form-group">
                                                                    <label class="control-label">Código HTML (largura máx. 360px)</label>
                                                                    <textarea class="form-control rounded" type="text"  name="modulo4_side" style="min-height:150px;" placeholder="Insira o código HTML aqui"><?= stripslashes($modulo4->modulo4_side) ?></textarea>
                                                                </div>
                                                                
                                                                
                                                                <div class="form-group form-group-divider" style="margin: 20px 0 10px 0;">
                                                                <div class="form-inner">
                                                                    <h5 class="no-margin"><span class="label label-success label-circle"></span>Anúncio Rodapé </h5>
                                                                </div>
                                                                </div>  
                                                                       
                                                                
                                                                <div class="form-group">
                                                                    <label class="control-label">Código HTML (largura máx. 1140px)</label>
                                                                    <textarea class="form-control rounded" type="text"  name="modulo4_bottom" style="min-height:150px;" placeholder="Insira o código HTML aqui"><?= stripslashes($modulo4->modulo4_bottom) ?></textarea>
                                                                </div>

                                                              
                                                                <div style="margin-bottom: 20px;"></div>

                                                                <div class="form-footer">
                                                                    <div class="text-center">
                                                                        <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                        <input class="form-control rounded" type="hidden" id="modulo4_id" name="modulo4_id" value="<?= $modulo4->modulo4_id ?>" />
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO PUBLICIDADE ****************************-->
                                            
                                            
                                             <!-- **************************** MODULO INSTAGRAM ****************************-->
                                            <div class="panel panel-custom hidden"  <?php if ($modulo16->modulo16_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#sixteen">
                                                            <i class="fa fa-list"></i> Instagram  <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>
                                                
                                                

                                                <div id="sixteen" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo16_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                        
                                                        
                                                  <div class="form-group form-group-divider" style="margin-bottom: 10px;">
                                                                <div class="form-inner">
                                                                    <h4 class="no-margin"><span class="label label-success label-circle"></span> Adicione as fotos mais recentes do seu Instagram na coluna lateral do Blog</h4>
                                                                </div>
                                                            </div>        
                                                            <div class="form-group">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo16_status" name="modulo16_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                                
                                                              <div style="margin-bottom: 20px;"></div>
                                                                           
   
                                                             
                                                             <div class="form-group col-md-6">
                                                                    <label class="control-label">Título</label>
                                                                    <input class="form-control rounded" type="text"  name="modulo16_nome" placeholder="Ex: Instagram" value="<?= stripslashes($modulo16->modulo16_nome) ?>" />
                                                                </div>  
                                                                
                                                                 <div class="form-group col-md-6">
                                                                    <label class="control-label"> Usuário Instagram@</label>
                                                                    <input class="form-control rounded" type="text"  name="modulo16_userid" placeholder="Ex: microsoft" required value="<?= stripslashes($modulo16->modulo16_userid) ?>" />
                                                                </div>   

                                                              
                                                                <div style="margin-bottom: 20px;"></div>

                                                                <div class="form-footer">
                                                                    <div class="text-center">
                                                                        <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                        <input class="form-control rounded" type="hidden" id="modulo16_id" name="modulo16_id" value="<?= $modulo16->modulo16_id ?>" />
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO INSTAGRAM ****************************-->


                                            <!-- **************************** MODULO DEPOIMENTOS ****************************-->

                                            <div class="panel panel-custom" <?php if ($modulo5->modulo5_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordionfive" >
                                                            <i class="fa fa-list"></i> Depoimentos  <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordionfive" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo5_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo5_status" name="modulo5_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>
                                                            <br />
                                                            <div class="form-group">
                                                                <label class="col-sm-12 control-label">Depoimentos a serem exibidos</label>
                                                                <div class="col-sm-12">
                                                                    <div class="slider-primary">
                                                                        <input id="depoimento" type="text" name="modulo5_limite" value="<?= $modulo5->modulo5_limite ?>" />
                                                                    </div>
                                                                    <br />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo5_nome" style="max-height:50px;"><?= stripslashes($modulo5->modulo5_nome) ?></textarea>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Descricao</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo5_descricao" style="max-height:50px;"><?= stripslashes($modulo5->modulo5_descricao) ?></textarea>
                                                            </div>


                                                   
                                                            
                                                            <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="row-fluid">
                                                                            <div class="col-md-9 pull-right">
                                                                                <label class="control-label">Imagem</label>
                                                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                    <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo5_imagem" name="modulo5_imagem"></span>
                                                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <?php if (!empty($modulo5->modulo5_imagem)): ?>
                                                                                    <img src="../thumb.php?w=260&h=100&zc=0&src=images/<?= $modulo5->modulo5_imagem ?>" class="img-thumbnail" />
                                                                                <?php else: ?>
                                                                                    <img src="../thumb.php?w=260&h=100&zc=0&src=../img/testimonial.jpg" class="img-thumbnail" />
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div style="margin-bottom: 20px;"></div>

                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                    <input class="form-control rounded" type="hidden" id="modulo5_id" name="modulo5_id" value="<?= $modulo5->modulo5_id ?>" />
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                            <!-- **************************** MODULO DEPOIMENTOS ****************************-->

                                            <!-- **************************** MODULO SERVIÇOS ****************************-->
                                            <div class="panel panel-custom" <?php if ($modulo6->modulo6_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordionsix">
                                                            <i class="fa fa-list"></i> Serviços  <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordionsix" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo6_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group ">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo6_status" name="modulo6_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <input class="form-control rounded" type="text" name="modulo6_nome" value="<?= stripslashes($modulo6->modulo6_nome) ?>" />
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="control-label">Descricao</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo6_descricao" style="max-height:50px;"><?= stripslashes($modulo6->modulo6_descricao) ?></textarea>
                                                            </div>
                                                            
                                                            <div style="margin-bottom: 20px;"></div>


                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                    <input class="form-control rounded" type="hidden" id="modulo6_id" name="modulo6_id" value="<?= $modulo6->modulo6_id ?>" />
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO SERVIÇOS ****************************-->

                                            <!-- **************************** MODULO PORTFÓLIO ****************************-->
                                            <div class="panel panel-custom" <?php if ($modulo7->modulo7_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordionseven">
                                                            <i class="fa fa-list"></i> Associados <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordionseven" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo7_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group ">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo7_status" name="modulo7_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo7_nome" style="max-height:50px;"><?= stripslashes($modulo7->modulo7_nome) ?></textarea>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Descrição</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo7_descricao" style="max-height:50px;"><?= stripslashes($modulo7->modulo7_descricao) ?></textarea>
                                                                <input class="form-control rounded" type="hidden" id="modulo7_id" name="modulo7_id" value="<?= $modulo7->modulo7_id ?>" />
                                                            </div>
                                                            
                                                           <div class="row">
                                                                        <div class="form-group">
                                                                            <div class="row-fluid">
                                                                                <div class="col-md-9 pull-right">
                                                                                    <label class="control-label">Imagem Topo</label>
                                                                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                        <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                        <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo7_imagem" name="modulo7_imagem"></span>
                                                                                        <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                <?php if (!empty($modulo7->modulo7_imagem)): ?>
                                                                                  <img src="../thumb.php?w=260&zc=0&src=images/<?= $modulo7->modulo7_imagem ?>" class="img-thumbnail" />
                                                                                   <?php else: ?>
                                                                                  <img src="../thumb.php?w=260&zc=0&src=images/bread-bg.jpg" class="img-thumbnail" />
                                                                                  <?php endif; ?>
                                                                               
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                             <div style="margin-bottom: 20px;"></div>

                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO PORTFÓLIO ****************************-->
                                            
                                            <!-- **************************** MODULO EQUIPE ****************************-->
                                            <div class="panel panel-custom" <?php if ($modulo13->modulo13_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordionthirteen">
                                                            <i class="fa fa-list"></i> Equipe <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordionthirteen" class="panel-collapse collapse">
                                                    <form method="post" action="modulo13_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group ">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo13_status" name="modulo13_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo13_nome" style="max-height:50px;"><?= stripslashes($modulo13->modulo13_nome) ?></textarea>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Descrição</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo13_descricao" style="max-height:50px;"><?= stripslashes($modulo13->modulo13_descricao) ?></textarea>
                                                                <input class="form-control rounded" type="hidden" id="modulo13_id" name="modulo13_id" value="<?= $modulo13->modulo13_id ?>" />
                                                            </div>
                                                            
                                                            <div style="margin-bottom: 20px;"></div>

                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO EQUIPE ****************************-->
                                            
                                            
                                            <!-- **************************** MODULO VÍDEOS ****************************-->
                                            <div class="panel panel-custom" <?php if ($modulo12->modulo12_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordiontwelve">
                                                            <i class="fa fa-list"></i> Vídeos  <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordiontwelve" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo12_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo12_status" name="modulo12_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo12_nome" style="max-height:50px;"><?= stripslashes($modulo12->modulo12_nome) ?></textarea>
                                                            </div>
                                                            
                                                             <div class="form-group">
                                                                <label class="control-label">Descrição</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo12_descricao" style="max-height:50px;"><?= stripslashes($modulo12->modulo12_descricao) ?></textarea>
                                                                <input class="form-control rounded" type="hidden" id="modulo12_id" name="modulo12_id" value="<?= $modulo12->modulo12_id ?>" />
                                                            </div>
                                                            
                                                            <div class="row">
                                                                        <div class="form-group">
                                                                            <div class="row-fluid">
                                                                                <div class="col-md-9 pull-right">
                                                                                    <label class="control-label">Imagem Topo</label>
                                                                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                        <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                        <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo12_imagem" name="modulo12_imagem"></span>
                                                                                        <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                <?php if (!empty($modulo12->modulo12_imagem)): ?>
                                                                                  <img src="../thumb.php?w=260&zc=0&src=images/<?= $modulo12->modulo12_imagem ?>" class="img-thumbnail" />
                                                                                   <?php else: ?>
                                                                                  <img src="../thumb.php?w=260&zc=0&src=images/bread-bg.jpg" class="img-thumbnail" />
                                                                                  <?php endif; ?>
                                                                               
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                             <div style="margin-bottom: 20px;"></div>

                                                             <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO VÍDEOS ****************************-->
                                            
                                            
                                            <!-- **************************** MODULO PARCEIROS ****************************-->
                                            <div class="panel panel-custom" <?php if ($modulo8->modulo8_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordioneight" >
                                                            <i class="fa fa-list"></i> Parceiros  <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordioneight" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo8_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group ">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo8_status" name="modulo8_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo8_nome" style="max-height:50px;"><?= stripslashes($modulo8->modulo8_nome) ?></textarea>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Descriçao</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo8_descricao" style="max-height:50px;"><?= stripslashes($modulo8->modulo8_descricao) ?></textarea>
                                                                <input type="hidden" id="modulo8_id" name="modulo8_id" value="<?= $modulo8->modulo8_id ?>" />
                                                            </div>
                                                            
                                                            <div style="margin-bottom: 20px;"></div>

                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO PARCEIROS ****************************-->
                                            <!-- **************************** MODULO CONTATO ****************************-->
                                            <div class="panel panel-custom" <?php if ($modulo9->modulo9_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordionnine">
                                                            <i class="fa fa-list"></i> Contato  <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordionnine" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo9_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group ">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo9_status" name="modulo9_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <textarea class="form-control rounded" type="text"   name="modulo9_nome" style="max-height:50px;"><?= stripslashes($modulo9->modulo9_nome) ?></textarea>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Subtítulo</label>
                                                                <textarea class="form-control rounded" type="text"   name="modulo9_subtitulo" style="max-height:50px;"><?= stripslashes($modulo9->modulo9_subtitulo) ?></textarea>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Botão</label>
                                                                <input class="form-control rounded" type="text" name="modulo9_button" value="<?= stripslashes($modulo9->modulo9_button) ?>" />
                                                                <input class="form-control rounded" type="hidden" id="modulo9_id" name="modulo9_id" value="<?= $modulo9->modulo9_id ?>" />
                                                            </div>

                                                            <div class="form-group hidden">
                                                                <label class="control-label">Imagem</label>
                                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                    <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo9_imagem" name="modulo9_imagem"></span>
                                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                </div>
                                                            </div>
                                                            
                                                            <div style="margin-bottom: 20px;"></div>

                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO CONTATO ****************************-->
                                            <!-- **************************** MODULO BLOG ****************************-->
                                            <div class="panel  panel-custom" <?php if ($modulo10->modulo10_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordionten">
                                                            <i class="fa fa-list"></i> Noticias <span class="pull-right"><b class="fa fa-edit"></b> </span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordionten" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo10_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group ">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo10_status" name="modulo10_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>



                                                            <div class="form-group form-group-divider">
                                                                <div class="form-inner">
                                                                    <h4 class="no-margin"><span class="label label-success label-circle"></span> Posts </h4>
                                                                </div>
                                                            </div>

               
                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo10_nome" style="max-height:50px;" placeholder='Nós Amamos Escrever'><?= stripslashes($modulo10->modulo10_nome) ?></textarea>
                                                            </div>

                                                            
                                                            
                                                            
                                                            <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="row-fluid">
                                                                            <div class="col-md-9 pull-right">
                                                                                <label class="control-label">Imagem</label>
                                                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                    <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo10_imagem" name="modulo10_imagem"></span>
                                                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <?php if (!empty($modulo10->modulo10_imagem)): ?>
                                                                                    <img src="../thumb.php?w=260&h=100&zc=0&src=images/<?= $modulo10->modulo10_imagem ?>" class="img-thumbnail" />
                                                                                <?php else: ?>
                                                                                    <img src="../thumb.php?w=260&h=100&zc=0&src=../img/bread-bg.jpg" class="img-thumbnail" />
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <div style="margin-bottom: 20px;"></div>

                                                            <div class="form-group form-group-divider">
                                                                <div class="form-inner">
                                                                    <h4 class="no-margin"><span class="label label-success label-circle"></span> Home </h4>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-12 control-label">Paginação</label>
                                                                <div class="col-sm-12">
                                                                    <div class="slider-primary">
                                                                        <input id="pagina" type="text" name="modulo10_paginacao" value="<?= $modulo10->modulo10_paginacao ?>" />
                                                                    </div>
                                                                    <br />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <textarea class="form-control rounded" type="text" name="modulo10_button" style="max-height:50px;"><?= stripslashes($modulo10->modulo10_button) ?></textarea>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Subtítulo</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo10_subtitulo" style="max-height:50px;"><?= stripslashes($modulo10->modulo10_subtitulo) ?></textarea>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="control-label">Botão</label>
                                                                <input class="form-control rounded" type="text" name="modulo10_button1" value="<?= stripslashes($modulo10->modulo10_button1) ?>" />
                                                                <input class="form-control rounded" type="hidden" id="modulo10_id" name="modulo10_id" value="<?= $modulo10->modulo10_id ?>" />
                                                            </div>
                                                            
                                                             <div class="form-group ">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Permitir Comentários ?</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo10_comment" name="modulo10_comment">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Sim</option>
                                                                    <option value="0">Não</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="form-group ">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Mostrar Tags (Palavras-Chaves) no Post?</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo10_tags" name="modulo10_tags">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Sim</option>
                                                                    <option value="0">Não</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div style="margin-bottom: 20px;"></div>

                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO BLOG ****************************-->
                                            
                                            
                                            <!-- **************************** MODULO PAGINAS ****************************-->
                                            <div class="panel  panel-custom" <?php if ($modulo17->modulo17_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordionseventeen">
                                                            <i class="fa fa-list"></i> Páginas <span class="pull-right"><b class="fa fa-edit"></b> </span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordionseventeen" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo17_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group ">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo17_status" name="modulo17_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>
        
                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo17_nome" style="max-height:50px;" placeholder='Páginas'><?= stripslashes($modulo17->modulo17_nome) ?></textarea>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Subtítulo</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo17_subtitulo" placeholder="Nós Amamos Escrever" style="max-height:50px;"><?= stripslashes($modulo17->modulo17_subtitulo) ?></textarea>
                                                            </div>
                                                            
                                                            <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="row-fluid">
                                                                            <div class="col-md-9 pull-right">
                                                                                <label class="control-label">Imagem</label>
                                                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                    <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo17_imagem" name="modulo17_imagem"></span>
                                                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <?php if (!empty($modulo17->modulo17_imagem)): ?>
                                                                                    <img src="../thumb.php?w=260&h=100&zc=0&src=images/<?= $modulo17->modulo17_imagem ?>" class="img-thumbnail" />
                                                                                <?php else: ?>
                                                                                    <img src="../thumb.php?w=260&h=100&zc=0&src=../img/bread-bg.jpg" class="img-thumbnail" />
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <div style="margin-bottom: 20px;"></div>

                                                            <div class="form-group">
                                                                <label class="col-sm-12 control-label">Paginação</label>
                                                                <div class="col-sm-12">
                                                                    <div class="slider-primary">
                                                                        <input id="paginas" type="text" name="modulo17_paginacao" value="<?= $modulo17->modulo17_paginacao ?>" />
                                                                    </div>
                                                                    <br />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Botão</label>
                                                                <input class="form-control rounded" type="text" name="modulo17_button" value="<?= stripslashes($modulo17->modulo17_button) ?>" />
                                                                <input class="form-control rounded" type="hidden" id="modulo17_id" name="modulo17_id" value="<?= $modulo17->modulo17_id ?>" />
                                                            </div>
                                                            
                                                             <div class="form-group ">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Permitir Comentários ?</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo17_comment" name="modulo17_comment">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Sim</option>
                                                                    <option value="0">Não</option>
                                                                </select>
                                                            </div>
                                                            
                                                            
                                                            <div style="margin-bottom: 20px;"></div>

                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO PAGINAS ****************************-->
                                            
                                            
                                            <!-- **************************** MODULO INDICADORES ****************************-->
                                            <div class="panel  panel-custom" <?php if ($modulo14->modulo14_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordionfour">
                                                            <i class="fa fa-list"></i> Indicadores <span class="pull-right"><b class="fa fa-edit"></b> </span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordionfour" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo14_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo14_status" name="modulo14_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>


                                                              
                                                          
                                                                <div class="form-group col-md-3" id="icon1">
                                                                    <label class="control-label"> Ícone 1</label>
                                                                <select class="chosen-select1 mb-15" data-placeholder="Selecione um Ícone" id="modulo14_icon1" name="modulo14_icon1" style="display: none;">
                                                                  <option value="">Selecione um Ícone</option>
                                                                        <?php foreach ($icon->db->data as $icon) : ?>
                                                                        <option  style="font-family:'FontAwesome', sans-serif;" class="<?= $icon->icones_nome ?>" value="<?= $icon->icones_nome ?>" <?php if($modulo14->modulo14_icon1 == $icon->icones_nome) { echo 'selected="selected"'; }?>> &nbsp;&nbsp;<?= $icon->icones_nome ?></option>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                                    
                                                                 </div>
                                                                 
                                                                
                                                                <div class="form-group col-md-5">
                                                                    <label class="control-label">Título 1</label>
                                                                    <input class="form-control rounded" type="text"  name="modulo14_descricao1" value="<?= stripslashes($modulo14->modulo14_descricao1) ?>" />
                                                                </div>

                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label">Número 1</label>
                                                                    <input class="form-control rounded" type="number"  name="modulo14_text1" value="<?= stripslashes($modulo14->modulo14_text1) ?>">
                                                                </div>
                                                                <hr />
                                                                <div class="clearfix"></div>

                                                                <div class="form-group col-md-3" id="icon2">
                                                                    <label class="control-label"> Ícone 2</label>
                                                                     <select  class="chosen-select2 mb-15" data-placeholder="Selecione um Ícone" id="modulo14_icon2" name="modulo14_icon2">
                                                                        <option value="">Selecione um Ícone</option>
                                                                        <?php foreach ($icon2->db->data as $icon2) : ?>
                                                                        <option  style="font-family:'FontAwesome', sans-serif;" class="<?= $icon2->icones_nome ?>" value="<?= $icon2->icones_nome ?>" <?php if($modulo14->modulo14_icon2 == $icon2->icones_nome) { echo 'selected="selected"'; }?>> &nbsp;&nbsp;<?= $icon2->icones_nome ?></option>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                                </div>
                                                                
                                                                <div class="form-group col-md-5">
                                                                    <label class="control-label">Título 2</label>
                                                                    <input class="form-control rounded" type="text"  name="modulo14_descricao2" value="<?= stripslashes($modulo14->modulo14_descricao2) ?>" />
                                                                </div>

                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label">Número 2</label>
                                                                    <input class="form-control rounded" type="number"  name="modulo14_text2" value="<?= stripslashes($modulo14->modulo14_text2) ?>">
                                                                </div>
                                                                <hr />
                                                                <div class="clearfix"></div>

                                                                <div class="form-group col-md-3" id="icon3">
                                                                    <label class="control-label"> Ícone 3</label>
                                                                     <select  class="chosen-select3 mb-15" data-placeholder="Selecione um Ícone" id="modulo14_icon3" name="modulo14_icon3">
                                                                        <option value="">Selecione um Ícone</option>
                                                                        <?php foreach ($icon3->db->data as $icon3) : ?>
                                                                        <option  style="font-family:'FontAwesome', sans-serif;" class="<?= $icon3->icones_nome ?>" value="<?= $icon3->icones_nome ?>" <?php if($modulo14->modulo14_icon3 == $icon3->icones_nome) { echo 'selected="selected"'; }?>> &nbsp;&nbsp;<?= $icon3->icones_nome ?></option>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                                </div>
                                                                
                                                                <div class="form-group col-md-5">
                                                                    <label class="control-label">Título 3</label>
                                                                    <input class="form-control rounded" type="text"  name="modulo14_descricao3" value="<?= stripslashes($modulo14->modulo14_descricao3) ?>" />
                                                                </div>

                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label">Número 3</label>
                                                                    <input class="form-control rounded" type="number"  name="modulo14_text3" value="<?= stripslashes($modulo14->modulo14_text3) ?>">
                                                                </div>

                                                                <hr />
                                                                <div class="clearfix"></div>


                                                                <div class="form-group col-md-3" id="icon4">
                                                                    <label class="control-label"> Ícone 4</label>
                                                                     <select  class="chosen-select4 mb-15" data-placeholder="Selecione um Ícone" id="modulo14_icon4" name="modulo14_icon4">
                                                                        <option value="">Selecione um Ícone</option>
                                                                        <?php foreach ($icon4->db->data as $icon4) : ?>
                                                                        <option  style="font-family:'FontAwesome', sans-serif;" class="<?= $icon4->icones_nome ?>" value="<?= $icon4->icones_nome ?>" <?php if($modulo14->modulo14_icon4 == $icon4->icones_nome) { echo 'selected="selected"'; }?>> &nbsp;&nbsp;<?= $icon4->icones_nome ?></option>
                                                                        <?php endforeach; ?>
                                                                    </select>
                                                                </div>
                                                                
                                                                <div class="form-group col-md-5">
                                                                    <label class="control-label">Título 4</label>
                                                                    <input class="form-control rounded" type="text"  name="modulo14_descricao4" value="<?= stripslashes($modulo14->modulo14_descricao4) ?>" />
                                                                </div>

                                                                <div class="form-group col-md-4">
                                                                    <label class="control-label">Número 4</label>
                                                                    <input class="form-control rounded" type="number"  name="modulo14_text4" value="<?= stripslashes($modulo14->modulo14_text4) ?>" />
                                                                </div>

                                                                <hr />
                                                                <div class="clearfix"></div>

                                                                
                                                                <div style="margin-bottom: 20px;"></div>
                                                                 <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="row-fluid">
                                                                            <div class="col-md-9 pull-right">
                                                                                <label class="control-label">Imagem de fundo</label>
                                                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                    <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo14_imagem" name="modulo14_imagem"></span>
                                                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <?php if (!empty($modulo14->modulo14_imagem)): ?>
                                                                                    <img src="../thumb.php?w=260&h=100&zc=0&src=images/<?= $modulo14->modulo14_imagem ?>" class="img-thumbnail" />
                                                                                <?php else: ?>
                                                                                    <img src="../thumb.php?w=260&h=100&zc=0&src=../img/count-bg.jpg" class="img-thumbnail" />
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div style="margin-bottom: 20px;"></div>

                                                                <div class="form-footer">
                                                                    <div class="text-center">
                                                                        <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                        <input class="form-control rounded" type="hidden" id="modulo14_id" name="modulo14_id" value="<?= $modulo14->modulo14_id ?>" />
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO INDICADORES ****************************-->
                                            
                                            
                                            <!-- **************************** MODULO NEWSLETTER ****************************-->
                                            <div class="panel  panel-custom" <?php if ($modulo15->modulo15_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordionfifteen">
                                                            <i class="fa fa-list"></i> Newsletter <span class="pull-right"><b class="fa fa-edit"></b> </span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordionfifteen" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo15_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group ">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo15_status" name="modulo15_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>


               
                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <input class="form-control rounded" type="text"  name="modulo15_nome" placeholder="Assinar a Newsletter" value="<?= stripslashes($modulo15->modulo15_nome) ?>"/>
                                                            </div>
                                                            
                                                           <div class="form-group">
                                                              <label class="control-label">Subtítulo</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo15_subtitulo" style="max-height:50px;"><?= stripslashes($modulo15->modulo15_subtitulo) ?></textarea>
                                                            </div>

                                                            
                                                            <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="row-fluid">
                                                                            <div class="col-md-9 pull-right">
                                                                                <label class="control-label">Imagem</label>
                                                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                    <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo15_imagem" name="modulo15_imagem"></span>
                                                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <?php if (!empty($modulo15->modulo15_imagem)): ?>
                                                                                    <img src="../thumb.php?w=260&h=100&zc=0&src=images/<?= $modulo15->modulo15_imagem ?>" class="img-thumbnail" />
                                                                                <?php else: ?>
                                                                                    <img src="../thumb.php?w=260&h=100&zc=0&src=../img/news.jpg" class="img-thumbnail" />
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div style="margin-bottom: 20px;"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-12 control-label">Envios por hora <span class="small pull-right">[ajuste conforme o limite da sua hospedagem - deixe 0 para ilimitado]</span></label>
                                                                <div class="col-sm-12">
                                                                    <div class="slider-primary">
                                                                        <input id="envios" type="text" name="modulo15_envios" value="<?= $modulo15->modulo15_paginacao ?>" />
                                                                    </div>
                                                                    <br />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Botão</label>
                                                                <input class="form-control rounded" type="text" name="modulo15_button" style="max-width:200px;" value="<?= stripslashes($modulo15->modulo15_button) ?>" />
                                                                <input class="form-control rounded" type="hidden" id="modulo15_id" name="modulo15_id" value="<?= $modulo15->modulo15_id ?>" />
                                                            </div>
                                                            
                                                            <div style="margin-bottom: 20px;"></div>

                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO NEWSLETTER ****************************-->

                                            <!-- **************************** MODULO COTACAO ****************************-->
                                            <div class="panel  panel-custom" <?php  if($modulo18->modulo18_status == 0): ?>style="background: #E76464;"<?php endif; ?>>
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordioneighteen">
                                                            <i class="fa fa-list"></i> Cotação <span class="pull-right"><b class="fa fa-edit"></b> </span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordioneighteen" class="panel-collapse collapse">
                                                    <form enctype="multipart/form-data" method="post" action="modulo18_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group ">
                                                                <label class="control-label"><i class="fa fa-exclamation-triangle" style="color:yellow"></i> Status do Módulo</label>
                                                                <select class="form-control input-sm mb-15 rounded" id="modulo18_status" name="modulo18_status">
                                                                    <option value="">Selecione uma Opção</option>
                                                                    <option value="1">Ativado</option>
                                                                    <option value="0">Desativado</option>
                                                                </select>
                                                            </div>


               
                                                            <div class="form-group">
                                                                <label class="control-label">Título</label>
                                                                <input class="form-control rounded" type="text"  name="modulo18_nome" placeholder="Assinar a Cotação" value="<?= stripslashes($modulo18->modulo18_nome) ?>"/>
                                                            </div>
                                                            
                                                           <div class="form-group">
                                                              <label class="control-label">Subtítulo</label>
                                                                <textarea class="form-control rounded" type="text"  name="modulo18_subtitulo" style="max-height:50px;"><?= stripslashes($modulo18->modulo18_subtitulo) ?></textarea>
                                                            </div>

                                                            
                                                            <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="row-fluid">
                                                                            <div class="col-md-9 pull-right">
                                                                                <label class="control-label">Imagem</label>
                                                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                                    <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                                                                    <span class="input-group-addon btn btn-success btn-file"><span class="fileinput-new">Selecione a Imagem</span><span class="fileinput-exists">Mudar de Imagem</span><input type="file" id="modulo18_imagem" name="modulo18_imagem"></span>
                                                                                    <a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <?php if (!empty($modulo18->modulo18_imagem)): ?>
                                                                                    <img src="../thumb.php?w=260&h=100&zc=0&src=images/<?= $modulo18->modulo18_imagem ?>" class="img-thumbnail" />
                                                                                <?php else: ?>
                                                                                    <img src="../thumb.php?w=260&h=100&zc=0&src=../img/news.jpg" class="img-thumbnail" />
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div style="margin-bottom: 20px;"></div>


                                                            <div class="form-group">
                                                                <label class="col-sm-12 control-label">Envios por hora <span class="small pull-right">[ajuste conforme o limite da sua hospedagem - deixe 0 para ilimitado]</span></label>
                                                                <div class="col-sm-12">
                                                                    <div class="slider-primary">
                                                                        <input id="envioscotacao" type="text" name="modulo18_envios" value="<?= $modulo18->modulo18_paginacao ?>" />
                                                                    </div>
                                                                    <br />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Botão</label>
                                                                <input class="form-control rounded" type="text" name="modulo18_button" style="max-width:200px;" value="<?= stripslashes($modulo18->modulo18_button) ?>" />
                                                                <input class="form-control rounded" type="hidden" id="modulo18_id" name="modulo18_id" value="<?= $modulo18->modulo18_id ?>" />
                                                            </div>
                                                            
                                                            <div style="margin-bottom: 20px;"></div>

                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO COTACAO ****************************-->
                                                         
                                            
                                            <!-- **************************** MODULO RODAPÉ ****************************-->
                                            <div class="panel panel-custom">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordioneleven">
                                                            <i class="fa fa-list"></i> Texto do Rodapé <span class="pull-right"><i class="fa fa-edit"></i></span> 
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div id="accordioneleven" class="panel-collapse collapse">
                                                    <form method="post" action="modulo11_fn.php?acao=atualizar">
                                                        <div class="panel-body">
                                                            <div class="form-group hidden">
                                                                <label class="control-label">Título do Mapa</label>
                                                                <input class="form-control rounded" type="text" name="modulo11_nome" value="<?= stripslashes($modulo11->modulo11_nome) ?>" />
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Direitos Reservados</label>
                                                                <input class="form-control rounded" type="text" name="modulo11_button" value="<?= stripslashes($modulo11->modulo11_button) ?>" />
                                                                <input class="form-control rounded" type="hidden" id="modulo11_id" name="modulo11_id" value="<?= $modulo11->modulo11_id ?>" />
                                                            </div>
                                                            
                                                            <div style="margin-bottom: 20px;"></div>
                                                            <div class="form-footer">
                                                                <div class="text-center">
                                                                    <button class="btn btn-primary" type="submit">Atualizar</button>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- **************************** MODULO RODAPÉ ****************************-->
                                        </div>
                                    </div><!-- /.panel-body -->
                                </div><!-- /.panel -->
                            </div><!-- /.col-md-6 -->
                        </div><!-- /.row -->
                    </div><!-- /.body-content -->
                    <!--/ End body content -->
            </section><!-- /#page-content -->
        </section><!-- /#wrapper -->
        <!--/ END WRAPPER -->

        <!-- START @BACK TOP -->
        <div id="back-top" class="animated pulse circle">
            <i class="fa fa-angle-up"></i>
        </div><!-- /#back-top -->
        <!--/ END BACK TOP -->
        <script src="./assets/js/jquery.min.js"></script>
        <script src="./assets/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="./assets/js/handlebars.js"></script>
        <script src="./assets/js/typeahead.bundle.min.js"></script>
        <script src="./assets/js/jquery.nicescroll.min.js"></script>
        <script src="./assets/js/index.js"></script>
        <script src="./assets/js/jquery.easing.1.3.min.js"></script>
        <script src="./assets/ionsound/ion.sound.min.js"></script>
        <script src="./assets/js/bootbox.js"></script>
        <script src="./assets/js/bootstrap-tagsinput.min.js"></script>
        <script src="./assets/js/jasny-bootstrap.fileinput.min.js"></script>
        <script src="./assets/js/holder.js"></script>
        <script src="./assets/js/bootstrap-maxlength.min.js"></script>
        <script src="./assets/js/jquery.autosize.min.js"></script>
        <script src="./assets/js/chosen.jquery.min.js"></script>
        <!--/ END PAGE LEVEL PLUGINS -->

        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="./assets/js/apps.js"></script>
        <script src="./assets/js/ion.rangeSlider.min.js"></script>
        <script src="./assets/js/dark.form.js"></script>
        <script src="./assets/js/jquery.rtnotify.js"></script>
        <script src="./assets/js/chosen.jquery.min.js"></script>
    </body>
    <script>
    $(document).ready(function() {
        $("span.cor").on('click',function(){
        var color = $(this).attr('data-color');
        if (document.getElementById(color).checked) document.getElementById(color).checked=false; else document.getElementById(color).checked=true;        
        ion.sound.play("button_tiny");   
       });
     });
        $("#depoimento").ionRangeSlider({
            min: 1,
            max: 30,
            from: <?= $modulo5->modulo5_limite ?>
        });

        $("#pagina").ionRangeSlider({
            min: 0,
            from_min: 1,
            max: 300,
            grid: true,
            grid_num: 30,
            from: <?= $modulo10->modulo10_paginacao ?>
        });
        
        $("#paginas").ionRangeSlider({
            min: 0,
            from_min: 1,
            max: 300,
            grid: true,
            grid_num: 30,
            from: <?= $modulo17->modulo17_paginacao ?>
        });
        
        $("#envios").ionRangeSlider({
            min: 0,
            max: 3000,
            grid: true,
            grid_num: 100,
            step: 10,
            from: <?= $modulo15->modulo15_envios ?>
        });

        $("#envioscotacao").ionRangeSlider({
            min: 0,
            max: 3000,
            grid: true,
            grid_num: 100,
            step: 10,
            from: <?= $modulo18->modulo18_envios ?>
        });
        
         $(document).ready(function() {
         $(".chosen-select1").chosen({disable_search:true,inherit_select_classes:true});
     
        $('div#icon1 #modulo14_icon1').val("<?= $modulo14->modulo14_icon1 ?>").trigger("chosen:updated");
         $( document ).ready(function() {
        $('div#icon1 .chosen-single').addClass("<?= $modulo14->modulo14_icon1 ?>").css("font-family","FontAwesome, sans-serif").html("&nbsp;&nbsp;<?= $modulo14->modulo14_icon1 ?> <i class='fa fa-caret-down' style='float:right; padding: 6px;'></i>");   
        });
        $('div#icon1 select#modulo14_icon1').on("change", function () {
         var icon1 = $(this).find(":selected").val();                   
        $('div#icon1 .chosen-single').removeAttr('class').attr('class', 'chosen-single'); 
        $('div#icon1 .chosen-single').addClass(icon1).css("font-family","FontAwesome, sans-serif").html("&nbsp;&nbsp;" + icon1 +  "<i class='fa fa-caret-down' style='float:right; padding: 6px;'></i>");
        });
        
        });
        
         $(document).ready(function() {
         $(".chosen-select2").chosen({disable_search:true});
     
        $('div#icon2 #modulo14_icon2').val("<?= $modulo14->modulo14_icon2 ?>").trigger("chosen:updated");
         $( document ).ready(function() {
        $('div#icon2 .chosen-single').addClass("<?= $modulo14->modulo14_icon2 ?>").css("font-family","FontAwesome, sans-serif").html("&nbsp;&nbsp;<?= $modulo14->modulo14_icon2 ?>  <i class='fa fa-caret-down' style='float:right; padding: 6px;'></i>");   
        });
        $('div#icon2 select#modulo14_icon2').on("change", function () {
         var icon2 = $(this).find(":selected").val();   
        $('div#icon2 .chosen-single').removeAttr('class').attr('class', 'chosen-single'); 
        $('div#icon2 .chosen-single').addClass(icon2).css("font-family","FontAwesome, sans-serif").html("&nbsp;&nbsp;" + icon2 +  "<i class='fa fa-caret-down' style='float:right; padding: 6px;'></i>");
        });
        
        });
        
        $(document).ready(function() {
        $(".chosen-select3").chosen({disable_search:true});
     
        $('div#icon3 #modulo14_icon3').val("<?= $modulo14->modulo14_icon3 ?>").trigger("chosen:updated");
         $( document ).ready(function() {
        $('div#icon3 .chosen-single').addClass("<?= $modulo14->modulo14_icon3 ?>").css("font-family","FontAwesome, sans-serif").html("&nbsp;&nbsp;<?= $modulo14->modulo14_icon3 ?>  <i class='fa fa-caret-down' style='float:right; padding: 6px;'></i>");   
        });
        $('div#icon3 select#modulo14_icon3').on("change", function () {
         var icon3 = $(this).find(":selected").val();   
        $('div#icon3 .chosen-single').removeAttr('class').attr('class', 'chosen-single'); 
        $('div#icon3 .chosen-single').addClass(icon3).css("font-family","FontAwesome, sans-serif").html("&nbsp;&nbsp;" + icon3 +  "<i class='fa fa-caret-down' style='float:right; padding: 6px;'></i>");
        });
        
        });
         
        $(document).ready(function() {
         $(".chosen-select4").chosen({disable_search:true});
     
        $('div#icon4 #modulo14_icon4').val("<?= $modulo14->modulo14_icon4 ?>").trigger("chosen:updated");
         $( document ).ready(function() {
        $('div#icon4 .chosen-single').addClass("<?= $modulo14->modulo14_icon4 ?>").css("font-family","FontAwesome, sans-serif").html("&nbsp;&nbsp;<?= $modulo14->modulo14_icon4 ?>  <i class='fa fa-caret-down' style='float:right; padding: 6px;'></i>");   
        });
        $('div#icon4 select#modulo14_icon4').on("change", function () {
         var icon4 = $(this).find(":selected").val();   
        $('div#icon4 .chosen-single').removeAttr('class').attr('class', 'chosen-single'); 
        $('div#icon4 .chosen-single').addClass(icon4).css("font-family","FontAwesome, sans-serif").html("&nbsp;&nbsp;" + icon4 +  "<i class='fa fa-caret-down' style='float:right; padding: 6px;'></i>");
        });
        
        });
        
    </script>
    <script>

<?php if (isset($_GET['success'])): ?>
            $(document).ready(function () {
                $.rtnotify({title: "Procedimento Realizado",
                    type: "default"});
            });
<?php endif; ?>

        $('#frontend').addClass('active');
        $('#<?=$modulo_aparencia->modulo_aparencia_cor?>').attr( 'checked', true );
        $('#modulo_aparencia_pelicula').val('<?= $modulo_aparencia->modulo_aparencia_pelicula ?>'); 
        $('#modulo_aparencia_slide').val('<?= $modulo_aparencia->modulo_aparencia_slide ?>');  
        $('#modulo_aparencia_idioma').val('<?= $modulo_aparencia->modulo_aparencia_idioma ?>');   
        $('#modulo1_status').val('<?= $modulo1->modulo1_status ?>');
        $('#modulo2_status').val('<?= $modulo2->modulo2_status ?>');
        $('#modulo3_status').val('<?= $modulo3->modulo3_status ?>');

        $('#modulo4_status').val('<?= $modulo4->modulo4_status ?>');
        $('#modulo5_status').val('<?= $modulo5->modulo5_status ?>');
        $('#modulo6_status').val('<?= $modulo6->modulo6_status ?>');

        $('#modulo7_status').val('<?= $modulo7->modulo7_status ?>');
        $('#modulo8_status').val('<?= $modulo8->modulo8_status ?>');
        $('#modulo9_status').val('<?= $modulo9->modulo9_status ?>');
        $('#modulo10_status').val('<?= $modulo10->modulo10_status ?>');
        $('#modulo10_comment').val('<?= $modulo10->modulo10_comment ?>');
        $('#modulo10_tags').val('<?= $modulo10->modulo10_tags ?>');
        $('#modulo11_status').val('<?= $modulo11->modulo11_status ?>');
        $('#modulo12_status').val('<?= $modulo12->modulo12_status ?>');
        $('#modulo13_status').val('<?= $modulo13->modulo13_status ?>');
        $('#modulo14_status').val('<?= $modulo14->modulo14_status ?>');
        $('#modulo15_status').val('<?= $modulo15->modulo15_status ?>');
        $('#modulo16_status').val('<?= $modulo16->modulo16_status ?>');
        $('#modulo17_status').val('<?= $modulo17->modulo17_status ?>');        
        $('#modulo17_comment').val('<?= $modulo17->modulo17_comment ?>');
        $('#modulo18_status').val('<?= $modulo18->modulo18_status ?>');
    </script>
</html>