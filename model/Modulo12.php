<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class Modulo12 extends Controle {

    public $db;
    public $modulo12_id;
    public $modulo12_nome;
    public $modulo12_descricao;
    public $modulo12_imagem;
    public $modulo12_status;
    public $result;

    public function __construct() {
        parent::__construct();
        require_once 'Registry.php';
        $registry = Registry::getInstance();
        if( $registry->get('db') == false ) {
            $registry->set('db', new DB);
        }
        $this->db = $registry->get('db');           
    }

    
    public function atualizar() {
        $query = "modulo12_nome = '$this->modulo12_nome', modulo12_descricao  = '$this->modulo12_descricao',modulo12_status = '$this->modulo12_status'";
        if (isset($_FILES['modulo12_imagem']['name']) && !empty($_FILES['modulo12_imagem']['name'])) {
            $query .= ", modulo12_imagem = '$this->modulo12_imagem'";
        }
        $this->update("modulo12", "$query", "modulo12_id = '$this->modulo12_id'");
    }
    
    public function removerArquivo() {
        $this->deleteArquivo("modulo12", "modulo12_id = '$this->modulo12_id'", "modulo12_imagem", "../images/");
    }

    public function enviar() {
        $this->upload("../images/", "modulo12_imagem", "");
    }

    
    public function getModulo12() {
        $this->select("modulo12", "", "*", "", "WHERE modulo12_id = 1", "");
    }

}
