<?php

require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}
function Json() {
        $j = new Mensagens();
        $j->mensagem_id =  intval($_REQUEST['mensagem_id']);
        echo $j->JSON();
}
function incluir() {
    $mensagens = new Mensagens();
    $mensagens->mensagem_nome = addslashes($_POST['mensagem_nome']);
    $texto = addslashes($_POST['mensagem_texto']);
    $texto = str_replace('<!DOCTYPE html>','',$texto);
    $texto = str_replace('<html>','',$texto);
    $texto = str_replace('</head>','',$texto);
    $texto = str_replace('<head>','',$texto);
    $texto = str_replace('<body>','',$texto);
    $texto = str_replace('</body>','',$texto);
    $texto = str_replace('</html>','',$texto);
    $texto = trim($texto);
    $mensagens->mensagem_texto = str_replace("../../images",Validacao::getBase()."images",$texto);   
    $mensagens->incluir();
    Filter::redirect("newsletter/?success");
}

function remover() {
    if (isset($_REQUEST['id'])) {
        $id = intval($_REQUEST['id']);
        $r = new Mensagens();
        $r->mensagem_id = $id;
        $r->remover();
        Filter :: redirect("newsletter/?success");
    }
}

function enviar() {
    if (isset($_REQUEST['id'])) {
        $id = intval($_REQUEST['id']);
        $r = new Mensagens();
        $r->mensagem_id = $id;
        $r->Enviar();
        Filter :: redirect("newsletter/?success");
    }
}

function atualizar() {
    $mensagem_id = intval($_POST['mensagem_id']);
    $mensagem_nome = addslashes($_POST['mensagem_nome']);
    $texto = addslashes($_POST['mensagem_texto']);
    $mensagem_texto = str_replace("../../../images",Validacao::getBase()."images",$texto);
    $mensagem_texto = str_replace('<!DOCTYPE html>','',$mensagem_texto);
    $mensagem_texto = str_replace('<html>','',$mensagem_texto);
    $mensagem_texto = str_replace('</head>','',$mensagem_texto);
    $mensagem_texto = str_replace('<head>','',$mensagem_texto);
    $mensagem_texto = str_replace('<body>','',$mensagem_texto);
    $mensagem_texto = str_replace('</body>','',$mensagem_texto);
    $mensagem_texto = str_replace('</html>','',$mensagem_texto);
    $mensagem_texto = trim($mensagem_texto);
    $msg_data = explode("/",$_POST['mensagem_data']);
    $mensagem_data = $msg_data[2]."-".$msg_data[1]."-".$msg_data[0];
    
    $a = new Mensagens();
    $a->mensagem_id = $mensagem_id;
    $a->mensagem_nome = $mensagem_nome;
    $a->mensagem_texto = $mensagem_texto;
    $a->mensagem_data = $mensagem_data;

    $a->atualizar();
    Filter :: redirect("newsletter/?success");
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
}