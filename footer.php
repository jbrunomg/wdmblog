<!-- Footer Top -->
	<footer class="footer wow fadeInUp">
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<!-- Single Widget -->
					<div class="col-md-4 col-sm-4 col-xs-12 ">
						<div class="single-widget about">
							<div class="footer-logo col-md-7 col-sm-7 col-xs-12">
							 <?php if($modulo_aparencia->modulo_aparencia_rodape != '') {?>
                             <a href="<?= Validacao::getBaseUrl() ?>/"><img src="images/<?= $modulo_aparencia->modulo_aparencia_rodape ?>" alt="" id="footerLogo" class="img-responsive"/></a>
                            <?php } ?>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
                           <?php
                          
                          $resumoSobre = Validacao::cut(stripslashes($sobre->modulo3_descricao), 220, '...');
                         // $resumoSobre = str_replace(".",". ",$resumoSobre);?>                            
							<p><?php echo $resumoSobre; ?></p>
                            <?php if(strlen($resumoSobre)>15) { ?>
							<div class="button">
								<a href="sobre/" class="btn primary">Leia mais</a>
                     
							</div>
                            <?php } ?>
                           </div>
						</div>
					</div>
					<!--/ End Single Widget -->
					<!-- Single Widget -->
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="single-widget contact">
							<h2><?= stripslashes($modulo2->modulo2_nome5) ?></h2>
							<ul class="list">
								<li><i class="fa fa-map"></i><?= stripslashes($contato->contato_endereco) ?></li>
                                <?php if ($contato->contato_telefone1) : ?>
								<li><i class="fa fa-phone"></i><?= stripslashes($contato->contato_telefone1) ?></li>
                                <?php endif; ?>
                                <?php if ($contato->contato_telefone2) : ?>
								<li><i class="fa fa-phone"></i><?= stripslashes($contato->contato_telefone2) ?></li>
                                <?php endif; ?>
                                <?php if ($contato->contato_telefone3) : ?>
								<li><i class="fa fa-phone"></i><?= stripslashes($contato->contato_telefone3) ?></li>
                                <?php endif; ?>
                                <?php if ($contato->contato_telefone4) : ?>
								<li><i class="fa fa-phone"></i><?= stripslashes($contato->contato_telefone4) ?></li>
                                <?php endif; ?>
                                <?php if ($contato->contato_telefone5) : ?>
								<li><i class="fa fa-phone"></i><?= stripslashes($contato->contato_telefone5) ?></li>
                                <?php endif; ?>
                                <?php if ($contato->contato_telefone6) : ?>
								<li><i class="fa fa-phone"></i><?= stripslashes($contato->contato_telefone6) ?></li>
                                <?php endif; ?>
								<li><i class="fa fa-envelope"></i><a href="mailto:<?= stripslashes($contato->contato_email) ?>"><?= stripslashes($contato->contato_email) ?></a></li>
							</ul>
						</div>
					</div>
					<!--/ End Single Widget -->
					<!-- Single Widget -->
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="single-widget">
							<h2>Siga-nos</h2>
							<ul class="social-icon">
                            <?php
                            $social = new Social();
                            $social->getSociaistatus();
                            ?>
                                  <?php if ($social->db->data[0]): ?>
                                        <?php foreach ($social->db->data as $redes) : 
											if($redes->social_nome == "fa fa-youtube") {
												$redes->social_nome = "fa fa-youtube-play";
											}
										?>
                                        <li  class="active"><a href="<?= Filter::UrlExternal($redes->social_url) ?>" target="_blank"><i class="<?= stripslashes($redes->social_nome) ?>"></i><?= $redes->social_titulo ?></a></li>
                                        <?php endforeach; ?>
                                 <?php endif; ?>
    
							</ul>					
						</div>
					</div>
					<!--/ End Single Widget -->
 <style>
select {
    background: #313131;
    color: #F7F7F7;
    border-radius: 2px;
    padding: 2px 6px;
    float: left;
    margin-right: 8px;
    
}
.goog-logo-link img {
    float: left;
    margin: 2px 0 0 6px;
}
.goog-te-gadget {
   
  }
 </style>                   
                    
                    
                    <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="single-widget about">
                   <p>Idioma:</p> <div id="google_translate_element" style="max-width: 200px;"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'pt', includedLanguages: 'de,en,es,fr,it,ja,pt,ru,zh-TW', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</div></div>
				</div>
			</div>
		</div>
  <!-- Footer Bottom -->
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-center">
						<!-- Copyright -->
						<p>&#9400; <?= $modulo11->modulo11_button ?></p>
						<!--/ End Copyright -->
					</div>
				</div>
            </div>
		</div>	
		<!--/ End Footer Bottom -->
	</footer>
	<!--/ End footer Top -->
 <script>
 //jQuery("._4t2a").hidden();
 //jQuery(".loader").hidden();
 </script>


<!-- Botão ZAP flutuante -->

    <?php foreach ($social->db->data as $redes) { 
		if($redes->social_nome == "fa fa-whatsapp") {
			$social_nome = "whatsapp";
			$social_numero = $redes->social_url;
		}}
	?>

   <?php if(!empty($social_nome)) { ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <a href="https://wa.me/55<?php echo $social_numero ?>?text=Olá%20desejo%20informações" style="position:fixed;width:60px;height:60px;bottom:40px;right:40px;background-color:#25d366;color:#FFF;border-radius:50px;text-align:center;font-size:30px;box-shadow: 1px 1px 2px #888;
      z-index:1000;" target="_blank">
    <i style="margin-top:16px" class="fa fa-whatsapp"></i>
    </a>
                
  <?php } ?>

<!-- Fim botão Zap flutuante -->

