<?php
require_once '../loader.php';
@session_start();
if (!isset($_SESSION['LOGADO']) || $_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}
$site = new Site();
$site->getMeta();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

    <!-- START @HEAD -->
    <head>
        <!-- START @META SECTION -->
        <?php require_once './base.php'; ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title><?= $site->site_meta_titulo ?></title>
        <!--/ END META SECTION -->

        <!-- START @FAVICONS -->
        <link href="./assets/img/ico/<?= $modulo_aparencia->modulo_aparencia_favicon ?>" rel="shortcut icon" sizes="144x144">
        <!--/ END FAVICONS -->

        <!-- START @FONT STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'>
        <!--/ END FONT STYLES -->

        <!-- START @GLOBAL MANDATORY STYLES -->
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- START @PAGE LEVEL STYLES -->
        <link href="./assets/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="./assets/css/animate.min.css" rel="stylesheet">
        <link href="./assets/css/bootstrap-tagsinput.css" rel="stylesheet">
        <link href="./assets/css/jasny-bootstrap-fileinput.min.css" rel="stylesheet">
        <link href="./assets/css/chosen.min.css" rel="stylesheet">
        <link href="./assets/css/bootstrap-switch.min.css" rel="stylesheet">
        <!--/ END PAGE LEVEL STYLES -->

        <!-- START @THEME STYLES -->
        <link href="./assets/css/reset.css" rel="stylesheet">
        <link href="./assets/css/layout.css" rel="stylesheet">
        <link href="./assets/css/components.css" rel="stylesheet">
        <link href="./assets/css/plugins.css" rel="stylesheet">
        <link href="./assets/css/themes/default.theme.css" rel="stylesheet" id="theme">
        <link href="./assets/css/custom.css" rel="stylesheet">
        <link href="./assets/css/datepicker.css" rel="stylesheet">
         <link href="./assets/css/summernote.css" rel="stylesheet">
        <link href="./assets/css/social.css" rel="stylesheet">
        <link href="./assets/css/jquery.rtnotify.css" rel="stylesheet">
        <link href="./assets/css/noty_theme_default.css" rel="stylesheet">
  
        <!--/ END THEME STYLES -->

        <!-- START @IE SUPPORT -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="./assets/js/html5shiv.min.js"></script>
        <script src="./assets/js/respond.min.js"></script>
        <![endif]-->
        <!--/ END IE SUPPORT -->
       
    </head>
    <!--/ END HEAD -->

    <body>

        <!--[if lt IE 9]>
        <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
<script type="text/javascript" src="assets/tiny_mce/tinymce.js" id="richedit"></script>
<script type="text/javascript">
 tinymce.init({
  selector: "textarea",
  language: "pt_BR",
  selector: 'textarea',
  browser_spellcheck: true,
  contextmenu: false,
  external_plugins: {"nanospell": "<?= Validacao::getBaseUrl(); ?>/assets/tiny_mce/plugins/nanospell/plugin.js"},
  nanospell_server: "php",
  nanospell_dictionary: "pt_br",
  nanospell_autostart: true,
  nanospell_ignore_words_with_numerals: false,
  nanospell_ignore_block_caps: false,
  nanospell_compact_menu: true,
  height: 380,
  theme: 'modern',
  cleanup_on_startup : true,
  
  images_upload_base_path: '<?=Validacao::getBase()?>images/newsletter/',
  plugins: [
    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
  ],

  toolbar1: "cut copy paste | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | searchreplace nanospell",
  toolbar2: "bullist numlist | outdent indent blockquote | undo redo | link unlink image code preview | forecolor backcolor | table hr fullscreen | ltr rtl",

  menubar: false,
  toolbar_items_size: 'small',

 
  content_css: [
   
    '//www.tinymce.com/css/codepen.min.css'
  ]
});
</script>

        <section id="wrapper" class="page-sound">
            <?php require_once './navegacao.php'; ?>
            <?php require_once './menu.php'; ?>
            <section id="page-content">
                <div class="header-content">
                    <h2><i class="fa fa-comments"></i>  <span>Gerenciar Mensagens  </span></h2>
                    <div class="breadcrumb-wrapper hidden-xs">
                        <span class="label">Você está em :</span>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="home/">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Mensagens</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="body-content animated fadeIn">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="pull-left">
                            <h3 class="panel-title"><button class="btn btn-primary" data-toggle="modal" data-target="#incluir"><i class="fa fa-plus-circle"></i> Adicionar Mensagem</button></h3>
                        </div>
                        <div class="clearfix"></div>
                        <br />
                        <?php $mensagens = new Mensagens();
                                          $mensagens->getMensagens();
                                          $mensagens->db->paginate(24);
                                    ?>
                                        <?php if (isset($mensagens->db->data [0])): ?>
                            <div class="table-responsive mb-20">
                                <table class="table table-striped table-primary">
                                    <thead>
                                        <tr>
                                           <th>Título</th>
                                            <th width="140">Data</th>
                                            <th width="180">Envio</th>
                                            <th width="160">Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    
                                            <?php foreach ($mensagens->db->data as $msg): ?>
                                                <tr>

                                                    <td>
                                                        <?= stripslashes($msg->mensagem_nome) ?>    
                                                    </td>
                                                     <td>
                                                         <?= date('d/m/Y', strtotime($msg->mensagem_data)); ?>     
                                                    </td>
                                                     <td>
                                                     <?php if($msg->mensagem_envio != "0000-00-00 00:00:00") {?>
                                                     <?= date('d/m/Y H:i', strtotime($msg->mensagem_envio)); ?>   
                                                     <?php } else {?>
                                                     sem envios ainda 
                                                      <?php } ?>   
  
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                        
                                                         <a title="Enviar Mensagem" data-url="mensagem_fn.php?acao=enviar&id=<?= $msg->mensagem_id ?>" class="btn btn-circle btn-success enviar">
                                                                <i class="fa fa-paper-plane-o icon-white"></i>
                                                            </a>
                                                            <a title="Editar Mensagem" class="btn btn-circle btn-info" href="news/editar/<?= $msg->mensagem_id ?>" style="margin-left: 6px">
                                                                <i class="fa fa-edit icon-white"></i>
                                                            </a>
                                                            <a title="Remover Mensagem" data-url="mensagem_fn.php?acao=remover&id=<?= $msg->mensagem_id ?>" class="btn  btn-danger btn-circle delete"  style="margin-left: 6px">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                            
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            <?= $mensagens->db->paginacao ?>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        
           <!--***************MODAL INCLUIR*****************-->
        <div class="modal fade" id="incluir" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog panel panel-primary" style="width: 768px;">
                <div class="modal-content ">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h3 class="panel-title text-center">Nova Mensagem</h3>
                    </div>
                    <div class="modal-header ">
                        <form role="form" method="post" id="form-incluir" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label">Título</label>
                                <input class="form-control" type="text" name="mensagem_nome" placeholder="Informe o título da mensagem " required>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Texto</label>
                                
                                <textarea class="form-control rounded" name="mensagem_texto" id="mensagem_texto_ad" placeholder="Escreva a mensagem " required></textarea>           
                            
                            </div>


                            <!-- /.form-group -->

                            <div class="form-group">
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary" onclick="myFunction()">Cadastrar</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
       <!--***************MODAL INCLUIR*****************--> 
        

        
        
        
        
        <!--***************MODAL ENVIAR*****************-->
          <div class="modal fade" id="MODALSEND" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        
                       <h4 class="text-center text-primary">Enviar Mensagem</h4>
                        <p id="text-alert" class="text-center text-primary">
                            Enviar mensagem para os emails cadastrados.<br />
                            Você confirma este procedimento?
                        </p>
                        <p id="text-send" class="text-center text-primary" style="display: none;">Sua mensagem está sendo enviada!<br /> O processo poderá demorar alguns minutos dependendo da quantidade de cadastros e do limite de envios por hora.<br />Mantenha esta janela aberta até a finalização dos envios.<br /></p>
                        <p class="text-center">
                            <button type="button" class="btn btn-danger" id="btn-cancel-send" data-dismiss="modal"><i class="fa fa-remove"></i> Cancelar</button>
                            <button type="button" class="btn btn-success" id="btn-confirm-send"><i class="fa fa-paper-plane-o"></i> Enviar</button>

                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!--***************MODAL ENVIAR*****************-->


        <!--***************MODAL REMOVER*****************-->
        <div class="modal fade" id="MODALREMOVE" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="text-center text-danger">Atenção!</h4>
                        <p class="text-center text-danger">
                            Você está prestes à excluir um registro de forma permanente.<br />
                            Deseja realmente executar este procedimento?
                        </p>
                        <p class="text-center">
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-remove"></i> Cancelar</button>
                            <button type="button" class="btn btn-danger" id="btn-confirm-remove"><i class="fa fa-trash"></i> Remover</button>

                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!--***************MODAL REMOVER*****************-->
        <div id="back-top" class="animated pulse circle">
            <i class="fa fa-angle-up"></i>
        </div>

        <!-- START @CORE PLUGINS -->
        <script src="./assets/js/jquery.min.js"></script>
        <script src="./assets/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="./assets/js/handlebars.js"></script>
        <script src="./assets/js/typeahead.bundle.min.js"></script>
        <script src="./assets/js/jquery.nicescroll.min.js"></script>
        <script src="./assets/js/index.js"></script>
        <script src="./assets/js/jquery.easing.1.3.min.js"></script>
        <script src="./assets/ionsound/ion.sound.min.js"></script>
        <script src="./assets/js/bootbox.js"></script>
        <!--/ END CORE PLUGINS -->

        <!-- START @PAGE LEVEL PLUGINS -->
        <script src="./assets/js/bootstrap-tagsinput.min.js"></script>
        <script src="./assets/js/jasny-bootstrap.fileinput.min.js"></script>
        <script src="./assets/js/holder.js"></script>
        <script src="./assets/js/bootstrap-maxlength.min.js"></script>
        <script src="./assets/js/jquery.autosize.min.js"></script>
        <script src="./assets/js/chosen.jquery.min.js"></script>
        <script src="./assets/js/jquery.rtnotify.js"></script>
        <!--/ END PAGE LEVEL PLUGINS -->

        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="./assets/js/apps.js"></script>
        <script src="./assets/js/summernote.min.js"></script>
        <script src="./assets/js/summernote-pt-BR.js"></script>
        <script src="./assets/js/bootstrap-switch.min.js"></script>
        <script src="./assets/js/bootstrap-datepicker.js"></script>
        <script src="./assets/js/bootstrap-datepicker.pt-BR.js"></script>
      
        <script>
            $('.newsletter').addClass('active');
<?php if (isset($_GET['success'])): ?>
                $(document).ready(function () {
                    $.rtnotify({title: "Procedimento Realizado",
                        type: "default"});
                });
<?php endif; ?>
            $("[name='switch']").bootstrapSwitch();
            

            $('.delete').on('click', function () {
                var url = $(this).attr('data-url');
                $('#MODALREMOVE').modal('show');
                $('#btn-confirm-remove').on('click', function () {
                    window.location = url;
                });
            });
            
            $('.enviar').on('click', function () {
                var url = $(this).attr('data-url');
                $('#MODALSEND').modal('show');
                $('#btn-confirm-send').on('click', function () {
                    $('#text-alert').fadeOut("fast");
                    $('#btn-cancel-send').fadeOut("fast");
                    $('#text-send').fadeIn("slow");
                    $('#btn-confirm-send').html("Aguarde...");
                    window.location = url;
                });
            });

            $(".sound").on("click", function () {
                ion.sound.play("button_push.mp3");
            });

         
$("form#form-incluir").submit(function(event){
    $('#err').hide(); //Esconde o elemento com id errolog

  //disable the default form submission
  event.preventDefault();
 
  //grab all form data  
  var formData = new FormData($(this)[0]);
 
  $.ajax({
    url: 'mensagem_fn.php?acao=incluir',
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false
  
  });
 
  return false;
});
           
          $('#mensagem_data,#data_in,#data_out').datepicker({
            format: "dd/mm/yyyy",
            language: "pt-BR"
        });  
        </script>
        
 <script>
function myFunction() {
    document.getElementById("form-incluir").submit();
}
</script>
    </body>
</html>