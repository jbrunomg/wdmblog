<?php

/*
 * @author phpstaff.com.br
 */

require_once 'Controle.php';
class ModuloAparencia extends Controle {

    public $modulo_aparencia_id;
    public $modulo_aparencia_cor;
    public $modulo_aparencia_idioma;
    public $modulo_aparencia_favicon;
    public $modulo_aparencia_logo;
    public $modulo_aparencia_rodape;
    public $modulo_aparencia_slide;
    public $modulo_aparencia_pelicula;

    public function __construct() {
        parent::__construct();
    }

    public function atualizar() {
        $query = "modulo_aparencia_cor = '$this->modulo_aparencia_cor', modulo_aparencia_idioma = '$this->modulo_aparencia_idioma', modulo_aparencia_slide = '$this->modulo_aparencia_slide', modulo_aparencia_pelicula = '$this->modulo_aparencia_pelicula' ";
        if (isset($_FILES['modulo_aparencia_logo']['name']) && !empty($_FILES['modulo_aparencia_logo']['name'])) {
            $query .= ", modulo_aparencia_logo = '$this->modulo_aparencia_logo'";
        }
        if (isset($_FILES['modulo_aparencia_favicon']['name']) && !empty($_FILES['modulo_aparencia_favicon']['name'])) {
            $query .= ", modulo_aparencia_favicon = '$this->modulo_aparencia_favicon'";
        }
        
        if (isset($_FILES['modulo_aparencia_rodape']['name']) && !empty($_FILES['modulo_aparencia_rodape']['name'])) {
            $query .= ", modulo_aparencia_rodape = '$this->modulo_aparencia_rodape'";
        }

        $this->update("modulo_aparencia", "$query", "modulo_aparencia_id = '$this->modulo_aparencia_id'");
    }

    public function removerLogo() {
        $this->deleteArquivo("modulo_aparencia", "modulo_aparencia_id = '$this->modulo_aparencia_id'", "modulo_aparencia_imagem", "../images/");
    }

    public function enviarLogo() {
        $this->upload3("../images/", "modulo_aparencia_logo", "");
    }
    
    public function removerRodape() {
        $this->deleteArquivo("modulo_aparencia", "modulo_aparencia_id = '$this->modulo_aparencia_id'", "modulo_aparencia_rodape", "../images/");
    }

    public function enviarRodape() {
        $this->upload2("../images/", "modulo_aparencia_rodape", "");
    }

    public function removerFavicon() {
        $this->deleteArquivo("modulo_aparencia", "modulo_aparencia_id = '$this->modulo_aparencia_id'", "modulo_aparencia_favicon", "./assets/img/ico/");
    }

    public function enviarFavicon() {
        $this->upload("./assets/img/ico/", "modulo_aparencia_favicon", "");
    }

    public function getModuloAparencia() {
        $query = "WHERE modulo_aparencia_id = 1";
        $this->select("modulo_aparencia", "", "*", "", "$query", "");
    }

}