<?php
function __autoload($className) {
    $classpath = array(
        'model/','helpers/','plugin/email/',//frontend
        '../model/','../helpers/','../plugin/email/'//backend
        );    
    $classFile = ucfirst($className) . ".php";
    $loaded = false;
    foreach ($classpath as $path) {
        if (is_readable("$path$classFile")) {
            require "$path$classFile";
            $loaded = true;
            break;
        }
    }
}

function getUserIP()
{
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}

function getUrl($value='')
{
   // $base_url  = "http://" . $_SERVER['HTTP_HOST']; 
   // $base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);

    $base_url =  $_SERVER['PHP_SELF'];

    $base_url = explode('/',$base_url);

    return $base_url[2];

}


$site = new Site();
$site->getMeta();

$modulo_aparencia = new ModuloAparencia();
$modulo_aparencia->getModuloAparencia();

$modulo1 = new Modulo1();
$modulo1->getModulo1();

$topo = new Modulo1();
$topo->getModulo1();

$modulo2 = new Modulo2();
$modulo2->getModulo2();

$menu = new Modulo2();
$menu->getModulo2();

$modulo3 = new Modulo3();
$modulo3->getModulo3();

$sobre = new Modulo3();
$sobre->getModulo3();

$modulo4 = new Modulo4();
$modulo4->getModulo4();

$modulo5 = new Modulo5();
$modulo5->getModulo5();

$modulo6 = new Modulo6();
$modulo6->getModulo6();

$modulo7 = new Modulo7();
$modulo7->getModulo7();

//$projeto = new Portfolio();
//$projeto->getPortfolios();

$modulo8 = new Modulo8();
$modulo8->getModulo8();

$modulo9 = new Modulo9();
$modulo9->getModulo9();

$contato = new Contato();
$contato->getContato();

$modulo10 = new Modulo10();
$modulo10->getModulo10();

$blog = new Modulo10();
$blog->getModulo10();

$modulo11 = new Modulo11();
$modulo11->getModulo11();

$rodape = new Modulo11();
$rodape->getModulo11();

$modulo12 = new Modulo12();
$modulo12->getModulo12();

$video = new Modulo12();
$video->getModulo12();

$modulo13 = new Modulo13();
$modulo13->getModulo13();

$modulo14 = new Modulo14();
$modulo14->getModulo14();

$modulo15 = new Modulo15();
$modulo15->getModulo15();

$modulo16 = new Modulo16();
$modulo16->getModulo16();

$modulo17 = new Modulo17();
$modulo17->getModulo17();

$modulo18 = new Modulo18();
$modulo18->getModulo18();

$servico = new Servico();
$servico->getServicos();

$categoria_blog = new Area();
$categoria_blog->getAreas();


//if($modulo_aparecia->modulo_aparecia_idioma != 'pt') {
setcookie('googtrans', '/pt/'.stripslashes($modulo_aparencia->modulo_aparencia_idioma));
//}