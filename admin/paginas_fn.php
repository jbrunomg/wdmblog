<?php
require_once '../loader.php';
@session_start();
if ($_SESSION['LOGADO'] == FALSE) {
    @header('location:' . Validacao::getBase() . 'admin/logar/');
    exit;
}

function incluir() {
    $paginas_nome = addslashes($_POST['paginas_nome']);
    $paginas_descricao = addslashes($_POST['paginas_descricao']);
    $paginas_area3 = $_POST['paginas_area3'];
  
    $paginas = new Paginas();
    $paginas->paginas_nome = $paginas_nome;
	$paginas->paginas_descricao = str_replace("../../../images",Validacao::getBase()."images",$paginas_descricao);
    $paginas->paginas_area3 = $paginas_area3;
    if (isset($_FILES['paginas_imagem']['name']) && !empty($_FILES['paginas_imagem']['name'])) {
        $paginas->enviar();
    }
    $paginas->incluir();

    $id = $paginas->db->getId();
    Filter :: redirect("paginas/?success");
}


function incluir_fotos() {
    $paginas_nome = addslashes($_POST['paginas_nome']);
    $paginas_descricao = addslashes($_POST['paginas_descricao']);
    $paginas_area3 = $_POST['paginas_area3'];
  
    $paginas = new Paginas();
    $paginas->paginas_nome = $paginas_nome;
    $paginas->paginas_descricao = $paginas_descricao;
    $paginas->paginas_area3 = $paginas_area3;
    if (isset($_FILES['paginas_imagem']['name']) && !empty($_FILES['paginas_imagem']['name'])) {
        $paginas->enviar();
    }
    $paginas->incluir();

    $id = $paginas->db->getId();
    Filter :: redirect("paginas/editar/$id/");
}

function atualizar() {
	$paginas_descricao = addslashes($_POST['paginas_descricao']);
    $a = new Paginas();
    $a->paginas_nome = addslashes($_POST['paginas_nome']);
	//$a->paginas_descricao = str_replace("../../../../images",Validacao::getBase()."images",$paginas_descricao);
	$a->paginas_descricao = str_replace("images/indica003.png","https://www.indicaeuropa.com/images/indica003.png",$paginas_descricao);
    $a->paginas_area3 = $_POST['paginas_area3'];
    $a->paginas_id = intval($_POST['paginas_id']);
    if (isset($_FILES['paginas_imagem']['name']) && !empty($_FILES['paginas_imagem']['name'])) {
        $a->removerArquivo();
        $a->enviar();
    }
    $a->atualizar();
    Filter :: redirect("paginas/?success");
}

function remover() {
    if (isset($_REQUEST['id'])) {
        $id = intval($_REQUEST['id']);
        $r = new Paginas();
        $r->paginas_id = $id;
        $r->removerArquivo();
        $r->remover();
        Filter :: redirect("paginas/?success");
    }
}

function removerFoto() {
    $paginas_id = $_REQUEST['paginas_id'];
    foreach ($_POST['fotos'] as $fotos_id) {
        $fotos = new Fotos();
        $fotos->fotos_id = "$fotos_id";
        $fotos->removerImagem();
        $fotos->remover();
    }
    Filter:: redirect("paginas/editar/$paginas_id/#galeria");
}

function Json() {
    if (isset($_REQUEST['paginas_id'])) {
        $paginas_id = intval($_REQUEST['paginas_id']);
        $j = new Paginas();
        $j->paginas_id = $paginas_id;
        echo $j->JSON();
    }
}

if (isset($_REQUEST['acao']) && !empty($_REQUEST['acao'])) {
    $acao = $_REQUEST['acao'];
    if (function_exists($acao)) {
        $acao();
    }
} elseif (isset($_POST['incluir'])) {
    $acao = "incluir";
    if (function_exists($acao)) {
        $acao();
    }
} elseif (isset($_POST['incluir_fotos'])) {
    $acao = "incluir_fotos";
    if (function_exists($acao)) {
        $acao();
    }
}